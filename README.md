# Accammo
Accammo is a project which role is to prove the concept of 'Coordinators' approach that allows better isolation of modules while working on the high-scale projects.  
This project also serves as a showcase for more efficient dependency segregation and modularization in general.  
![App Preview](https://bitbucket.org/alex_apriamashvili/accammo/raw/11a57ef8c459d84dff2599bb760fc5a515d5382e/readme/img/app_preview.png)
#### Installation

1. Clone repo or download an archive
2. Install Bundler by executing: `$ gem install bundler`  
More information regarding Bundler coud be found [here](https://bundler.io/)

#### Configuration

1. Navigate to your working copy directory `$ cd ~/path/to/your/local/accammo`
2. Install bundled dependencies by executing `$ bundle install`
3. Install CocoaPods dependendencies by executing the following command: `$ bundle exec pod install`

## Overview

### Modularization & Build Optimizations

The project is contained of the main executable and a few libraries that serve as the project dependencies (modules) and being controlled by CocoaPods.

Both the main executable and the modules could be described in 3 different levels:

1. **Application.** The main executable, one ond only.  
Module on this level can consume the rest of the modules, but cannot be consumed by any of those.
2. **Domain.** The modules that represents a separate domain or a functionality of the application.  
These modules can be consumed by the main executable (Accammo), but cannot be consumed by one-another or any low-level (_Utility_) modules.  
3. **Utility.** The modules that serve the domain modules and provide those the utility level support.  
These modules can be consumed by all upper-level modules and by one another.

Here is the scheme that represents the modules in all 3 levels:  
![Conceptual Scheme](https://bitbucket.org/alex_apriamashvili/accammo/raw/11a57ef8c459d84dff2599bb760fc5a515d5382e/readme/img/accammo_final_scheme.png)

Such level-segregation has been introduced to control and accelerate compile time with the New Xcode Build System, to use build-tasks parallelization more efficiently.  
The idea is to compile the most light-weight but rarely modifiable modules (_Utilities_) first,  
to facilitate faster incremental builds, while more frequently modified modules (_Domain_) could use the parallelization, by having all untouched low-level dependencies prudently cached.

### Coordinators

The idea of a coordinators is to allow its user to manage the application flow with no much hassle.  

Coordinators could be organized in a **Tree** structure in case of complex flows (such as Bottom Navigation or pre- / post- login experiences),  
or as a **LinkedList** for more primitive scenarios, as the one that is shown below:  
![Coordinators Chain](https://bitbucket.org/alex_apriamashvili/accammo/raw/11a57ef8c459d84dff2599bb760fc5a515d5382e/readme/img/coordinators_chain.png)

Each coordinator has one module associated with this coordinator.  
Each module associated with the coordinator reports to that coordinator as to a **Module Output** but it doesn't own the coordinator.  
The coordinator instead, owns its module and provides it with all input neccessary injected upon module creation, also it can communicate to the module via it's **Module Input** protocol, to provide any state updare or facilitate requests that comes from other modules / coordinators.  
A detailed scheme of binding between a coordinator and its module is shown below:

![Detailed Binding](https://bitbucket.org/alex_apriamashvili/accammo/raw/11a57ef8c459d84dff2599bb760fc5a515d5382e/readme/img/coordinator_module_binding.png)

The 'Coordinators' is a very powerful approach that gives its user an opportunity to easily manage the application flow.  
The second point for using Coordinators is that it facilitates a better segregation for the components of an application, which results in a better testability of an app.

More information about coordinators, ways of their appliance and testability could be found in the source code and [this presentation](https://bitbucket.org/alex_apriamashvili/accammo/raw/4612b61dee908fdb42dcb64c51f07d4b2c7312f3/Accammo/Resources/Accamo_pres.key).

## Q&A

##### 1. How Do I Login?
Currently, there is no validation for the login credentials, so any non-empty symbol combination shall work.  
e.g:  
user: `user`  
pass: `qwerty`

Although, as a remote Mock Server is used to facilitate dummy backend responses, the Internet connection id required.


##### 2. What does the very first console log mean?  
```text
Total pre-main time: 724.13 milliseconds (100.0%)
         dylib loading time: 171.25 milliseconds (23.6%)
        rebase/binding time: 429.30 milliseconds (59.2%)
            ObjC setup time:  83.28 milliseconds (11.5%)
           initializer time:  40.13 milliseconds (5.5%)
           slowest intializers :
             libSystem.B.dylib :   5.06 milliseconds (0.6%)
    libMainThreadChecker.dylib :  17.73 milliseconds (2.4%)
```

This report is provided by `dyld` tool and shows the time spent on different operations performed during the application bootstrap before the control over the process is passed to the application.
Currently, all development pod dependencies are linked statically to the main executable. Such configuration gives a significantly faster _pre-main_ time, but makes the resulting binary bigger, as all symbols are included into that binary.  
However, it doesn't have any negative impact on the application size, as all application dependencies are bundled with the main executable on iOS.
You might see how this time changes if you replace `true` with `false` in `Podfile`:

```ruby
def pod.static_framework?;
  true # replace with `false`
end
```
_Note: Please run `bundle exec pod install` once you made any changes to the `Podfile` to apply those changes to the project._

##### 3. What are these UICollectionViewCell warnings are about?

```text
2019-07-27 11:52:31.356543+0800 Accammo[1940:1463472] [Assert] contentView of collectionViewCell has translatesAutoresizingMaskIntoConstraints false and is missing constraints to the cell, 
which will cause substandard performance in cell autosizing. Please leave the contentView's translatesAutoresizingMaskIntoConstraints true or else provide constraints between the contentView and the cell. 
<Spots.SpotPreviewCell: 0x7fc9d4520220; baseClass = UICollectionViewCell; frame = (0 0; 0 0); layer = <CALayer: 0x600001d88480>>
```

Although, this warning message might look frightning, there is nothing to be worried about.  
These warnings are applicable for the _offstage_ cells only, meaning that these cells won't be rendered as they are not associated with a collection view displayed on the screen.  
An urgency to have such _offstage_ cells was in unability for certain iOS versions to calculate `UICollectionViewCell` height properly.  
This height calculation workaround will be gone once the minimal iOS version is bumped up to iOS 11.  
This change is planned as a future enhancement.