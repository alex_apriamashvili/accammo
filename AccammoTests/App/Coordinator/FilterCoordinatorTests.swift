
import XCTest
import Domain
import SearchFilter
@testable import Accammo

final class FilterCoordinatorTests: XCTestCase {

    private var sut: FilterCoordinator!
    private var tabFactory: MockTabFactory!
    private var searchCriteriaContainer: MockSearchCriteriaContainer!
    private var appState: MockApplicationState!
    private var dependencies: MockTabBarCoordinatorDependencies!
    private var parentCoordinator: MockCoordinator!

    override func setUp() {
        super.setUp()
        parentCoordinator = MockCoordinator()
        sut = FilterCoordinator()
        sut.parent = parentCoordinator
        tabFactory = MockTabFactory()
        searchCriteriaContainer = MockSearchCriteriaContainer()
        appState = MockApplicationState()
        dependencies = MockTabBarCoordinatorDependencies(
            appState: appState,
            searchCriteriaContainer: searchCriteriaContainer,
            tabFactory: tabFactory
        )
    }
    
    override func tearDown() {
        parentCoordinator = nil
        sut = nil
        dependencies = nil
        tabFactory = nil
        searchCriteriaContainer = nil
        dependencies = nil
        appState = nil
        super.tearDown()
    }
    
    func testCoordinatorNotWrappedIntoNaviogationStack() {
        let flow = sut.start(parameters: searchCriteriaContainer.currentSearchCriteria().filter)

        XCTAssertFalse(flow is UINavigationController)
        XCTAssertTrue(flow is SearchFilterViewController)
    }

    func testApplyFilter() {
        sut.applyFilter(MockEntity.mockFilter())

        XCTAssertTrue(parentCoordinator.isCalled(.didFinish))
    }

    func testDismissWithoutChanges() {
        sut.dismissWithoutAnyChanges()

        XCTAssertTrue(parentCoordinator.isCalled(.didFinish))
    }
}

