
import XCTest
import Domain
import SpotDetail
@testable import Accammo

final class SpotDetailCoordinatorTests: XCTestCase {

    private var sut: SpotDetailCoordinator!
    private var tabFactory: MockTabFactory!
    private var searchCriteriaContainer: MockSearchCriteriaContainer!
    private var appState: MockApplicationState!
    private var dependencies: MockTabBarCoordinatorDependencies!
    private var parentCoordinator: MockCoordinator!

    override func setUp() {
        super.setUp()
        parentCoordinator = MockCoordinator()
        sut = SpotDetailCoordinator()
        sut.parent = parentCoordinator
        tabFactory = MockTabFactory()
        searchCriteriaContainer = MockSearchCriteriaContainer()
        appState = MockApplicationState()
        dependencies = MockTabBarCoordinatorDependencies(
            appState: appState,
            searchCriteriaContainer: searchCriteriaContainer,
            tabFactory: tabFactory
        )
    }
    
    override func tearDown() {
        parentCoordinator = nil
        sut = nil
        dependencies = nil
        tabFactory = nil
        searchCriteriaContainer = nil
        dependencies = nil
        appState = nil
        super.tearDown()
    }
    
    func testCoordinatorNotWrappedIntoNaviogationStack() {
        let flow = sut.start(parameters: MockEntity.mockSpot())

        XCTAssertFalse(flow is UINavigationController)
        XCTAssertTrue(flow is SpotDetailViewController)
    }

    func testDidFinishPresentation() {
        sut.didFinishPresentation()

        XCTAssertTrue(parentCoordinator.isCalled(.didFinish))
    }
}
