
import XCTest
import Domain
import Spots
@testable import Accammo

final class SpotsCoordinatorTests: XCTestCase {

    private var sut: SpotCoordinator!
    private var tabFactory: MockTabFactory!
    private var searchCriteriaContainer: MockSearchCriteriaContainer!
    private var appState: MockApplicationState!
    private var dependencies: MockTabBarCoordinatorDependencies!

    override func setUp() {
        super.setUp()
        sut = SpotCoordinator()
        tabFactory = MockTabFactory()
        searchCriteriaContainer = MockSearchCriteriaContainer()
        appState = MockApplicationState()
        dependencies = MockTabBarCoordinatorDependencies(
            appState: appState,
            searchCriteriaContainer: searchCriteriaContainer,
            tabFactory: tabFactory
        )
    }
    
    override func tearDown() {
        sut = nil
        dependencies = nil
        tabFactory = nil
        searchCriteriaContainer = nil
        dependencies = nil
        appState = nil
        super.tearDown()
    }
    
    func testCoordinatorWrppedIntoNavigationStack() {
        let flow = sut.start(parameters: dependencies)

        XCTAssertTrue(flow is UINavigationController)
    }

    func testCoordinatorFirstViewControllerInStack() {
        let flow = sut.start(parameters: dependencies)
        let rootViewContrroller = (flow as! UINavigationController).viewControllers.first!

        XCTAssert(rootViewContrroller is SpotsViewController)
    }

    func testLoginCoordinatorIsShown() {
        _ = sut.start(parameters: dependencies)

        XCTAssertEqual(sut.nodes.count, 1)
        XCTAssertTrue(sut.nodes[0] is LoginCoordinator)
    }

    func testLoginCoordinatorDidFinish() {
        _ = sut.start(parameters: dependencies)
        let loginCoordinator = LoginCoordinator()
        let loginCoordinatorResult = LoginCompletionResult.success(token: String())
        sut.coordinator(loginCoordinator, didFinish: loginCoordinatorResult)

        XCTAssertTrue(sut.nodes.isEmpty)
    }

    func testUserDidTapOnSpot() {
        sut.handleTapOnSpot(MockEntity.mockSpot())

        XCTAssertEqual(sut.nodes.count, 1)
        XCTAssertTrue(sut.nodes[0] is SpotDetailCoordinator)
    }

    func testUserReturnedFromSpotdetail() {
        sut.handleTapOnSpot(MockEntity.mockSpot())
        let spotDetailCoordinator = SpotDetailCoordinator()
        let spotDetaiolResult = SpotDetailCompletionResult()
        sut.coordinator(spotDetailCoordinator, didFinish: spotDetaiolResult)

        XCTAssertTrue(sut.nodes.isEmpty)
    }

}
