
import XCTest
import Domain
import TabBar
@testable import Accammo

class TabBarCoordinatorTests: XCTestCase {

    var sut: TabBarCoordinator!
    var tabFactory: MockTabFactory!
    var searchCriteriaContainer: MockSearchCriteriaContainer!
    var appState: MockApplicationState!
    var dependencies: MockTabBarCoordinatorDependencies!

    let mockCoordinator = MockCoordinator()
    let mockFlow = UIViewController()

    override func setUp() {
        super.setUp()

        sut = TabBarCoordinator()
        tabFactory = MockTabFactory()
        searchCriteriaContainer = MockSearchCriteriaContainer()
        appState = MockApplicationState()
        dependencies = MockTabBarCoordinatorDependencies(
            appState: appState,
            searchCriteriaContainer: searchCriteriaContainer,
            tabFactory: tabFactory
        )
    }
    
    override func tearDown() {
        sut = nil
        tabFactory = nil
        searchCriteriaContainer = nil
        dependencies = nil
        appState = nil

        super.tearDown()
    }
    
    func testStartInvocations() {
        let flow = sut.start(parameters: dependencies)

        XCTAssertTrue(flow is TabBarModule)
        XCTAssertTrue(tabFactory.isCalled(.tab(coordinator: mockCoordinator, flow: mockFlow), times: 3))
    }

    func testSubCoordinatorsCreation() {
        _ = sut.start(parameters: dependencies)

        XCTAssertEqual(sut.nodes.count, 3)
    }

    func testSubCoordinatorsOrder() {
        _ = sut.start(parameters: dependencies)

        XCTAssertTrue(sut.nodes[0] is SpotCoordinator)
        XCTAssertTrue(sut.nodes[1] is SearchCoordinator)
        XCTAssertTrue(sut.nodes[2] is ProfileCoordinator)
    }

    func testSubCoordinatorsRemoval() {
        _ = sut.start(parameters: dependencies)
        let spotsCoordinator: SpotCoordinator = sut.nodes[0] as! SpotCoordinator
        let result = SpotCompletionResult()

        XCTAssertEqual(sut.nodes.count, 3)

        sut.coordinator(spotsCoordinator, didFinish: result)

        XCTAssertEqual(sut.nodes.count, 2)
        XCTAssertFalse(sut.nodes[0] is SpotCoordinator)
    }
}
