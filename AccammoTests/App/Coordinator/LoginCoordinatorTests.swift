
import XCTest
import Login
@testable import Accammo

final class LoginCoordinatorTests: XCTestCase {

    private var sut: LoginCoordinator!
    private var parentCoordinator: MockCoordinator!

    override func setUp() {
        super.setUp()
        parentCoordinator = MockCoordinator()
        sut = LoginCoordinator()
        sut.parent = parentCoordinator
    }

    override func tearDown() {
        parentCoordinator = nil
        sut = nil
        super.tearDown()
    }

    func testCoordinatorIsNotWrappedIntoNaviogationStack() {
        let flow = sut.start(parameters: nil)

        XCTAssertFalse(flow is UINavigationController)
    }

    func testCoordinatorFlowClass() {
        let flow = sut.start(parameters: nil)

        XCTAssert(flow is LoginViewController)
    }

    func testHandleLogin() {
        sut.handleLogin(token: String())

        XCTAssertTrue(parentCoordinator.isCalled(.didFinish))
    }
}

