
import XCTest
import Domain
import Search
@testable import Accammo

final class SearchCoordinatorTests: XCTestCase {

    private var sut: SearchCoordinator!
    private var tabFactory: MockTabFactory!
    private var searchCriteriaContainer: MockSearchCriteriaContainer!
    private var appState: MockApplicationState!
    private var dependencies: MockTabBarCoordinatorDependencies!
    private var parentCoordinator: MockCoordinator!

    override func setUp() {
        super.setUp()
        parentCoordinator = MockCoordinator()
        sut = SearchCoordinator()
        sut.parent = parentCoordinator
        tabFactory = MockTabFactory()
        searchCriteriaContainer = MockSearchCriteriaContainer()
        appState = MockApplicationState()
        dependencies = MockTabBarCoordinatorDependencies(
            appState: appState,
            searchCriteriaContainer: searchCriteriaContainer,
            tabFactory: tabFactory
        )
    }
    
    override func tearDown() {
        parentCoordinator = nil
        sut = nil
        dependencies = nil
        tabFactory = nil
        searchCriteriaContainer = nil
        dependencies = nil
        appState = nil
        super.tearDown()
    }
    
    func testCoordinatorWrappedIntoNaviogationStack() {
        let flow = sut.start(parameters: dependencies)

        XCTAssertTrue(flow is UINavigationController)
    }

    func testCoordinatorFirstViewControllerInStack() {
        let flow = sut.start(parameters: dependencies)
        let rootViewContrroller = (flow as! UINavigationController).viewControllers.first!

        XCTAssert(rootViewContrroller is SearchViewController)
    }

    func testSpotDetailCoordinatorIsAssigned() {
        sut.handleTapOnSpot(MockEntity.mockSpot())

        XCTAssertEqual(sut.nodes.count, 1)
        XCTAssertTrue(sut.nodes[0] is SpotDetailCoordinator)
    }

    func testSpotDetailCoordinatorDidFinish() {
        _ = sut.start(parameters: dependencies)
        let detailCoordinator = SpotDetailCoordinator()
        let detailCoordinatorResult = SpotDetailCompletionResult()
        sut.coordinator(detailCoordinator, didFinish: detailCoordinatorResult)

        XCTAssertTrue(sut.nodes.isEmpty)
    }

    func testFilterCoordinatorIsAssigned() {
        _ = sut.start(parameters: dependencies)
        sut.handleTapOnSearch()

        XCTAssertEqual(sut.nodes.count, 1)
        XCTAssertTrue(sut.nodes[0] is FilterCoordinator)
    }

    func testFilterCoordinatorDidFinish() {
        _ = sut.start(parameters: dependencies)
        let filterCoordinator = SpotDetailCoordinator()
        let filterCoordinatorResult = FilterCompletionResult(filter: MockEntity.mockFilter())
        sut.coordinator(filterCoordinator, didFinish: filterCoordinatorResult)

        XCTAssertTrue(sut.nodes.isEmpty)
    }
}
