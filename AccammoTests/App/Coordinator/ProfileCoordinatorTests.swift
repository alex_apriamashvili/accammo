
import XCTest
import Domain
import Profile
@testable import Accammo

final class ProfileCoordinatorTests: XCTestCase {

    private var sut: ProfileCoordinator!
    private var tabFactory: MockTabFactory!
    private var searchCriteriaContainer: MockSearchCriteriaContainer!
    private var appState: MockApplicationState!
    private var dependencies: MockTabBarCoordinatorDependencies!

    override func setUp() {
        super.setUp()
        sut = ProfileCoordinator()
        tabFactory = MockTabFactory()
        searchCriteriaContainer = MockSearchCriteriaContainer()
        appState = MockApplicationState()
        dependencies = MockTabBarCoordinatorDependencies(
            appState: appState,
            searchCriteriaContainer: searchCriteriaContainer,
            tabFactory: tabFactory
        )
    }
    
    override func tearDown() {
        sut = nil
        dependencies = nil
        tabFactory = nil
        searchCriteriaContainer = nil
        dependencies = nil
        appState = nil
        super.tearDown()
    }
    
    func testCoordinatorWrappedIntoNaviogationStack() {
        appState.stubUserState = .loggedIn(token: "token")
        let flow = sut.start(parameters: dependencies)

        XCTAssertTrue(flow is UINavigationController)
    }

    func testEmptyScreenIncaseIfUserIsNotAuthorized() {
        let flow = sut.start(parameters: dependencies)

        XCTAssertFalse(flow is UINavigationController)
    }

    func testCoordinatorFirstViewControllerInStack() {
        appState.stubUserState = .loggedIn(token: "token")
        let flow = sut.start(parameters: dependencies)
        let rootViewContrroller = (flow as! UINavigationController).viewControllers.first!

        XCTAssert(rootViewContrroller is ProfileViewController)
    }

    func testLoginCoordinatorIsShown() {
        appState.stubUserState = .loggedIn(token: "token")
        let _ = sut.start(parameters: dependencies)
        sut.handleLogout()

        XCTAssertEqual(sut.nodes.count, 1)
        XCTAssertTrue(sut.nodes[0] is LoginCoordinator)
    }

    func testLoginCoordinatorDidFinish() {
        _ = sut.start(parameters: dependencies)
        let loginCoordinator = LoginCoordinator()
        let loginCoordinatorResult = LoginCompletionResult.success(token: String())
        sut.coordinator(loginCoordinator, didFinish: loginCoordinatorResult)

        XCTAssertTrue(sut.nodes.isEmpty)
    }
}
