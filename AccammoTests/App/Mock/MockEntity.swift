
import Foundation
import Domain
@testable import Accammo

/// a factory that produces mock entities
final class MockEntity {

    private struct Constants {
        static let url = URL(string:"http://ya.ru")!
        static let currency = Currency(symbol: "", code: "")
        static let doubleValue: Double = 1.0
        static let intValue: Int = 1
        static let stringValue: String = "Test"
    }

    /// creates a mock Spot entity
    static func mockSpot() -> Spot {
        return Spot(
            identifier: MockEntity.Constants.intValue,
            name: MockEntity.Constants.stringValue,
            description: MockEntity.Constants.stringValue,
            rating: MockEntity.Constants.doubleValue,
            image: Image(
                imageURL: MockEntity.Constants.url,
                width: MockEntity.Constants.doubleValue,
                height: MockEntity.Constants.doubleValue
            ),
            wage: Wage(
                amount: MockEntity.Constants.doubleValue,
                currency: MockEntity.Constants.currency,
                basis: .day
            ),
            type: .apartment
        )
    }

    /// creates a mock Profile entity
    static func mockProfile() -> Profile {
        return Profile(
            avatarImageURL: MockEntity.Constants.url,
            avatarWidth: MockEntity.Constants.doubleValue,
            avatarHeight: MockEntity.Constants.doubleValue,
            experienceMonths: MockEntity.Constants.intValue,
            firstName: MockEntity.Constants.stringValue,
            lastName: MockEntity.Constants.stringValue,
            title: MockEntity.Constants.stringValue,
            biography: MockEntity.Constants.stringValue
        )
    }

    /// creates mock filter entity
    static func mockFilter() -> SearchCriteria.Filter {
        return SearchCriteria.Filter(
            spotName: "",
            rating: [],
            wage: SearchCriteria.Filter.Range(
                min: 0.0,
                selectedMin: 0.0,
                max: 10,
                selectedMax: 10
            ),
            skills: []
        )
    }

    /// create mock spot detail wrapper entity
    static func mockSpotDetail() -> SpotDetail {
        return SpotDetail(spot: mockSpot())
    }
}
