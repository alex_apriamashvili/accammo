
import TestUtils
import UIKit
@testable import Accammo

final class MockCoordinator: Coordinator, CoordinatorOutput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case start(parameters: Params)
        case didFinish
    }

    var invocations: [Invocation] = []

    init() {}

    typealias Params = Void?
    var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    var stubViewController = Flow()
    func start(parameters: Params) -> Coordinator.Flow {
        invocations.append(.start(parameters: parameters))
        return stubViewController
    }

    func coordinator<C: Coordinator>(_ coordinator: C, didFinish result: CoordinatorResult) {
        invocations.append(.didFinish)
    }
}
