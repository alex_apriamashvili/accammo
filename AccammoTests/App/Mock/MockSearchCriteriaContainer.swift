
import Domain
import TestUtils

final class MockSearchCriteriaContainer: SearchCriteriaContainer, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case currentSearchCriteria
        case updateFilter(filter: SearchCriteria.Filter)
    }

    var invocations: [Invocation] = []

    var stubSearchCriteria: SearchCriteria = MockSearchCriteriaContainer.mockSearchCriteria()
    func currentSearchCriteria() -> SearchCriteria {
        invocations.append(.currentSearchCriteria)
        return stubSearchCriteria
    }

    func update(filter: SearchCriteria.Filter) {
        invocations.append(.updateFilter(filter: filter))
    }
    
}

private extension MockSearchCriteriaContainer {

    typealias FilterRange = SearchCriteria.Filter.Range

    static func mockSearchCriteria() -> SearchCriteria {
        return SearchCriteria(
            spotInfo: .studio,
            filter: MockEntity.mockFilter()
        )
    }
}
