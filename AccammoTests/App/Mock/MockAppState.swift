
@testable import Accammo
import TestUtils

final class MockApplicationState: ApplicationState, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case updateUser(state: UserState)
    }

    var invocations: [Invocation] = []

    var stubUserState: UserState = .unauthorized
    var userState: UserState {
        return stubUserState
    }

    func updateUser(state: UserState) {
        invocations.append(.updateUser(state: state))
    }
}
