
import Foundation
import Domain
@testable import Accammo

struct MockTabBarCoordinatorDependencies: TabBarCoordinatorDependencies {
    let appState: ApplicationState
    let searchCriteriaContainer: SearchCriteriaContainer
    let tabFactory: TabFactory
}
