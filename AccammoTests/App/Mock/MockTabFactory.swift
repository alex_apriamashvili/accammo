
import TestUtils
import UIKit
@testable import Accammo

final class MockTabFactory: TabFactory, MockInvocable{

    enum Invocation: MockInvocationEnum {
        case tab(coordinator: CoordinatorNode, flow: Flow)
    }

    var invocations: [Invocation] = []

    var stubProduct = MockTabBarItemPresentable()
    func tab<C: Coordinator>(coordinator: C, flow: Coordinator.Flow) -> TabFactory.Product {
        invocations.append(.tab(coordinator: coordinator, flow: flow))
        return stubProduct
    }
}
