
import XCTest
import TestUtils
import Domain
@testable import SearchFilter

class SearchFilterModuleInteractorTests: XCTestCase {

    var sut: SearchFilterInteractor!
    var presenter: MockSearchFilterPresenter!

    override func setUp() {
        super.setUp()
        sut = SearchFilterInteractor(filter: MockEntity.mockFilter())
        presenter = MockSearchFilterPresenter()
        sut.output = presenter
    }

    override func tearDown() {
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func testSetSpotNameNotToNil() {
        sut.set(spotName: "TEST")

        XCTAssertEqual(sut.fetchLatestFilter().spotName, "TEST")
    }

    func testSetSpotNameToNil() {
        sut.set(spotName: nil)

        XCTAssertEqual(sut.fetchLatestFilter().spotName, "")
    }

    func testSetNewSkill() {
        sut.set(skill: .singing)

        XCTAssertEqual(sut.fetchLatestFilter().skills, [SearchCriteria.Skill.singing])
    }

    func testSetExistingSkill() {
        sut.set(skill: .singing)
        sut.set(skill: .drums)
        sut.set(skill: .singing)

        XCTAssertEqual(sut.fetchLatestFilter().skills, [SearchCriteria.Skill.drums])
    }

    func testSetWageRange() {
        sut.set(wage: 0, max: 25)

        XCTAssertEqual(sut.fetchLatestFilter().wage.selectedMin, 0)
        XCTAssertEqual(sut.fetchLatestFilter().wage.selectedMax, 25)
    }

    func testSetStarRating() {
        sut.set(starRating: [1,2,3])

        XCTAssertEqual(sut.fetchLatestFilter().rating, [1,2,3])
    }

    func testUpdateState() {
        sut.updateState()

        XCTAssertTrue(presenter.isCalled(.updateData(data: MockEntity.mockFilter())))
    }
}
