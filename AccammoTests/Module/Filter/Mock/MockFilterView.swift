
import TestUtils
import UI
@testable import SearchFilter

final class MockSearchFilterView: SearchFilterViewInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case updateContents(contents: [SectionViewModel])
        case hideKeyboard
    }

    var invocations: [Invocation] = []

    func updateContents(_ contents: [SectionViewModel]) {
        invocations.append(.updateContents(contents: contents))
    }

    func hideKeyboard() {
        invocations.append(.hideKeyboard)
    }
}
