
import TestUtils
import Domain
@testable import SearchFilter

final class MockSearchFilterModuleOutput: SearchFilterModuleOutput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case applyFilter(filter: SearchCriteria.Filter)
        case dismissWithoutAnyChanges
    }

    var invocations: [Invocation] = []

    func applyFilter(_ filter: SearchCriteria.Filter) {
        invocations.append(.applyFilter(filter: filter))
    }

    func dismissWithoutAnyChanges() {
        invocations.append(.dismissWithoutAnyChanges)
    }
}
