
import TestUtils
import Domain
@testable import SearchFilter

typealias MockSearchFilterPresenterComitments = SearchFilterInteractorOutput &
                                                SearchFilterViewOutput &
                                                SearchFilterModuleInput

final class MockSearchFilterPresenter: MockSearchFilterPresenterComitments, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case viewIsReady
        case updateData(data: SearchCriteria.Filter)
        case applyFilter
        case handleDismiss
    }

    var invocations: [Invocation] = []

    func viewIsReady() {
        invocations.append(.viewIsReady)
    }

    func update(_ data: SearchCriteria.Filter) {
        invocations.append(.updateData(data: data))
    }

    func applyFilter() {
        invocations.append(.applyFilter)
    }

    func handleDismiss() {
        invocations.append(.handleDismiss)
    }
}
