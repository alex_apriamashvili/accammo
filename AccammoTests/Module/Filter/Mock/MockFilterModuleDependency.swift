
import UI
import TestUtils
@testable import SearchFilter

struct MockSearchFilterModuleDependency: SearchFilterModuleDependencies {

    var collectionViewAdapter: CollectionViewAdapter = CollectionViewAdapter(dependencies: MockCollectionViewAdapterDependency())
    var styleProvider: SearchFilterStyleProvider = MockSearchFilterModuleStyleProvider()
    var translationProvider: SearchFilterTranslationProvider = MockSearchFilterModuleResourcesPovider()
}


struct MockSearchFilterModuleStyleProvider: SearchFilterStyleProvider {

    var backgroundColor: UIColor = .clear
    let tintColor: UIColor = .clear

    var starImage: UIImage = UIImage()
    var blueColor: UIColor = .clear
    var grayTextColor: UIColor = .clear
    var imageTintColor: UIColor = .clear
    var lightBlueColor: UIColor = .clear
    var lightGrayColor: UIColor = .clear
    var selectedElementColor: UIColor = .clear
    var selectedTextColor: UIColor = .clear
    var textColor: UIColor = .clear
    var whiteColor: UIColor = .clear
    var smallBoldFont: UIFont = UIFont.systemFont(ofSize: 1)
    var mediumLightFont: UIFont = UIFont.systemFont(ofSize: 1)
    var mediumBoldFont: UIFont = UIFont.systemFont(ofSize: 1)
    var bigBoldFont: UIFont = UIFont.systemFont(ofSize: 1)
    var veryBigLightFont: UIFont = UIFont.systemFont(ofSize: 1)
    var starButtonRadii: CGFloat = 0
    var cellWidth: CGFloat = 0
}

struct MockSearchFilterModuleResourcesPovider: SearchFilterTranslationProvider {

    var screenTitle: String = ""
    var filterBySpotNameText: String = ""
    var spotNamePlaceholderText: String = ""
    var starRatingTitle: String = ""
    var wageTitle: String = ""
    var skillsTitle: String = ""
    var filterText: String = ""
}

final class MockUIComponentsDifferAdapter: UIComponentsDifferAdapter, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case diff
    }

    var invocations: [Invocation] = []

    var stubDifferElements: [UIComponentsDifferElement] = []
    func diff(previous: [SectionViewModel],
              update: [SectionViewModel],
              isEqualSection: @escaping UIComponentsDifferAdapter.SectionEquality,
              isEqualElement: @escaping UIComponentsDifferAdapter.ElementEquality) -> [UIComponentsDifferElement] {
        invocations.append(.diff)
        return []
    }
}

struct MockCollectionViewAdapterDependency: HasUIComponentsDifferAdapter {

    var differAdapter: UIComponentsDifferAdapter = MockUIComponentsDifferAdapter()
}

