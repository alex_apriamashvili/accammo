
import TestUtils
import Domain
@testable import SearchFilter

final class MockSearchFilterInteractor: SearchFilterInteractorInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case setSpotName(name: String?)
        case setSkill(skill: SearchCriteria.Skill)
        case setWage(min: Double, max: Double)
        case setStarRating(rating: [Int])
        case updateState
        case fetchCurrentWage
        case fetchLatestFilter
    }

    var invocations: [Invocation] = []

    func set(spotName name: String?) {
        invocations.append(.setSpotName(name: name))
    }

    func set(skill: SearchCriteria.Skill) {
        invocations.append(.setSkill(skill: skill))
    }

    func set(wage min: Double, max: Double) {
        invocations.append(.setWage(min: min, max: max))
    }

    func set(starRating rating: [Int]) {
        invocations.append(.setStarRating(rating: rating))
    }

    func updateState() {
        invocations.append(.updateState)
    }

    var currentWageStub = SearchCriteria.Filter.Range(min: 0, selectedMin: 0, max: 1, selectedMax: 1)
    func fetchCurrentWage() -> SearchCriteria.Filter.Range {
        invocations.append(.fetchCurrentWage)
        return currentWageStub
    }

    var stubLatestFilter = MockEntity.mockFilter()
    func fetchLatestFilter() -> SearchCriteria.Filter {
        invocations.append(.fetchLatestFilter)
        return stubLatestFilter
    }
}
