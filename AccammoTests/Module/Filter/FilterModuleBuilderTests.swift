
import XCTest
@testable import SearchFilter

class SearchFilterModuleBuilderTests: XCTestCase {

    // NOTE: setUp() & tearDown() are redundant here, since only one static method is being tested

    func testViewControllerAndItsOutput() {
        let dependency = MockSearchFilterModuleDependency()
        let output = MockSearchFilterModuleOutput()
        let sut = SearchFilterModuleBuilder(dependencies: dependency).build(with: output, filter: MockEntity.mockFilter())
        let presenter = (sut as! SearchFilterViewController).presenter

        XCTAssertTrue(sut is SearchFilterViewController)
        XCTAssertTrue(presenter is SearchFilterPresenter)
    }

    func testPresenterAndItsReferrals() {
        let dependency = MockSearchFilterModuleDependency()
        let output = MockSearchFilterModuleOutput()
        let sut = SearchFilterModuleBuilder(dependencies: dependency).build(with: output, filter: MockEntity.mockFilter()) as! SearchFilterViewController
        let presenter = sut.presenter as! SearchFilterPresenter

        XCTAssertTrue(presenter.interactor is SearchFilterInteractor)
        XCTAssertTrue(presenter.router is SearchFilterRouter)
        XCTAssertTrue(presenter.view === sut)
    }

    func testInteractorAndItsReferrals() {
        let dependency = MockSearchFilterModuleDependency()
        let output = MockSearchFilterModuleOutput()
        let sut = SearchFilterModuleBuilder(dependencies: dependency).build(with: output, filter: MockEntity.mockFilter()) as! SearchFilterViewController
        let presenter = sut.presenter as! SearchFilterPresenter
        let interactor = presenter.interactor as! SearchFilterInteractor

        XCTAssertTrue(interactor.output === presenter)
    }
}
