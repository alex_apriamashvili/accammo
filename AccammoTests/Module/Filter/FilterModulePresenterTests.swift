
import XCTest
import TestUtils
import UI
import Domain
@testable import SearchFilter

final class SearchFilterModulePresenterTests: XCTestCase {

    var sut: SearchFilterPresenter!
    var view: MockSearchFilterView!
    var interactor: MockSearchFilterInteractor!
    var output: MockSearchFilterModuleOutput!

    private let dependency = MockSearchFilterModuleDependency()

    override func setUp() {
        super.setUp()

        sut = SearchFilterPresenter(dependencies: dependency)
        view = MockSearchFilterView()
        interactor = MockSearchFilterInteractor()
        output = MockSearchFilterModuleOutput()

        sut.view = view
        sut.interactor = interactor
        sut.moduleOutput = output
    }
    
    override func tearDown() {
        sut = nil
        view = nil
        interactor = nil
        output = nil

        super.tearDown()
    }
    
    func testViewIsReadyWasCalled() {
        sut.viewIsReady()

        XCTAssertTrue(interactor.isCalled(.updateState))
    }

    func testApplyFilter() {
        sut.applyFilter()

        XCTAssertTrue(output.isCalled(.applyFilter(filter: MockEntity.mockFilter())))
    }

    func handleDismiss() {
        sut.handleDismiss()

        XCTAssertTrue(output.isCalled(.dismissWithoutAnyChanges))
    }

    func testUpdateData() {
        sut.update(MockEntity.mockFilter())

        XCTAssertTrue(view.isCalled(.updateContents(contents: [])))
    }

    func testDidTapSpotName() {
        sut.didTapSpotName()

        XCTAssertTrue(interactor.isCalled(.updateState))
    }

    func testDidCommitSpotName() {
        sut.didCommitSpotName(nil)

        XCTAssertTrue(interactor.isCalled(.setSpotName(name: name)))
    }

    func testDidEndEditingSpotName() {
        sut.didEndEditing()

        XCTAssertTrue(interactor.isCalled(.updateState))
    }

    func testDidTapOnTag() {
        let cell = CloudTagCell()
        let item = CloudTagItem(text: "", itemId: 0)
        sut.tagCloud(cell, didTapOnTag: item, at: 0)

        XCTAssertTrue(view.isCalled(.hideKeyboard))
        XCTAssertTrue(interactor.isCalled(.setSkill(skill: .singing)))
    }

    func testMinWageWilChange() {
        _ = sut.minWageWillChange(to: 0)

        XCTAssertTrue(interactor.isCalled(.fetchCurrentWage))
    }

    func testMaxWageWillChange() {
        _ = sut.maxWageWillChange(to: 0)

        XCTAssertTrue(interactor.isCalled(.fetchCurrentWage))
    }

    func testWageDidChange() {
        sut.wageDidChange(minValue: 0, maxValue: 1)

        XCTAssertTrue(view.isCalled(.hideKeyboard))
        XCTAssertTrue(interactor.isCalled(.setWage(min: 0, max: 1)))
    }

    func starRatingDidChangeValue() {
        let cell = StarRatingCell()
        let value = [1]
        sut.starRating(cell, didChangeValue: value, newValue: 0)

        XCTAssertTrue(view.isCalled(.hideKeyboard))
        XCTAssertTrue(interactor.isCalled(.setStarRating(rating: [1])))
    }
}
