
import XCTest
import TestUtils
@testable import Login

class LoginModulePresenterTests: XCTestCase {

    var sut: LoginPresenter!
    var view: MockLoginView!
    var interactor: MockLoginInteractor!
    var output: MockLoginModuleOutput!
    var router: MockLoginRouter!

    private let dependency = MockLoginModuleDependency()

    override func setUp() {
        super.setUp()

        sut = LoginPresenter(dependency: dependency)
        view = MockLoginView()
        interactor = MockLoginInteractor()
        output = MockLoginModuleOutput()
        router = MockLoginRouter()

        sut.view = view
        sut.interactor = interactor
        sut.output = output
        sut.router = router
    }
    
    override func tearDown() {
        sut = nil
        view = nil
        interactor = nil
        output = nil
        router = nil

        super.tearDown()
    }

    func testAuthorize() {
        sut.authorize(token: String())

        XCTAssertTrue(view.isCalled(.hidePreloader))
        XCTAssertTrue(output.isCalled(.handleLogin(token: String())))
    }

    func testPresentError() {
        sut.present(error: MockError.error)

        XCTAssertTrue(view.isCalled(.hidePreloader))
    }

    func testHandleTapOnLoginButton() {
        sut.handleTapOnLoginButton(login: String(), handle: String())

        XCTAssertTrue(view.isCalled(.showPreloader))
        XCTAssertTrue(interactor.isCalled(.doLogin(credentials: Credentials(login:String(), password: String()))))
    }
    
}
