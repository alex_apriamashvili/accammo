
import XCTest
@testable import Login

class LoginModuleBuilderTests: XCTestCase {

    // NOTE: setUp() & tearDown() are redundant here, since only one static method is being tested

    func testViewControllerAndItsOutput() {
        let dependency = MockLoginModuleDependency()
        let sut = LoginModuleBuilder.build(output: nil, dependencies: dependency)
        let presenter = sut.output

        XCTAssertTrue(presenter is LoginPresenter)
    }

    func testPresenterAndItsReferrals() {
        let dependency = MockLoginModuleDependency()
        let controller = LoginModuleBuilder.build(output: nil, dependencies: dependency)
        let presenter = controller.output as! LoginPresenter

        XCTAssertTrue(presenter.interactor is LoginInteractor)
        XCTAssertTrue(presenter.router is LoginRouter)
        XCTAssertTrue(presenter.view === controller)
    }

    func testInteractorAndItsReferrals() {
        let dependency = MockLoginModuleDependency()
        let controller = LoginModuleBuilder.build(output: nil, dependencies: dependency)
        let presenter = controller.output as! LoginPresenter
        let interactor = presenter.interactor as! LoginInteractor

        XCTAssertTrue(interactor.output === presenter)
    }
}
