
import XCTest
import TestUtils
@testable import Login

class LoginModuleInteractorTests: XCTestCase {

    var sut: LoginInteractor!
    var service: MockLoginService!
    var presenter: MockLoginPresenter!

    override func setUp() {
        super.setUp()
        service = MockLoginService()
        sut = LoginInteractor(service: service)
        presenter = MockLoginPresenter()
        sut.output = presenter
    }

    override func tearDown() {
        service = nil
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func testServiceWasCalled() {
        sut.doLogin(credentials: Credentials(login: "", password: ""))

        XCTAssertTrue(service.isCalled(.login))
    }

    func testResultIsSentToTheOutput() {
        sut.doLogin(credentials: Credentials(login: "", password: ""))

        XCTAssertTrue(presenter.isCalled(.authorize(token: String())))
    }

    func testReturnError() {
        service.stubFailure = true
        sut.doLogin(credentials: Credentials(login: "", password: ""))

        XCTAssertTrue(presenter.isCalled(.present(error: MockError.error)))
    }
}
