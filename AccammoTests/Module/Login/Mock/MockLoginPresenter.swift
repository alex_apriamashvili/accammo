
import UIKit
import TestUtils
@testable import Login

typealias MockLoginPresenterComitments =    LoginInteractorOutput &
                                            LoginViewOutput &
                                            LoginModuleInput

final class MockLoginPresenter: MockLoginPresenterComitments, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case authorize(token: String)
        case present(error: Error)
        case handleTapOnLoginButton(login: String, handle: String)
    }

    var invocations: [Invocation] = []

    func authorize(token: String) {
        invocations.append(.authorize(token: token))
    }

    func present(error: Error) {
        invocations.append(.present(error: error))
    }

    func handleTapOnLoginButton(login: String, handle: String) {
        invocations.append(.handleTapOnLoginButton(login: login, handle: login))
    }
}
