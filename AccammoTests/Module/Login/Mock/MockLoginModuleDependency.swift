
import UIKit
import Login

struct MockLoginModuleDependency: LoginModuleDependency {

    var style: LoginModuleStyleProvider = MockLoginModuleStyleProvider()
    var resources: LoginModuleResourcesPovider = MockLoginModuleResourcesPovider()
}


struct MockLoginModuleStyleProvider: LoginModuleStyleProvider {

    var backgroundColor: UIColor = .clear
    var tintColor: UIColor = .clear
    var inputFont: UIFont = UIFont.systemFont(ofSize: 1)
    var inputTextColor: UIColor = .clear
    var overlayColor: UIColor = .clear
}

struct MockLoginModuleResourcesPovider: LoginModuleResourcesPovider {

    var backgroundImage: UIImage = UIImage()
    var logoImage: UIImage = UIImage()
    var loginPlaceholderText: String = ""
    var handlePlaceholderText: String = ""
}
