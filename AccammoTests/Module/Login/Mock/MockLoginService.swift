
import Service
import TestUtils

final class MockLoginService: LoginService, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case login
    }

    var invocations: [Invocation] = []

    var stubResponse: AuthorizationResult = MockLoginService.mockResponse()
    var stubFailure: Bool = false
    func login(credentials: AuthorizationParams,
               success: @escaping SuccessClosure,
               failure: @escaping FailureClosure) {
        invocations.append(.login)
        if stubFailure {
            failure(MockError.error)
        } else {
            success(stubResponse)
        }
    }
}

private extension MockLoginService {

    static func mockResponse() -> AuthorizationResult {
        return AuthorizationResult(
            token: "gjhkglh9876vbn0hoiug75d6"
        )
    }
}
