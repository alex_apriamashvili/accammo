
import TestUtils
@testable import Login

final class MockLoginModuleOutput: LoginModuleOutput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case handleLogin(token: String)
    }

    var invocations: [Invocation] = []

    func handleLogin(token: String) {
        invocations.append(.handleLogin(token: token))
    }
}
