
import UIKit
import TestUtils
@testable import Login

final class MockLoginRouter: LoginRouterInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case present(viewController: UIViewController)
        case push(viewController: UIViewController)
    }

    var invocations: [Invocation] = []

    func present(_ viewController: UIViewController) {
        invocations.append(.present(viewController: viewController))
    }
    func push(_ viewController: UIViewController) {
        invocations.append(.push(viewController: viewController))
    }
}
