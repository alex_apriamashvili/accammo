
import TestUtils
@testable import Login

final class MockLoginView: LoginViewInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case showError(message: String)
        case showPreloader
        case hidePreloader
    }

    var invocations: [Invocation] = []

    func showError(message: String) {
        invocations.append(.showError(message: message))
    }

    func showPreloader() {
        invocations.append(.showPreloader)
    }

    func hidePreloader() {
        invocations.append(.hidePreloader)
    }
}
