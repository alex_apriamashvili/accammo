
import TestUtils
@testable import Login

final class MockLoginInteractor: LoginInteractorInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case doLogin(credentials: Credentials)
    }

    var invocations: [Invocation] = []

    func doLogin(credentials: Credentials) {
        invocations.append(.doLogin(credentials: credentials))
    }
}
