
import XCTest
import TestUtils
@testable import Search

class SearchModuleInteractorTests: XCTestCase {

    var sut: SearchInteractor!
    var service: MockSpotFinderService!
    var presenter: MockSearchPresenter!

    private let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()

    override func setUp() {
        super.setUp()
        service = MockSpotFinderService()
        sut = SearchInteractor(service: service)
        presenter = MockSearchPresenter()
        sut.output = presenter
    }

    override func tearDown() {
        service = nil
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func testServiceWasCalled() {
        sut.search(searchCriteria: searchCriteria)

        XCTAssertTrue(service.isCalled(.findSpots))
    }

    func testResultIsSentToTheOutput() {
        sut.search(searchCriteria: searchCriteria)

        XCTAssertTrue(presenter.isCalled(.presentResults(spots: [])))
    }

    func testReturnError() {
        service.stubSearchFailure = true
        sut.search(searchCriteria: searchCriteria)

        XCTAssertTrue(presenter.isCalled(.presentError(error: MockError.error)))
    }

    func testCanFetchAnEntity() {
        sut.search(searchCriteria: searchCriteria)
        sut.fetchSearchResult(at: 0)

        XCTAssertTrue(presenter.isCalled(.presentSpot(spot: MockEntity.mockSpot())))
    }
}
