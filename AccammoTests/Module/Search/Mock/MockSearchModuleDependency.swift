
import Foundation
import UI
@testable import Search

struct MockSearchModuleDependency: SearchModuleDependency {

    var style: SearchModuleStyleProvider = MockSearchModuleStyleProvider()
    var resources: SearchModuleResourcesPovider = MockSearchModuleResourcesPovider()
}

struct MockSearchModuleStyleProvider: SearchModuleStyleProvider {
    var backgroundColor: UIColor = .clear
    var tintColor: UIColor = .clear
    var searchResultTitleColor: UIColor = .clear
    var searchResultFont: UIFont = UIFont.systemFont(ofSize: 1)
    var searchResultDescriptionTextColor: UIColor = .clear
    var searchResultDescriptionFont: UIFont = UIFont.systemFont(ofSize: 1)
    var wageTitleColor: UIColor = .clear
    var wageTitleFont: UIFont = UIFont.systemFont(ofSize: 1)
    var wageBaseColor: UIColor = .clear
    var wageBaseFont: UIFont = UIFont.systemFont(ofSize: 1)
}

struct MockSearchModuleResourcesPovider: SearchModuleResourcesPovider {

    var title: String = ""
    var searchButtonImage: UIImage = UIImage()
}
