
import Domain
import TestUtils
@testable import Search

final class MockSearchInteractor: SearchInteractorInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case search(searchCriteria: SearchCriteria)
        case fetchSearchResult(index: Int)
    }

    var invocations: [Invocation] = []

    func search(searchCriteria: SearchCriteria) {
        invocations.append(.search(searchCriteria: searchCriteria))
    }

    func fetchSearchResult(at index: Int) {
        invocations.append(.fetchSearchResult(index: index))
    }
}
