
import TestUtils
import Domain
@testable import Search

final class MockSearchModuleOutput: SearchModuleOutput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case handleTapOnSpot(spot: Spot)
        case handleTapOnSearch
    }

    var invocations: [Invocation] = []

    func handleTapOnSpot(_ spot: Spot) {
        invocations.append(.handleTapOnSpot(spot: spot))
    }

    func handleTapOnSearch() {
        invocations.append(.handleTapOnSearch)
    }
}
