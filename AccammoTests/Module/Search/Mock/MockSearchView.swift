
@testable import Search
import TestUtils

final class MockSearchView: SearchViewInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case showPreloader
        case hidePreloader
        case showEmptyState
        case showError(message: String)
        case update(viewModels: [SearchViewModel])
    }

    var invocations: [Invocation] = []

    func showPreloader() {
        invocations.append(.showPreloader)
    }

    func hidePreloader() {
        invocations.append(.hidePreloader)
    }

    func showEmptyState() {
        invocations.append(.showEmptyState)
    }

    func showError(message: String) {
        invocations.append(.showError(message: message))
    }

    func update(viewModels: [SearchViewModel]) {
        invocations.append(.update(viewModels: viewModels))
    }

}
