
import XCTest
@testable import Search

class SearchModuleBuilderTests: XCTestCase {

    // NOTE: setUp() & tearDown() are redundant here, since only one static method is being tested

    func testViewControllerAndItsOutput() {
        let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
        let dependency = MockSearchModuleDependency()
        let sut = SearchModuleBuilder.build(output: nil, searchCtiteria: searchCriteria, dependencies: dependency)
        let presenter = sut.controller.output

        XCTAssertTrue(presenter is SearchPresenter)
    }

    func testPresenterAndItsReferrals() {
        let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
        let dependency = MockSearchModuleDependency()
        let controller = SearchModuleBuilder.build(output: nil, searchCtiteria: searchCriteria, dependencies: dependency).controller
        let presenter = controller.output as! SearchPresenter

        XCTAssertTrue(presenter.interactor is SearchInteractor)
        XCTAssertTrue(presenter.router is SearchRouter)
        XCTAssertTrue(presenter.view === controller)
    }

    func testInteractorAndItsReferrals() {
        let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
        let dependency = MockSearchModuleDependency()
        let controller = SearchModuleBuilder.build(output: nil, searchCtiteria: searchCriteria, dependencies: dependency).controller
        let presenter = controller.output as! SearchPresenter
        let interactor = presenter.interactor as! SearchInteractor

        XCTAssertTrue(interactor.output === presenter)
    }
}
