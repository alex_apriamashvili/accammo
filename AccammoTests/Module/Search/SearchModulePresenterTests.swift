
import XCTest
import TestUtils
@testable import Search

class SearchModulePresenterTests: XCTestCase {

    var sut: SearchPresenter!
    var view: MockSearchView!
    var interactor: MockSearchInteractor!
    var output: MockSearchModuleOutput!
    var router: MockSearchRouter!

    private let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
    private let dependency = MockSearchModuleDependency()

    override func setUp() {
        super.setUp()

        sut = SearchPresenter(searchCriteria: searchCriteria, dependency: dependency)
        view = MockSearchView()
        interactor = MockSearchInteractor()
        output = MockSearchModuleOutput()
        router = MockSearchRouter()

        sut.view = view
        sut.interactor = interactor
        sut.output = output
        sut.router = router
    }
    
    override func tearDown() {
        sut = nil
        view = nil
        interactor = nil
        output = nil
        router = nil

        super.tearDown()
    }
    
    func testViewIsReadyWasCalled() {
        sut.viewIsReady()

        XCTAssertTrue(sut.viewLoaded)
        XCTAssertTrue(view.isCalled(.showPreloader))
        XCTAssertTrue(interactor.isCalled(.search(searchCriteria: searchCriteria)))
    }

    func testViewisReadyCalledWhileHavingAPendingController() {
        sut.present(login: UIViewController())
        sut.viewIsReady()

        XCTAssertTrue(router.isCalled(.present(viewController: UIViewController(), animated: false)))
        XCTAssertFalse(view.isCalled(.showPreloader))
        XCTAssertFalse(interactor.isCalled(.search(searchCriteria: searchCriteria)))
    }

    func testPresentLogin() {
        let vc = UIViewController()
        sut.present(login: vc)

        XCTAssertEqual(vc, sut.pendingViewController)
    }

    func testPresentDetail() {
        let vc = UIViewController()
        sut.show(detail: vc)

        XCTAssertTrue(router.isCalled(.push(viewController: vc)))
    }

    func testPresentFilter() {
        let vc = UIViewController()
        sut.show(filter: vc)

        XCTAssertTrue(router.isCalled(.push(viewController: vc)))
    }

    func testDismissAnyPresentedFlows() {
        sut.dismissAnyPresentedFlows()

        XCTAssertTrue(router.isCalled(.dismiss))
    }

    func testSetNeedsMakeNetworkCallWhileViewHasNotBeenLoadedYet() {
        sut.setNeedsMakeNetworkCall()

        XCTAssertNil(sut.pendingViewController)
        XCTAssertFalse(view.isCalled(.showPreloader))
        XCTAssertFalse(interactor.isCalled(.search(searchCriteria: searchCriteria)))
    }

    func testSetNeedsMakeNetworkCallWhenViewIsLoaded() {
        sut.viewIsReady()
        sut.setNeedsMakeNetworkCall()

        XCTAssertNil(sut.pendingViewController)
        XCTAssertTrue(view.isCalled(.showPreloader, times: 2))
        XCTAssertTrue(interactor.isCalled(.search(searchCriteria: searchCriteria), times: 2))
    }

    func testPresentResults() {
        sut.presentResults(spots: [MockEntity.mockSpot()])

        XCTAssertTrue(view.isCalled(.hidePreloader))
        XCTAssertTrue(view.isCalled(.update(viewModels: [])))
    }

    func testPresentEmptyResults() {
        sut.presentResults(spots: [])

        XCTAssertTrue(view.isCalled(.hidePreloader))
        XCTAssertTrue(view.isCalled(.showEmptyState))
    }

    func testPresentError() {
        sut.present(error: MockError.error)

        XCTAssertTrue(view.isCalled(.hidePreloader))
        XCTAssertTrue(view.isCalled(.showError(message: "")))
    }

    func testPresentSpot() {
        sut.present(spot: MockEntity.mockSpot())

        XCTAssertTrue(output.isCalled(.handleTapOnSpot(spot: MockEntity.mockSpot())))
    }

    func testHandleCellTap() {
        sut.handleCellTap(at: 0)

        XCTAssertTrue(interactor.isCalled(.fetchSearchResult(index: 0)))
    }
}
