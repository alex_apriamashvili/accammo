
import XCTest
import TestUtils
@testable import Profile

class ProfileModuleInteractorTests: XCTestCase {

    var sut: ProfileInteractor!
    var service: MockUserProfileService!
    var presenter: MockProfilePresenter!

    override func setUp() {
        super.setUp()
        service = MockUserProfileService()
        sut = ProfileInteractor(service: service)
        presenter = MockProfilePresenter()
        sut.output = presenter
    }

    override func tearDown() {
        service = nil
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func testServiceWasCalled() {
        sut.fetchUserDetails(token: String())

        XCTAssertTrue(service.isCalled(.fetchUserInfo))
    }

    func testResultIsSentToTheOutput() {
        sut.fetchUserDetails(token: String())

        XCTAssertTrue(presenter.isCalled(.presentProfile(profile: MockEntity.mockProfile())))
    }

    func testReturnError() {
        service.stubProfileFailure = true
        sut.fetchUserDetails(token: String())

        XCTAssertTrue(presenter.isCalled(.presentError(error: MockError.error)))
    }

    func testNotifyAboutLoggedOut() {
        sut.doLogout(token: String())

        XCTAssertTrue(service.isCalled(.logout))
    }
}
