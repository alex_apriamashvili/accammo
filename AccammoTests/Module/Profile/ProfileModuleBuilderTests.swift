
import XCTest
@testable import Profile

class ProfileModuleBuilderTests: XCTestCase {

    // NOTE: setUp() & tearDown() are redundant here, since only one static method is being tested

    func testViewControllerAndItsOutput() {
        let dependency = MockProfileModuleDependency()
        let sut = ProfileModuleBuilder.build(output: nil, dependencies: dependency)
        let presenter = sut.controller.output

        XCTAssertTrue(presenter is ProfilePresenter)
    }

    func testPresenterAndItsReferrals() {
        let dependency = MockProfileModuleDependency()
        let controller = ProfileModuleBuilder.build(output: nil, dependencies: dependency)
        let presenter = controller.controller.output as! ProfilePresenter

        XCTAssertTrue(presenter.interactor is ProfileInteractor)
        XCTAssertTrue(presenter.router is ProfileRouter)
        XCTAssertTrue(presenter.view === controller.controller)
    }

    func testInteractorAndItsReferrals() {
        let dependency = MockProfileModuleDependency()
        let controller = ProfileModuleBuilder.build(output: nil, dependencies: dependency)
        let presenter = controller.controller.output as! ProfilePresenter
        let interactor = presenter.interactor as! ProfileInteractor

        XCTAssertTrue(interactor.output === presenter)
    }
}
