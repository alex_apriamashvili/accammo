
import XCTest
import TestUtils
@testable import Profile

final class ProfileModulePresenterTests: XCTestCase {

    var sut: ProfilePresenter!
    var view: MockProfileView!
    var interactor: MockProfileInteractor!
    var output: MockProfileModuleOutput!
    var router: MockProfileRouter!

    private let dependency = MockProfileModuleDependency()

    override func setUp() {
        super.setUp()

        sut = ProfilePresenter(dependency: dependency)
        view = MockProfileView()
        interactor = MockProfileInteractor()
        output = MockProfileModuleOutput()
        router = MockProfileRouter()

        sut.view = view
        sut.interactor = interactor
        sut.output = output
        sut.router = router
    }
    
    override func tearDown() {
        sut = nil
        view = nil
        interactor = nil
        output = nil
        router = nil

        super.tearDown()
    }
    
    func testViewIsReadyWasCalled() {
        sut.viewIsReady()

        XCTAssertTrue(interactor.isCalled(.fetchUserDetails(token: "")))
    }

    func testPresentProfile() {
        sut.present(profile: MockEntity.mockProfile())

        XCTAssertTrue(view.isCalled(.updateProfile(profile: MockProfileViewModelContainer.viewModel())))
    }

    func testPresentError() {
        sut.present(error: MockError.error)

        XCTAssertTrue(view.isCalled(.showError(message: "")))
    }

    func testHandleTapOnLogout() {
        sut.handleTapOnLogoutButton()

        XCTAssertTrue(interactor.isCalled(.doLogout(token: String())))
        XCTAssertTrue(output.isCalled(.handleLogout))
    }

    func testHandleReloadAction() {
        sut.handleReloadAction()

        XCTAssertTrue(interactor.isCalled(.fetchUserDetails(token: String())))
    }

    func testShowLogin() {
        let flow = UIViewController()
        sut.show(login: flow)

        XCTAssertTrue(router.isCalled(.present(viewController: flow)))
    }

    func testDismissAllPresented() {
        sut.dismissAnyPresentedFlows()

        XCTAssertTrue(router.isCalled(.dismiss))
    }
}
