
import UI
@testable import Profile

struct MockProfileModuleDependency: ProfileModuleDependency {
    var token: String = ""
    var style: ProfileModuleStyleProvider = MockProfileModuleStyleProvider()
    var resources: ProfileModuleResourcesPovider = MockProfileModuleResourcesPovider()
}


struct MockProfileModuleStyleProvider: ProfileModuleStyleProvider {

    var backgroundColor: UIColor = .clear

    let tintColor: UIColor = .clear

    var logoutTintColor: UIColor = .clear

    var titleColor: UIColor = .clear

    var titleFont: UIFont = UIFont.systemFont(ofSize: 1)

    var descriptionTextColor: UIColor = .clear

    var descriptionFont: UIFont = UIFont.systemFont(ofSize: 1)


}

struct MockProfileModuleResourcesPovider: ProfileModuleResourcesPovider {

    var moduleLoadingTitle: String = String()

    var logoutIconImage: UIImage = UIImage()


}
