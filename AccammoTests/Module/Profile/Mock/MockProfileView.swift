
import TestUtils
@testable import Profile

final class MockProfileView: ProfileViewInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case updateProfile(profile: ProfileViewModel)
        case showError(message: String)
    }

    var invocations: [Invocation] = []

    func update(profile: ProfileViewModel) {
        invocations.append(.updateProfile(profile: profile))
    }

    func showError(message: String) {
        invocations.append(.showError(message: message))
    }
}
