
import TestUtils
@testable import Profile

final class MockProfileModuleOutput: ProfileModuleOutput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case handleLogout
    }

    var invocations: [Invocation] = []

    func handleLogout() {
        invocations.append(.handleLogout)
    }
}
