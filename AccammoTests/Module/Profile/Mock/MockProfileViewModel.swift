
import Foundation
@testable import Profile

final class MockProfileViewModelContainer {

    static func viewModel() -> ProfileViewModel {
        return ProfileViewModel(
            header: ProfileViewModel.Header(
                imageURL: URL(string: "http://ya.ru")!,
                imageSizeRatio: 1
            ),
            info: [],
            style: MockProfileModuleStyleProvider()
        )
    }
}
