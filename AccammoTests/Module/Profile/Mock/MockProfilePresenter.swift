
import TestUtils
import Domain
@testable import Profile

typealias MockProfilePresenterComitments =  ProfileInteractorOutput &
                                            ProfileViewOutput &
                                            ProfileModuleInput &
                                            ProfileCollectionViewSourceActionHandler

final class MockProfilePresenter: MockProfilePresenterComitments, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case handleCellTap(index: Int)
        case presentProfile(profile: Profile)
        case presentError(error: Error)
        case viewIsReady
        case handleTapOnLogoutButton
        case handleReloadAction
        case showLogin(flow: UIViewController)
        case dismissAnyPresentedFlows
    }

    var invocations: [Invocation] = []

    func handleCellTap(at index: Int) {
        invocations.append(.handleCellTap(index: index))
    }

    func present(profile: Profile) {
        invocations.append(.presentProfile(profile: profile))
    }

    func present(error: Error) {
        invocations.append(.presentError(error: error))
    }

    func viewIsReady() {
        invocations.append(.viewIsReady)
    }

    func handleTapOnLogoutButton() {
        invocations.append(.handleTapOnLogoutButton)
    }

    func handleReloadAction() {
        invocations.append(.handleReloadAction)
    }

    func show(login flow: UIViewController) {
        invocations.append(.showLogin(flow: flow))
    }


    func dismissAnyPresentedFlows() {
        invocations.append(.dismissAnyPresentedFlows)
    }
}
