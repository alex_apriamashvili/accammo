
import UIKit
import TestUtils
@testable import Profile

final class MockProfileRouter: ProfileRouterInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case dismiss
        case present(viewController: UIViewController)
        case push(viewController: UIViewController)
    }

    var invocations: [Invocation] = []

    func dismiss() {
        invocations.append(.dismiss)
    }

    func present(_ viewController: UIViewController) {
        invocations.append(.present(viewController: viewController))
    }

    func push(_ viewController: UIViewController) {
        invocations.append(.push(viewController: viewController))
    }
}
