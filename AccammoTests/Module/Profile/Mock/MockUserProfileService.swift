
@testable import Service
import TestUtils

final class MockUserProfileService: UserProfileService, MockInvocable {


    enum Invocation: MockInvocationEnum {
        case fetchUserInfo
        case logout
    }

    var invocations: [Invocation] = []

    var stubProfileResponse: UserDetailResponse = MockUserProfileService.mockResponse()
    var stubProfileFailure: Bool = false
    func fetchUserInfo(token: String,
                       success: @escaping UserProfileService.SuccessClosure,
                       failure: @escaping UserProfileService.FailureClosure) {
        invocations.append(.fetchUserInfo)
        if stubProfileFailure {
            failure(MockError.error)
        } else {
            success(stubProfileResponse)
        }
    }

    func logout(token: String) {
        invocations.append(.logout)
    }
}

private extension MockUserProfileService {

    static func mockResponse() -> UserDetailResponse {
        return UserDetailResponse(
            info: UserDetailResponse.UserInfo(
                imageURL: "http://ya.ru",
                imageW: 1,
                imageH: 1,
                expMonths: 1,
                fName: "test",
                lName: "test",
                title: "test",
                bio: "test"
            )
        )
    }
}
