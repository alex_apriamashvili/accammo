
import TestUtils
@testable import Profile

final class MockProfileInteractor: ProfileInteractorInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case fetchUserDetails(token: String)
        case doLogout(token: String)
    }

    var invocations: [Invocation] = []

    func fetchUserDetails(token: String) {
        invocations.append(.fetchUserDetails(token: token))
    }

    func doLogout(token: String) {
        invocations.append(.doLogout(token: token))
    }
}
