
import Foundation
import UI
@testable import TabBar


struct MockTabBarItemPresentable: TabBarItemPresentable {
    let title: String = ""
    var style: TabBarItemStyleProvider = MockTabBarItemStyleProvider()
    public var controller: UIViewController = UIViewController()
}

struct MockTabBarItemStyleProvider: TabBarItemStyleProvider {
    var image: UIImage = UIImage()
    var selectedImage: UIImage = UIImage()
    var activeColor: UIColor? = nil
    var inactiveColor: UIColor?  = nil
    var font: UIFont?  = nil
}
