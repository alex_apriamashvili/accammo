
import TestUtils
@testable import SpotDetail

final class MockSpotDetailView: SpotDetailViewInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case updateSpot(spot: SpotDetailViewModel)
        case showError(message: String)
    }

    var invocations: [Invocation] = []

    func update(spot: SpotDetailViewModel) {
        invocations.append(.updateSpot(spot: spot))
    }

    func showError(message: String) {
        invocations.append(.showError(message: message))
    }
}
