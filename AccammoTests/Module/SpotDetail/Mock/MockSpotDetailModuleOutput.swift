
import TestUtils
@testable import SpotDetail

final class MockSpotDetailModuleOutput: SpotDetailModuleOutput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case didFinishPresentation
    }

    var invocations: [Invocation] = []

    func didFinishPresentation() {
        invocations.append(.didFinishPresentation)
    }
}
