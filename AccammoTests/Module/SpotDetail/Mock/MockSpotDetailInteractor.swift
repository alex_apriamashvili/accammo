
import TestUtils
@testable import SpotDetail

final class MockSpotDetailInteractor: SpotDetailInteractorInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case fetchDetails(id: Int)
    }

    var invocations: [Invocation] = []

    func fetchDetails(id: Int) {
        invocations.append(.fetchDetails(id: id))
    }
}
