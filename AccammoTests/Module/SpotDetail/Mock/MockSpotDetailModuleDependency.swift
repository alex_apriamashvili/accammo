
import UI
@testable import SpotDetail

struct MockSpotDetailModuleDependency: SpotDetailModuleDependency {

    var style: SpotDetailModuleStyleProvider = MockSpotDetailModuleStyleProvider()
    var resources: SpotDetailModuleResourcesPovider = MockSpotDetailModuleResourcesPovider()
}


struct MockSpotDetailModuleStyleProvider: SpotDetailModuleStyleProvider {
    var elementTitleFont: UIFont = UIFont.systemFont(ofSize: 1)
    var elementTitleColor: UIColor = .clear
    var elementTextFont: UIFont = UIFont.systemFont(ofSize: 1)
    var elementTextColor: UIColor = .clear
    var wageAmountFont: UIFont = UIFont.systemFont(ofSize: 1)
    var wageAmountColor: UIColor = .clear
    var wageBaseFont: UIFont = UIFont.systemFont(ofSize: 1)
    var wageBaseColor: UIColor = .clear
    var ratingValueFont: UIFont = UIFont.systemFont(ofSize: 1)
    var ratingValueColor: UIColor = .clear
    var backgroundColor: UIColor = .clear
    let tintColor: UIColor = .clear
}

struct MockSpotDetailModuleResourcesPovider: SpotDetailModuleResourcesPovider {
    var descriptionTitle: String = ""
    var wageTitle: String = ""
    var ratingTitle: String = ""
}
