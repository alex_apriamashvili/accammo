
import TestUtils
import Domain
@testable import SpotDetail

typealias MockSpotDetailPresenterComitments =  SpotDetailInteractorOutput &
                                                SpotDetailViewOutput &
                                                SpotDetailModuleInput &
                                                SpotDetailCollectionViewSourceActionHandler

final class MockSpotDetailPresenter: MockSpotDetailPresenterComitments, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case handleCellTap(index: Int)
        case presentSpot(spot: SpotDetail)
        case presentError(error: Error)
        case viewIsReady
        case handleTapOnLogoutButton
        case handleReloadAction
        case handleDismiss
    }

    var invocations: [Invocation] = []

    func handleCellTap(at index: Int) {
        invocations.append(.handleCellTap(index: index))
    }

    func present(spot: SpotDetail) {
        invocations.append(.presentSpot(spot: spot))
    }

    func present(error: Error) {
        invocations.append(.presentError(error: error))
    }

    func viewIsReady() {
        invocations.append(.viewIsReady)
    }

    func handleTapOnLogoutButton() {
        invocations.append(.handleTapOnLogoutButton)
    }

    func handleReloadAction() {
        invocations.append(.handleReloadAction)
    }

    func handleDismiss() {
        invocations.append(.handleDismiss)
    }
}
