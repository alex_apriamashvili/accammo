
import Foundation
@testable import SpotDetail

final class MockSpotDetailViewModelContainer {

    static func viewModel() -> SpotDetailViewModel {
        return SpotDetailViewModel(
            spot: SpotDetailViewModel.Spot(
                title: "",
                description: "",
                wage: "",
                basis: "",
                imageURL: URL(string: "http://ya.ru")!,
                imageSize: .zero,
                type: "",
                rating: ""
            ),
            style: MockSpotDetailModuleStyleProvider(),
            resource: MockSpotDetailModuleResourcesPovider()
        )
    }
}
