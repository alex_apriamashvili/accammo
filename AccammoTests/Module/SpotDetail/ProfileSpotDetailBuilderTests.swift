
import XCTest
@testable import SpotDetail

class SpotDetailModuleBuilderTests: XCTestCase {

    // NOTE: setUp() & tearDown() are redundant here, since only one static method is being tested

    func testViewControllerAndItsOutput() {
        let dependency = MockSpotDetailModuleDependency()
        let sut = SpotDetailModuleBuilder.build(output: nil, spot: MockEntity.mockSpot(), dependencies: dependency)
        let presenter = sut.output

        XCTAssertTrue(presenter is SpotDetailPresenter)
    }

    func testPresenterAndItsReferrals() {
        let dependency = MockSpotDetailModuleDependency()
        let controller = SpotDetailModuleBuilder.build(output: nil, spot: MockEntity.mockSpot() , dependencies: dependency)
        let presenter = controller.output as! SpotDetailPresenter

        XCTAssertTrue(presenter.interactor is SpotDetailInteractor)
        XCTAssertTrue(presenter.router is SpotDetailRouter)
        XCTAssertTrue(presenter.view === controller)
    }

    func testInteractorAndItsReferrals() {
        let dependency = MockSpotDetailModuleDependency()
        let controller = SpotDetailModuleBuilder.build(output: nil, spot: MockEntity.mockSpot() , dependencies: dependency)
        let presenter = controller.output as! SpotDetailPresenter
        let interactor = presenter.interactor as! SpotDetailInteractor

        XCTAssertTrue(interactor.output === presenter)
    }
}
