
import XCTest
import TestUtils
@testable import SpotDetail

final class SpotDetailModulePresenterTests: XCTestCase {

    var sut: SpotDetailPresenter!
    var view: MockSpotDetailView!
    var interactor: MockSpotDetailInteractor!
    var output: MockSpotDetailModuleOutput!

    private let dependency = MockSpotDetailModuleDependency()

    override func setUp() {
        super.setUp()

        sut = SpotDetailPresenter(dependency: dependency, previewEntity: MockEntity.mockSpot())
        view = MockSpotDetailView()
        interactor = MockSpotDetailInteractor()
        output = MockSpotDetailModuleOutput()

        sut.view = view
        sut.interactor = interactor
        sut.output = output
    }
    
    override func tearDown() {
        sut = nil
        view = nil
        interactor = nil
        output = nil

        super.tearDown()
    }
    
    func testViewIsReadyWasCalled() {
        sut.viewIsReady()

        XCTAssertTrue(interactor.isCalled(.fetchDetails(id: 0)))
        XCTAssertTrue(view.isCalled(.updateSpot(spot: MockSpotDetailViewModelContainer.viewModel())))
    }

    func testHandleReloadAction() {
        sut.handleReloadAction()

        XCTAssertTrue(interactor.isCalled(.fetchDetails(id: 0)))
    }

    func testsHandleDismiss() {
        sut.handleDismiss()

        XCTAssertTrue(output!.isCalled(.didFinishPresentation))
    }

    func testPresentError() {
        sut.present(error: MockError.error)

        XCTAssertTrue(view.isCalled(.showError(message: "")))
    }

    func testPresentSpot() {
        sut.present(spot: MockEntity.mockSpotDetail())

        XCTAssertTrue(view.isCalled(.updateSpot(spot: MockSpotDetailViewModelContainer.viewModel())))
    }
}
