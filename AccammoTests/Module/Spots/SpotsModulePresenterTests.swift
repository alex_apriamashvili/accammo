
import XCTest
import TestUtils
@testable import Spots

class SpotsModulePresenterTests: XCTestCase {

    var sut: SpotsPresenter!
    var view: MockSpotsView!
    var interactor: MockSpotsInteractor!
    var output: MockSpotsModuleOutput!
    var router: MockSpotsRouter!

    private let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
    private let dependency = MockSpotsModuleDependency()

    override func setUp() {
        super.setUp()

        sut = SpotsPresenter(searchCriteria: searchCriteria, dependency: dependency)
        view = MockSpotsView()
        interactor = MockSpotsInteractor()
        output = MockSpotsModuleOutput()
        router = MockSpotsRouter()

        sut.view = view
        sut.interactor = interactor
        sut.output = output
        sut.router = router
    }
    
    override func tearDown() {
        sut = nil
        view = nil
        interactor = nil
        output = nil
        router = nil

        super.tearDown()
    }
    
    func testViewIsReadyWasCalled() {
        sut.viewIsReady()

        XCTAssertTrue(sut.viewLoaded)
        XCTAssertTrue(view.isCalled(.showPreloader))
        XCTAssertTrue(interactor.isCalled(.fetchRecommended))
    }

    func testViewisReadyCalledWhileHavingAPendingController() {
        sut.show(login: UIViewController())
        sut.viewIsReady()

        XCTAssertTrue(router.isCalled(.present(viewController: UIViewController(), animated: false)))
        XCTAssertFalse(view.isCalled(.showPreloader))
        XCTAssertFalse(interactor.isCalled(.fetchRecommended))
    }

    func testPresentLogin() {
        let vc = UIViewController()
        sut.show(login: vc)

        XCTAssertEqual(vc, sut.pendingViewController)
    }

    func testDismissAnyPresentedFlows() {
        sut.dismissAnyPresentedFlows()

        XCTAssertTrue(router.isCalled(.dismiss))
    }

    func testSetNeedsMakeNetworkCallWhileViewHasNotBeenLoadedYet() {
        sut.setNeedsMakeNetworkCall()

        XCTAssertNil(sut.pendingViewController)
        XCTAssertFalse(view.isCalled(.showPreloader))
        XCTAssertFalse(interactor.isCalled(.fetchRecommended))
    }

    func testSetNeedsMakeNetworkCallWhenViewIsLoaded() {
        sut.viewIsReady()
        sut.setNeedsMakeNetworkCall()

        XCTAssertNil(sut.pendingViewController)
        XCTAssertTrue(view.isCalled(.showPreloader, times: 2))
        XCTAssertTrue(interactor.isCalled(.fetchRecommended, times: 2))
    }

    func testPresentResults() {
        sut.presentResults(spots: [MockEntity.mockSpot()])

        XCTAssertTrue(view.isCalled(.hidePreloader))
        XCTAssertTrue(view.isCalled(.update(viewModels: [])))
    }

    func testPresentEmptyResults() {
        sut.presentResults(spots: [])

        XCTAssertTrue(view.isCalled(.hidePreloader))
        XCTAssertTrue(view.isCalled(.showEmptyState))
    }

    func testPresentError() {
        sut.present(error: MockError.error)

        XCTAssertTrue(view.isCalled(.hidePreloader))
        XCTAssertTrue(view.isCalled(.showError(message: "")))
    }

    func testPresentSpot() {
        sut.present(spot: MockEntity.mockSpot())

        XCTAssertTrue(output.isCalled(.handleTapOnSpot(spot: MockEntity.mockSpot())))
    }

    func testHandleCellTap() {
        sut.handleCellTap(at: 0)

        XCTAssertTrue(interactor.isCalled(.fetchSpot(index: 0)))
    }
}
