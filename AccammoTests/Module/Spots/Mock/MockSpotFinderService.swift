
import Service
import TestUtils

final class MockSpotFinderService: SpotFinderService, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case findSpots
        case recommendedSpots
    }

    var invocations: [Invocation] = []

    var stubSearchResponse: SearchResponse = MockSpotFinderService.mockResponse()
    var stubSearchFailure: Bool = false
    func findSpots(criteria: SpotSearchCriteriaModel,
                   success: @escaping (SearchResponse) -> (),
                   failure: @escaping (Error) -> ()) {
        invocations.append(.findSpots)
        if stubSearchFailure {
            failure(MockError.error)
        } else {
            success(stubSearchResponse)
        }
    }

    var stubRecommendedResponse: SearchResponse = MockSpotFinderService.mockResponse()
    var stubRecommendedFailure: Bool = false
    func recommendedSpots(success: @escaping SpotFinderService.SuccessClosure, failure: @escaping SpotFinderService.FailureClosure) {
        invocations.append(.recommendedSpots)
        if stubRecommendedFailure {
            failure(MockError.error)
        } else {
            success(stubSearchResponse)
        }
    }
}

private extension MockSpotFinderService {

    static func mockResponse() -> SearchResponse {
        return SpotSearchResponse(
            spotList: [
                SpotSearchResponse.Spot(
                    id: 0,
                    name: "",
                    description: "",
                    type: .apartment,
                    rating: 0.0,
                    image: SpotSearchResponse.Spot.Image(url: "http://ya.ru", width: 1, height: 1),
                    wage: SpotSearchResponse.Spot.Wage(
                        amount: 100,
                        currency: SpotSearchResponse.Spot.Wage.Currency(code: "", symbol: ""),
                        basis: .day
                    )
                )
            ]
        )
    }
}
