
import UIKit
import TestUtils
@testable import Spots

final class MockSpotsRouter: SpotsRouterInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case dismiss
        case present(viewController: UIViewController, animated: Bool)
        case push(viewController: UIViewController)
    }

    var invocations: [Invocation] = []

    func dismiss() {
        invocations.append(.dismiss)
    }

    func present(_ viewController: UIViewController, animated: Bool) {
        invocations.append(.present(viewController: viewController, animated: animated))
    }

    func push(_ viewController: UIViewController) {
        invocations.append(.push(viewController: viewController))
    }
}
