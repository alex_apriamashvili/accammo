
import Domain
import TestUtils
@testable import Spots

final class MockSpotsInteractor: SpotsInteractorInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case fetchRecommended
        case fetchSpot(index: Int)
    }

    var invocations: [Invocation] = []

    func fetchRecommended() {
        invocations.append(.fetchRecommended)
    }

    func fetchSpot(at index: Int) {
        invocations.append(.fetchSpot(index: index))
    }
}
