
@testable import Spots
import TestUtils

final class MockSpotsView: SpotsViewInput, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case showPreloader
        case hidePreloader
        case showEmptyState
        case showError(message: String)
        case update(viewModels: [SpotViewModel])
    }

    var invocations: [Invocation] = []

    func showPreloader() {
        invocations.append(.showPreloader)
    }

    func hidePreloader() {
        invocations.append(.hidePreloader)
    }

    func showEmptyState() {
        invocations.append(.showEmptyState)
    }

    func showError(message: String) {
        invocations.append(.showError(message: message))
    }

    func update(viewModels: [SpotViewModel]) {
        invocations.append(.update(viewModels: viewModels))
    }

}
