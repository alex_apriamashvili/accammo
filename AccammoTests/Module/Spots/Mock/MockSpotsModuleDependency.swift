
import Foundation
import UI
@testable import Spots

struct MockSpotsModuleDependency: SpotsModuleDependency {

    var style: SpotsModuleStyleProvider = MockSpotsModuleStyleProvider()
    var resources: SpotsModuleResourcesPovider = MockSpotsModuleResourcesPovider()
}


struct MockSpotsModuleStyleProvider: SpotsModuleStyleProvider {

    var backgroundColor: UIColor = .clear
    var tintColor: UIColor = .clear
    var spotTitleColor: UIColor = .clear
    var spotTitleFont: UIFont = UIFont.systemFont(ofSize: 1)
    var spotDescriptionTextColor: UIColor = .clear
    var spotDescriptionFont: UIFont = UIFont.systemFont(ofSize: 1)
    var wageTitleColor: UIColor = .clear
    var wageTitleFont: UIFont = UIFont.systemFont(ofSize: 1)
    var wageBaseColor: UIColor = .clear
    var wageBaseFont: UIFont = UIFont.systemFont(ofSize: 1)
}

struct MockSpotsModuleResourcesPovider: SpotsModuleResourcesPovider {

    var title: String = ""
    var searchButtonImage: UIImage = UIImage()
}
