
import UIKit
import Domain
import TestUtils
@testable import Spots

typealias MockSpotsPresenterComitments =    SpotsInteractorOutput &
                                            SpotsViewOutput &
                                            SpotsModuleInput &
                                            SpotsCollectionViewSourceActionHandler

final class MockSpotsPresenter: MockSpotsPresenterComitments, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case presentResults(spots: [Spot])
        case presentError(error: Error)
        case presentSpot(spot: Spot)
        case viewIsReady
        case handleTapOnSearch
        case updateSearchCriteria(searchCriteria: SearchCriteria)
        case handleCellTapAt(index: Int)
        case reloadData
        case loadData
        case setNeedsMakeNetworkCall
        case showLogin(flow: UIViewController)
        case showDetail(flow: UIViewController)
        case dismissAnyPresentedFlows
    }

    var invocations: [Invocation] = []

    func presentResults(spots: [Spot]) {
        invocations.append(.presentResults(spots: spots))
    }

    func present(error: Error) {
        invocations.append(.presentError(error: error))
    }

    func present(spot: Spot) {
        invocations.append(.presentSpot(spot: spot))
    }

    func viewIsReady() {
        invocations.append(.viewIsReady)
    }

    func handleTapOnSearch() {
        invocations.append(.handleTapOnSearch)
    }

    func update(searchCriteria: SearchCriteria) {
        invocations.append(.updateSearchCriteria(searchCriteria: searchCriteria))
    }

    func handleCellTap(at index: Int) {
        invocations.append(.handleCellTapAt(index: index))
    }

    func reloadData() {
        invocations.append(.reloadData)
    }

    func loadData() {
        invocations.append(.loadData)
    }

    func setNeedsMakeNetworkCall() {
        invocations.append(.setNeedsMakeNetworkCall)
    }

    func show(detail flow: UIViewController) {
        invocations.append(.showLogin(flow: flow))
    }

    func show(login flow: UIViewController) {
        invocations.append(.showDetail(flow: flow))
    }

    func dismissAnyPresentedFlows() {
        invocations.append(.dismissAnyPresentedFlows)
    }
}
