
import XCTest
@testable import Spots

class SpotsModuleBuilderTests: XCTestCase {

    // NOTE: setUp() & tearDown() are redundant here, since only one static method is being tested

    func testViewControllerAndItsOutput() {
        let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
        let dependency = MockSpotsModuleDependency()
        let sut = SpotsModuleBuilder.build(output: nil, searchCtiteria: searchCriteria, dependencies: dependency)
        let presenter = sut.controller.output

        XCTAssertTrue(presenter is SpotsPresenter)
    }

    func testPresenterAndItsReferrals() {
        let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
        let dependency = MockSpotsModuleDependency()
        let controller = SpotsModuleBuilder.build(output: nil, searchCtiteria: searchCriteria, dependencies: dependency).controller
        let presenter = controller.output as! SpotsPresenter

        XCTAssertTrue(presenter.interactor is SpotsInteractor)
        XCTAssertTrue(presenter.router is SpotsRouter)
        XCTAssertTrue(presenter.view === controller)
    }

    func testInteractorAndItsReferrals() {
        let searchCriteria = MockSearchCriteriaContainer().currentSearchCriteria()
        let dependency = MockSpotsModuleDependency()
        let controller = SpotsModuleBuilder.build(output: nil, searchCtiteria: searchCriteria, dependencies: dependency).controller
        let presenter = controller.output as! SpotsPresenter
        let interactor = presenter.interactor as! SpotsInteractor

        XCTAssertTrue(interactor.output === presenter)
    }
}
