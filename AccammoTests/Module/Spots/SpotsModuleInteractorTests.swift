
import XCTest
import TestUtils
@testable import Spots

class SpotsModuleInteractorTests: XCTestCase {

    var sut: SpotsInteractor!
    var service: MockSpotFinderService!
    var presenter: MockSpotsPresenter!

    override func setUp() {
        super.setUp()
        service = MockSpotFinderService()
        sut = SpotsInteractor(service: service)
        presenter = MockSpotsPresenter()
        sut.output = presenter
    }

    override func tearDown() {
        service = nil
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func testServiceWasCalled() {
        sut.fetchRecommended()

        XCTAssertTrue(service.isCalled(.recommendedSpots))
    }

    func testResultIsSentToTheOutput() {
        sut.fetchRecommended()

        XCTAssertTrue(presenter.isCalled(.presentResults(spots: [])))
    }

    func testReturnError() {
        service.stubRecommendedFailure = true
        sut.fetchRecommended()

        XCTAssertTrue(presenter.isCalled(.presentError(error: MockError.error)))
    }

    func testCanFetchAnEntity() {
        sut.fetchRecommended()
        sut.fetchSpot(at: 0)

        XCTAssertTrue(presenter.isCalled(.presentSpot(spot: MockEntity.mockSpot())))
    }
}
