
import XCTest
@testable import Service


class LoginServiceTests: XCTestCase {

    var sut: LoginServiceImpl!
    var client: APIClientMock!

    override func setUp() {
        super.setUp()

        client = APIClientMock()
        sut = LoginServiceImpl(client: client)

    }

    override func tearDown() {
        client = nil
        sut = nil

        super.tearDown()
    }

    func testAPIClientGotCalledWhenFetchingUserInfo() {
        sut.login(credentials: AuthorizationParams(login: String(), password: String()), success: { _ in }) { _ in }

        XCTAssertTrue(client.isCalled(.post))
    }
}

