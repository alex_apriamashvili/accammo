
import XCTest
@testable import Service

class SpotFinderServiceTests: XCTestCase {

    var sut: SpotFinderServiceImpl!
    var client: APIClientMock!

    override func setUp() {
        super.setUp()

        client = APIClientMock()
        sut = SpotFinderServiceImpl(client: client)

    }
    
    override func tearDown() {
        client = nil
        sut = nil

        super.tearDown()
    }
    
    func testAPIClientGotCalledWhenLookingForSpots() {
        sut.findSpots(criteria: SpotSearchCriteriaModel(), success: { _ in }) { _ in }

        XCTAssertTrue(client.isCalled(.post))
    }

    func testAPIClientGotCalledWhenLookingForRecommended() {
        sut.recommendedSpots(success: { _ in }) { _ in }

        XCTAssertTrue(client.isCalled(.post))
    }
}
