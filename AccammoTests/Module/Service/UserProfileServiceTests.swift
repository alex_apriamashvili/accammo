
import XCTest
import Service
@testable import Accammo

class UserProfileServiceTests: XCTestCase {
    
    var sut: UserProfileServiceImpl!
    var client: APIClientMock!

    override func setUp() {
        super.setUp()

        client = APIClientMock()
        sut = UserProfileServiceImpl(client: client)

    }

    override func tearDown() {
        client = nil
        sut = nil

        super.tearDown()
    }

    func testAPIClientGotCalledWhenFetchingUserInfo() {
        sut.fetchUserInfo(token: String(), success: { _ in }) { _ in }

        XCTAssertTrue(client.isCalled(.post))
    }

    func testAPIClientGotCalledWhenSentingLogoutEvent() {
        sut.logout(token: String())

        XCTAssertTrue(client.isCalled(.post))
    }
}
