
import Service
import TestUtils

final class APIClientMock: APIClient, MockInvocable {

    enum Invocation: MockInvocationEnum {
        case get
        case post
        case put
    }

    var invocations: [Invocation] = []

    let baseURL: String = ""

    func post<R: Codable>(path: String,
                          parameters: Parameters?,
                          responseType: R.Type,
                          success: @escaping SuccessClosure<R>,
                          failure: @escaping FailureClosure) {
        invocations.append(.post)
    }

    func get<R: Codable>(path: String,
                         parameters: APIClient.Parameters?,
                         responseType: R.Type,
                         success: @escaping (R) -> (),
                         failure: @escaping APIClient.FailureClosure) {
        invocations.append(.get)
    }

    func put<R: Codable>(path: String,
                         parameters: APIClient.Parameters?,
                         responseType: R.Type,
                         success: @escaping (R) -> (),
                         failure: @escaping APIClient.FailureClosure) {
        invocations.append(.put)
    }
}
