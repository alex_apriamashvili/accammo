
import UI
import Domain
import Spots

// MARK: - Implementation

final class SpotCoordinator: Coordinator {

    typealias SpotCoordinatorDependency = HasSearchCriteriaContainer & HasAppState
    typealias Params = SpotCoordinatorDependency

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    private var params: Params?
    private var module: SpotsModule?

    func start(parameters: Params) -> Flow {
        params = parameters
        return connectSpotsModule()
    }
}

// MARK: - Private

private extension SpotCoordinator {

    func connectSpotsModule() -> UIViewController {
        guard let params = params else { fatalError("the required params are missing. unable to create a module") }
        let spotsModule = SpotsModuleBuilder.build(
            output: self,
            searchCtiteria: params.searchCriteriaContainer.currentSearchCriteria(),
            dependencies: SpotsModuleDependencyImpl()
        )
        module = spotsModule
        interceptModulePresentetionIfNeeded(spotsModule)
        return NavigationStackWrapper.wrap(spotsModule.controller, large: true)
    }

    func interceptModulePresentetionIfNeeded(_ module: SpotsModule) {
        guard let userState = params?.appState.userState else { return }
        switch userState {
        case .unauthorized:
            let loginCoordinator = linkNode(LoginCoordinator.self)
            let loginFlow = loginCoordinator.start(parameters: nil)
            module.input.show(login: loginFlow)
        default:
            allowModuleToLoadData(module)
        }
    }

    func allowModuleToLoadData(_ module: SpotsModule) {
        module.input.setNeedsMakeNetworkCall()
    }

    func handleLoginOutput(_ output: LoginCompletionResult) {
        guard let params = params else { return }
        switch output {
        case let .success(token: token):
            params.appState.updateUser(state: .loggedIn(token: token))
            unlinkNode(LoginCoordinator.self)
            guard let tabBarCoordinator = parent as? TabBarCoordinator else { return }
            tabBarCoordinator.restart()
        case let .failure(message: message):
            print("the following error has occured while user was trying to log in: \(message)")
        }
    }
}

// MARK: - SpotsModuleOutput

extension SpotCoordinator: SpotsModuleOutput {

    func handleTapOnSpot(_ spot: Spot) {
        let coordinator = linkNode(SpotDetailCoordinator.self)
        let flow = coordinator.start(parameters: spot)
        module?.input.show(detail: flow)
    }
}

// MARK: - CoordinatorOutput

extension SpotCoordinator: CoordinatorOutput {

    func coordinator<C: Coordinator>(_ coordinator: C, didFinish result: CoordinatorResult) {
        switch (coordinator, result) {
        case (is LoginCoordinator, is LoginCompletionResult):
            handleLoginOutput(result as! LoginCompletionResult)
        case (is SpotDetailCoordinator, is SpotDetailCompletionResult):
            unlinkNode(SpotDetailCoordinator.self)
        default:
            print("unrecornized coordinator didFinish. [\(coordinator)]")
        }
    }
}

struct SpotCompletionResult: CoordinatorResult {}
