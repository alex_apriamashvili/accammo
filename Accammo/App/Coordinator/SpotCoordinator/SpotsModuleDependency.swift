
import Foundation
import UI
import Spots

struct SpotsModuleDependencyImpl: SpotsModuleDependency {

    let style: SpotsModuleStyleProvider = SpotsModuleStyleProviderImpl()
    let resources: SpotsModuleResourcesPovider = SpotsModuleResourcesPoviderImpl()
}

struct SpotsModuleStyleProviderImpl: SpotsModuleStyleProvider {

    let backgroundColor: UIColor = Style.Color.white
    let tintColor: UIColor = Style.Color.green

    // Cell
    let spotTitleColor: UIColor = Style.Color.black
    let spotTitleFont: UIFont = Style.Font.mediumTitle
    let spotDescriptionTextColor: UIColor = Style.Color.darkGray
    let spotDescriptionFont: UIFont = Style.Font.mediumText
    let wageTitleColor: UIColor = Style.Color.green
    let wageTitleFont: UIFont = Style.Font.emphasisedLargeText
    let wageBaseColor: UIColor = Style.Color.gray
    let wageBaseFont: UIFont = Style.Font.mediumSubtitle
}

struct SpotsModuleResourcesPoviderImpl: SpotsModuleResourcesPovider {

    let title: String = "For You"
}


