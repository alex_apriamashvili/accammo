
import UIKit
import Domain
import SpotDetail

// MARK: - Implementation

final class SpotDetailCoordinator: Coordinator {

    typealias Params = Spot

    private var params: Params?

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    func start(parameters: Params) -> Flow {
        params = parameters
        return connectSpotDetailModule()
    }
}

// MARK: - Private

private extension SpotDetailCoordinator {

    func connectSpotDetailModule() -> UIViewController {
        guard let spot = params else { fatalError("the required params are missing. unable to create a module") }
        return SpotDetailModuleBuilder.build(
            output: self,
            spot: spot,
            dependencies: SpotDetailModuleDependencyImpl()
        )
    }
}

// MARK: - SpotDetailModuleOutput

extension SpotDetailCoordinator: SpotDetailModuleOutput {

    func didFinishPresentation() {
        guard let output = parent as? CoordinatorOutput else { return }
        output.coordinator(self, didFinish: SpotDetailCompletionResult())
    }
}

struct SpotDetailCompletionResult: CoordinatorResult {}
