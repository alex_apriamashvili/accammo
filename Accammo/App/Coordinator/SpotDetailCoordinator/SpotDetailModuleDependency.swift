
import UI
import SpotDetail

struct SpotDetailModuleDependencyImpl: SpotDetailModuleDependency {

    let style: SpotDetailModuleStyleProvider = SpotDetailModuleStyleProviderImpl()
    let resources: SpotDetailModuleResourcesPovider = SpotDetailModuleResourcesPoviderImpl()
}

struct SpotDetailModuleStyleProviderImpl: SpotDetailModuleStyleProvider {

    let backgroundColor: UIColor = Style.Color.white
    let tintColor: UIColor = Style.Color.green

    // Cells

    let elementTitleFont: UIFont = Style.Font.hugeTitle
    let elementTitleColor: UIColor = Style.Color.black
    let elementTextFont: UIFont = Style.Font.regularText
    let elementTextColor: UIColor = Style.Color.darkGray
    let wageAmountFont: UIFont = Style.Font.mediumTitle
    let wageAmountColor: UIColor = Style.Color.green
    let wageBaseFont: UIFont = Style.Font.mediumSubtitle
    let wageBaseColor: UIColor = Style.Color.gray
    let ratingValueFont: UIFont = Style.Font.mediumTitle
    let ratingValueColor: UIColor = Style.Color.red
}

struct SpotDetailModuleResourcesPoviderImpl: SpotDetailModuleResourcesPovider {

    let descriptionTitle: String = "Description"
    let wageTitle: String = "Wage"
    let ratingTitle: String = "Rating"
}


