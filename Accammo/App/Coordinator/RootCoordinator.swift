import Foundation
import UIKit

// MARK: - Implementation

final class RootCoordinator: Coordinator {

    init() {}

    typealias Params = Void?

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []
    weak var window: UIWindow?

    init(window: UIWindow?) {
        self.window = window
    }

    @discardableResult
    func start(parameters: Params) -> Flow {
        pickCoordinator(params: parameters)

        return UIViewController()
    }

    func restart() {
        nodes = []
        start(parameters: nil)
    }
}

// MARK: - Private

private extension RootCoordinator {

    /// here is the place where the whole App routing logic is being set up.
    /// depending on the configuration or the experiment, the default tabBar Coordinator
    /// could be easily replaced by any other coordinator
    func pickCoordinator(params: Params) {
        let tabBarCoordinator = TabBarCoordinator()
        tabBarCoordinator.parent = self

        let rootViewController = tabBarCoordinator.start(parameters: TabBarCoordinatorDependenciesImpl())
        attachToWindow(rootViewController: rootViewController)

        nodes = [tabBarCoordinator]
    }

    func attachToWindow(rootViewController: UIViewController) {
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
}

// MARK: TabBarCoordinator Output

extension RootCoordinator: CoordinatorOutput {

    func coordinator<C: Coordinator>(_ coordinator: C, didFinish result: CoordinatorResult) {
        guard coordinator is TabBarCoordinator,
            let result = result as? TabarCompletionResult else { return }
        print("the last viewed tab was: \(result.lastViewedTab)")
    }
}

