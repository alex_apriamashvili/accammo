
import UI
import Login

struct LoginModuleDependencyImpl: LoginModuleDependency {
    var resources: LoginModuleResourcesPovider = LoginModuleResourcesPoviderImpl()
    var style: LoginModuleStyleProvider = LoginModuleStyleProviderImpl()
}

struct LoginModuleResourcesPoviderImpl: LoginModuleResourcesPovider {

    let backgroundImage: UIImage = #imageLiteral(resourceName: "login_background")
    let logoImage: UIImage = #imageLiteral(resourceName: "accammo-logo")
    let loginPlaceholderText: String = "Login"
    let handlePlaceholderText: String = "Password"
}

struct LoginModuleStyleProviderImpl: LoginModuleStyleProvider {

    let backgroundColor: UIColor = Style.Color.white
    let tintColor: UIColor = Style.Color.green
    let inputFont: UIFont = Style.Font.hugeInput
    let inputTextColor: UIColor = Style.Color.black
    let overlayColor: UIColor = Style.Color.black.withAlphaComponent(0.8)
}
