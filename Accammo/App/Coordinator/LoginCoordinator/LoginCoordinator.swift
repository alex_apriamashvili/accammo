
import UIKit
import Login

// MARK: - Implementation

final class LoginCoordinator: Coordinator {

    typealias Params = Void?

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    func start(parameters: Params) -> Flow {
        return connectLoginModule(params: parameters)
    }
}

// MARK: - Private

private extension LoginCoordinator {

    func connectLoginModule(params: Params) -> UIViewController {
        return LoginModuleBuilder.build(output: self, dependencies: LoginModuleDependencyImpl())
    }
}

// MARK: - LoginModuleOutput

extension LoginCoordinator: LoginModuleOutput {

    func handleLogin(token: String) {
        let result = LoginCompletionResult.success(token: token)
        guard let output = parent as? CoordinatorOutput else { return }
        output.coordinator(self, didFinish: result)
    }
}

enum LoginCompletionResult: CoordinatorResult {
    case success(token: String)
    case failure(message: String)
}
