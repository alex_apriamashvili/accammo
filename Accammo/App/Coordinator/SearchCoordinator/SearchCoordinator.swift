
import UI
import Domain
import Search

// MARK: - Implementation

final class SearchCoordinator: Coordinator {

    typealias SearchCoordinatorDependency = HasSearchCriteriaContainer & HasAppState
    typealias Params = SearchCoordinatorDependency

    private var params: Params?
    private var module: SearchModule?

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    func start(parameters: Params) -> Flow {
        params = parameters
        return connectSearchModule()
    }
}

// MARK: - Private

private extension SearchCoordinator {

    func connectSearchModule() -> UIViewController {
        guard let params = params else { fatalError("the required params are missing. unable to create a module") }
        let searchModule = SearchModuleBuilder.build(
            output: self,
            searchCtiteria: params.searchCriteriaContainer.currentSearchCriteria(),
            dependencies: SearchModuleDependencyImpl()
        )
        module = searchModule
        return NavigationStackWrapper.wrap(searchModule.controller, large: false)
    }

    func handleFilter(result: FilterCompletionResult) {
        if let newFilter = result.filter, let params = params {
            module?.input.dismissAnyPresentedFlows()
            params.searchCriteriaContainer.update(filter: newFilter)
            module?.input.update(searchCriteria: params.searchCriteriaContainer.currentSearchCriteria())
        }
        unlinkNode(FilterCoordinator.self)
    }
}

// MARK: - SearchModuleOutput

extension SearchCoordinator: SearchModuleOutput {

    func handleTapOnSearch() {
        guard let filter = params?.searchCriteriaContainer.currentSearchCriteria().filter else { return }
        let filterCoordinator = linkNode(FilterCoordinator.self)
        let flow = filterCoordinator.start(parameters: filter)
        module?.input.show(filter: flow)
    }

    func handleTapOnSpot(_ spot: Spot) {
        let coordinator = linkNode(SpotDetailCoordinator.self)
        let flow = coordinator.start(parameters: spot)
        module?.input.show(detail: flow)
    }
}

extension SearchCoordinator: CoordinatorOutput {

    func coordinator<C: Coordinator>(_ coordinator: C, didFinish result: CoordinatorResult) {
        switch (coordinator, result) {
        case (is FilterCoordinator, is FilterCompletionResult):
            handleFilter(result: result as! FilterCompletionResult)
        case (is SpotDetailCoordinator, is SpotDetailCompletionResult):
            unlinkNode(SpotDetailCoordinator.self)
        default:
            print("unrecornized coordinator didFinish. [\(coordinator)]")
        }
    }
}

struct SearchCompletionResult: CoordinatorResult {}
