
import UI
import Search

struct SearchModuleDependencyImpl: SearchModuleDependency {

    let style: SearchModuleStyleProvider = SearchModuleStyleProviderImpl()
    let resources: SearchModuleResourcesPovider = SearchModuleResourcesPoviderImpl()
}

struct SearchModuleStyleProviderImpl: SearchModuleStyleProvider {

    let backgroundColor: UIColor = Style.Color.white
    let tintColor: UIColor = Style.Color.green

    // Cell
    let searchResultTitleColor: UIColor = Style.Color.black
    let searchResultFont: UIFont = Style.Font.mediumTitle
    let searchResultDescriptionTextColor: UIColor = Style.Color.darkGray
    let searchResultDescriptionFont: UIFont = Style.Font.mediumText
    let wageTitleColor: UIColor = Style.Color.green
    let wageTitleFont: UIFont = Style.Font.emphasisedLargeText
    let wageBaseColor: UIColor = Style.Color.gray
    let wageBaseFont: UIFont = Style.Font.mediumSubtitle
}

struct SearchModuleResourcesPoviderImpl: SearchModuleResourcesPovider {

    let title: String = "Search"
    let searchButtonImage: UIImage = #imageLiteral(resourceName: "search")
}


