
import Profile
import UI

struct PofileModuleDependencyImpl: ProfileModuleDependency {

    let token: String
    let style: ProfileModuleStyleProvider = ProfileStyleProviderImpl()
    let resources: ProfileModuleResourcesPovider = ProfileModuleResourcesPoviderImpl()
}

struct ProfileStyleProviderImpl: ProfileModuleStyleProvider {

    let backgroundColor: UIColor = Style.Color.white
    let tintColor: UIColor = Style.Color.green
    let logoutTintColor: UIColor = Style.Color.red

    // Info Cell
    var titleColor: UIColor = Style.Color.darkGray
    var titleFont: UIFont = Style.Font.smallTitle
    var descriptionTextColor: UIColor = Style.Color.black
    var descriptionFont: UIFont = Style.Font.regularText
}

struct ProfileModuleResourcesPoviderImpl: ProfileModuleResourcesPovider {

    let moduleLoadingTitle: String = "Profile"
    let logoutIconImage: UIImage = #imageLiteral(resourceName: "log_out")
}
