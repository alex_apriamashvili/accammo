
import UI
import Profile

// MARK: - Implementation

final class ProfileCoordinator: Coordinator {

    typealias Params = HasAppState

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    private var module: ProfileModule?
    private var params: Params?

    func start(parameters: Params) -> Flow {
        params = parameters
        return connectProfileModule(params: parameters)
    }
}

// MARK: - Private

private extension ProfileCoordinator {

    func connectProfileModule(params: Params) -> UIViewController {
        guard case let .loggedIn(token: token) = params.appState.userState else { return UIViewController() }
        let profileModule = ProfileModuleBuilder.build(
            output: self,
            dependencies: PofileModuleDependencyImpl(token: token)
        )
        module = profileModule
        return NavigationStackWrapper.wrap(profileModule.controller, large: false)
    }
}

// MARK: - ProfileModuleOutput

extension ProfileCoordinator: ProfileModuleOutput {

    func handleLogout() {
        guard let params = params else { return }
        params.appState.updateUser(state: .unauthorized)
        let coordinator = linkNode(LoginCoordinator.self)
        let flow = coordinator.start(parameters: nil)
        module?.input.show(login: flow)
    }
}

extension ProfileCoordinator: CoordinatorOutput {

    func coordinator<C: Coordinator>(_ coordinator: C, didFinish result: CoordinatorResult) {
        guard let loginResult = result as? LoginCompletionResult,
            let rootCoordinator = parent as? TabBarCoordinator,
            let params = params else { return }
        switch loginResult {
        case let .success(token: token):
            params.appState.updateUser(state: .loggedIn(token: token))
            module?.input.dismissAnyPresentedFlows()
            unlinkNode(LoginCoordinator.self)
            rootCoordinator.restart()
        default: return
        }

    }
}

struct ProfileCompletionResult {}

