
import UIKit
import Domain
import SearchFilter

// MARK: - Implementation

final class FilterCoordinator: Coordinator {

    typealias Params = SearchCriteria.Filter

    private var params: Params?

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    func start(parameters: Params) -> Flow {
        params = parameters
        return connectFilterModule()
    }
}

// MARK: - Private

private extension FilterCoordinator {

    func connectFilterModule() -> UIViewController {
        guard let filter = params else { fatalError("the required params are missing. unable to create a module") }
        return SearchFilterModuleBuilder(
            dependencies: SearchFilterModuleDependenciesImpl()
            ).build(with: self, filter: filter)
    }
}

// MARK: - FilterModuleOutput

extension FilterCoordinator: SearchFilterModuleOutput {

    func applyFilter(_ filter: SearchCriteria.Filter) {
        guard let output = parent as? CoordinatorOutput else { return }
        output.coordinator(self, didFinish: FilterCompletionResult(filter: filter))
    }

    func dismissWithoutAnyChanges() {
        guard let output = parent as? CoordinatorOutput else { return }
        output.coordinator(self, didFinish: FilterCompletionResult(filter: nil))
    }
}

struct FilterCompletionResult: CoordinatorResult {

    let filter: SearchCriteria.Filter?
}
