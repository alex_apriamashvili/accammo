
import UI
import SearchFilter

struct SearchFilterModuleDependenciesImpl: SearchFilterModuleDependencies {

    let collectionViewAdapter: CollectionViewAdapter = CollectionViewAdapter(dependencies: UIComponentsDifferDependencies())
    let styleProvider: SearchFilterStyleProvider = SearchFilterStyleProviderImpl()
    let translationProvider: SearchFilterTranslationProvider = SearchFilterTranslationProviderImpl()
}

struct SearchFilterStyleProviderImpl: SearchFilterStyleProvider {

    let starImage: UIImage = #imageLiteral(resourceName: "star")
    let backgroundColor: UIColor = Style.Color.white
    let blueColor: UIColor = Style.Color.green
    let tintColor: UIColor = Style.Color.green
    let grayTextColor: UIColor = Style.Color.darkGray
    let imageTintColor: UIColor = Style.Color.black
    let lightBlueColor: UIColor = Style.Color.white
    let lightGrayColor: UIColor = Style.Color.gray
    let selectedElementColor: UIColor = Style.Color.green
    let selectedTextColor: UIColor = Style.Color.white
    let textColor: UIColor = Style.Color.black
    let whiteColor: UIColor = Style.Color.white
    let smallBoldFont: UIFont = Style.Font.smallTitle
    let mediumLightFont: UIFont = Style.Font.hugeInput
    let mediumBoldFont: UIFont = Style.Font.mediumTitle
    let bigBoldFont: UIFont = Style.Font.hugeInput
    let veryBigLightFont: UIFont = Style.Font.hugeTitle
    let starButtonRadii: CGFloat = 3
    let cellWidth: CGFloat = UIScreen.main.bounds.size.width
}

struct SearchFilterTranslationProviderImpl: SearchFilterTranslationProvider {

    let screenTitle: String = "Filter"
    let filterBySpotNameText: String = "Spot Name"
    let spotNamePlaceholderText: String = "Studio X8"
    let starRatingTitle: String = "Rating"
    let wageTitle: String = "Wage per hour"
    let skillsTitle: String = "Skills"
    let filterText: String = "Filter"
}
