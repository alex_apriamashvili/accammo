
import Foundation
import UI
import TabBar

struct TabBarModuleDependenciesImpl: TabBarModuleDependencies {

    let resource: TabBarResourceProvider = TabBarResourceProviderImpl()
    let style: TabBarStyleProvider = TabBarStyleProviderImpl()
}

struct TabBarResourceProviderImpl: TabBarResourceProvider {

    private struct Identifiers {
        static let tabBarStoryboard = "TabBarViewController"
    }

    var tabBarControllerStoryboard: UIStoryboard {
            return .init(name: Identifiers.tabBarStoryboard, bundle: Bundle.main)
    }
}

struct TabBarStyleProviderImpl: TabBarStyleProvider {

    var font: UIFont = Style.Font.smallTitle
    var textOffset: UIOffset = UIOffset(horizontal: 0, vertical: -6)
    var activeColor: UIColor = Style.Color.green
    var inactiveColor: UIColor = Style.Color.black
    var shouldShowLine: Bool = true
    var lineColor: UIColor = Style.Color.green
}

struct TabBarItemPresentableImpl: TabBarItemPresentable {

    let title: String
    let style: TabBarItemStyleProvider
    let controller: UIViewController
}

struct TabBarItemStyleProviderImpl: TabBarItemStyleProvider {

    let image: UIImage
    let selectedImage: UIImage
    var activeColor: UIColor?
    var inactiveColor: UIColor?
    var font: UIFont?

    init(image: UIImage,
         selectedImage: UIImage,
         activeColor: UIColor? = nil,
         inactiveColor: UIColor? = nil,
         font: UIFont? = nil) {
        self.image = image
        self.selectedImage = selectedImage
        self.activeColor = activeColor
        self.inactiveColor = inactiveColor
        self.font = font
    }
}
