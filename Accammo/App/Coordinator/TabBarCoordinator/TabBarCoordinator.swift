
import UIKit
import TabBar

// MARK: - Implementation

final class TabBarCoordinator: Coordinator {

    typealias Params = TabBarCoordinatorDependencies

    weak var parent: CoordinatorNode?
    var nodes: [CoordinatorNode] = []

    private var params: Params?
    private var tabs: [TabBarItemPresentable] = []
    private var tabBarModule: TabBarModule?

    func start(parameters: Params) -> Flow {
        params = parameters
        attachNodes()
        guard let tabBarModule = createTabBar(items: tabs) else { fatalError("unable to create a TabBarController") }
        self.tabBarModule = tabBarModule

        return tabBarModule
    }

    func restart() {
        nodes = []
        tabs = []
        guard let rootCoordinator = parent as? RootCoordinator else { return }
        rootCoordinator.restart()
    }

}

// MARK: - Private

private extension TabBarCoordinator {

    func attachNodes() {
        let spots = spotsTuple()
        let search = searchTuple()
        let profile = profileTuple()

        nodes = [spots.node, search.node, profile.node]
        tabs = [spots.tab, search.tab, profile.tab]
    }

    func createTabBar(items: [TabBarItemPresentable]) -> TabBarModule? {
        return TabBarModuleBuilder.build(with: items, dependencies: TabBarModuleDependenciesImpl())
    }
}

// MARK: - Child Coordinator Tuples

private extension TabBarCoordinator {

    typealias Tuple = (node: CoordinatorNode, tab: TabBarItemPresentable)

    func spotsTuple() -> Tuple {
        guard let params = params else { fatalError("TabBarCoordinator params are empty") }
        let node = linkNode(SpotCoordinator.self)
        let flow = run(spots: node)
        let tab = params.tabFactory.tab(coordinator: node, flow: flow)

        return (node: node, tab: tab)
    }

    func searchTuple() -> Tuple {
        guard let params = params else { fatalError("TabBarCoordinator params are empty") }
        let node = linkNode(SearchCoordinator.self)
        let flow = run(search: node)
        let tab = params.tabFactory.tab(coordinator: node, flow: flow)

        return (node: node, tab: tab)
    }

    func profileTuple() -> Tuple {
        guard let params = params else { fatalError("TabBarCoordinator params are empty") }
        let node = linkNode(ProfileCoordinator.self)
        let flow = run(profile: node)
        let tab = params.tabFactory.tab(coordinator: node, flow: flow)

        return (node: node, tab: tab)
    }
}

// MARK: - Coordinators Runner

private extension TabBarCoordinator {

    func run(spots: SpotCoordinator) -> Flow {
        guard let params = params else {
            fatalError("dependencies needed to run `SpotCoordinator` are missing.")
        }
        return spots.start(parameters: params)
    }

    func run(search: SearchCoordinator) -> Flow {
        guard let params = params else {
            fatalError("dependencies needed to run `SearchCoordinator` are missing.")
        }
        return search.start(parameters: params)
    }

    func run(profile: ProfileCoordinator) -> Flow {
        guard let params = params else {
            fatalError("dependencies needed to run `ProfileCoordinator` are missing.")
        }
        return profile.start(parameters: params)
    }
}

// MARK: - TabList

extension TabBarCoordinator {

    enum TabBarTab {
        case spots
        case search
        case profile
    }
}

// MARK: Coordinator Output

extension TabBarCoordinator: CoordinatorOutput {

    func coordinator<C: Coordinator>(_ coordinator: C, didFinish result: CoordinatorResult) {
        switch (coordinator, result) {
        case (is SpotCoordinator, is SpotCompletionResult):
            unlinkNode(SpotCoordinator.self)
        case (is SearchCoordinator, is SearchCompletionResult):
            unlinkNode(SearchCoordinator.self)
        case (is ProfileCoordinator, is ProfileCompletionResult):
            unlinkNode(ProfileCoordinator.self)
        default: break
        }
    }
}

struct TabarCompletionResult {

    let lastViewedTab: TabBarCoordinator.TabBarTab
}
