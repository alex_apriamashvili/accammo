
import Foundation

/// an enum that describes user state
enum UserState {
    case loggedIn(token: String)
    case unauthorized
}

protocol ApplicationState {

    /// prodives current user state
    var userState: UserState { get }

    /// updates user state with the given token
    /// in case if the token is `nil` – previously stored token gets removed
    /// in case is teh token has value – previously stored token gets updated
    func updateUser(state: UserState)
}

final class ApplicationStateImpl {

    let storage: Storage

    init(storage: Storage) {
        self.storage = storage
    }
}

// MARK: - ApplicationState Implementation

extension ApplicationStateImpl: ApplicationState {

    var userState: UserState {
        guard let token = storage.fetchValue(for: Keys.userToken.rawValue, as: String.self) else {
                return .unauthorized
        }
        return .loggedIn(token: token)
    }

    func updateUser(state: UserState) {
        switch state {
        case let .loggedIn(token: token):
            storage.putValue(token, for: Keys.userToken.rawValue)
        default:
            let nilString: String? = nil
            storage.putValue(nilString, for: Keys.userToken.rawValue)
        }
    }
}

// MARK: - Keys

private extension ApplicationStateImpl {

    enum Keys: String {
        case userToken
    }
}
