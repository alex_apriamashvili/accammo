
import Foundation
import UI
import TabBar

protocol TabFactory {

    typealias Product = TabBarItemPresentable
    typealias Flow = Coordinator.Flow

    /// a generic factory method that produces a tab for a given Flow
    /// - parameter coordinator: an instance of a coordinator that should be assignrd to this tab
    /// - parameter flow: a flow that is coupled with the assigned coordinator
    func tab<C: Coordinator>(coordinator: C, flow: Coordinator.Flow) -> Product
}

// MARK: - Implementation

final class TabFactoryImpl: TabFactory {

    private enum TabNames: String {
        case spots
        case search
        case profile
    }

    func tab<C: Coordinator>(coordinator: C, flow: Coordinator.Flow) -> Product {
        switch coordinator {
        case is SpotCoordinator:
            return spotsTab(controller: flow)
        case is SearchCoordinator:
            return searchTab(controller: flow)
        case is ProfileCoordinator:
            return profileTab(controller: flow)
        default:
            fatalError("\(coordinator) has no tab prepared")
        }
    }
}

// MARK: - Private

private extension TabFactoryImpl {

    func spotsTab(controller: UIViewController) -> Product {
        let style = spotsTabStyle()
        return TabBarItemPresentableImpl(
            title: TabNames.spots.rawValue.capitalized,
            style: style,
            controller: controller
        )
    }

    func searchTab(controller: UIViewController) -> Product {
        let style = searchTabStyle()
        return TabBarItemPresentableImpl(
            title: TabNames.search.rawValue.capitalized,
            style: style,
            controller: controller
        )
    }

    func profileTab(controller: UIViewController) -> Product {
        let style = profileTabStyle()
        return TabBarItemPresentableImpl(
            title: TabNames.profile.rawValue.capitalized,
            style: style,
            controller: controller
        )
    }
}

// MARK: - Styles

private extension TabFactoryImpl {

    func spotsTabStyle() -> TabBarItemStyleProvider {
        return TabBarItemStyleProviderImpl(
            image: #imageLiteral(resourceName: "spots"),
            selectedImage: #imageLiteral(resourceName: "spots_selected")
        )
    }

    func searchTabStyle() -> TabBarItemStyleProvider {
        return TabBarItemStyleProviderImpl(
            image: #imageLiteral(resourceName: "search"),
            selectedImage: #imageLiteral(resourceName: "search_selected")
        )
    }

    func profileTabStyle() -> TabBarItemStyleProvider {
        return TabBarItemStyleProviderImpl(
            image: #imageLiteral(resourceName: "profile"),
            selectedImage: #imageLiteral(resourceName: "profile_selected")
        )
    }
}
