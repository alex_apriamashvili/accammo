
import Foundation

/// an entity that describes an actual persistence store
/// which is used for storing the data
protocol PersistentStore {

    /// put value into a persistant store
    /// - parameter value: a value that needs to be saved
    /// - parameter key: a key that needs to be assigned to the value
    func save<V>(value: V, for key: String)

    /// get a value out of the storage by a specified key
    /// - parameter key: a key for the value
    func value<V>(for key: String) -> V?

    /// remove the value by a specified key
    /// - parameter key: key for the value that needs to be removed
    func removeValue(for key: String)
}

/// an abstraction above the way of how data is stored in the App
protocol Storage {

    /// get a value from a storage for a specified key
    /// - parameter key: a key that leads to the wanted record
    /// - parameter type: a type of a record that needs to be gotten
    func fetchValue<V: Codable>(for key: String, as type: V.Type) -> V?

    /// write a value into a storage for a specified key
    /// - parameter value: a value that needs to be stored
    /// - parameter key: a key that value needs to be stored under
    func putValue<V: Codable>(_ value: V?, for key: String)
}

final class StorageImpl: Storage {

    private let persistentStore: PersistentStore

    init(store: PersistentStore = UserDefaults.standard) {
        persistentStore = store
    }

    func fetchValue<V: Codable>(for key: String, as type: V.Type) -> V? {
        let value: V? = persistentStore.value(for: key)
        return value
    }

    func putValue<V: Codable>(_ value: V?, for key: String) {
        if let value = value {
            persistentStore.save(value: value, for: key)
        } else {
            persistentStore.removeValue(for: key)
        }
    }
}

extension UserDefaults: PersistentStore {

    func save<V>(value: V, for key: String) {
        self.set(value, forKey: key)
    }

    func value<V>(for key: String) -> V? {
        guard let result = self.value(forKey: key) as? V else { return nil }
        return result
    }

    func removeValue(for key: String) {
        self.set(nil, forKey: key)
    }
}
