
import Foundation
import Domain

typealias TabBarCoordinatorDependencies =   HasTabFactory &
                                            HasSearchCriteriaContainer &
                                            HasAppState

protocol HasTabFactory {

    var tabFactory: TabFactory { get }
}

protocol HasAppState {

    var appState: ApplicationState { get }
}

protocol HasSearchCriteriaContainer {

    var searchCriteriaContainer: SearchCriteriaContainer { get }
}

struct TabBarCoordinatorDependenciesImpl: TabBarCoordinatorDependencies {
    let searchCriteriaContainer: SearchCriteriaContainer = SearchCriteriaContainerImpl()
    let tabFactory: TabFactory = TabFactoryImpl()
    let appState: ApplicationState = ApplicationStateImpl(storage: StorageImpl())
}
