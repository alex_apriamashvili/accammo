import Foundation
import UIKit

/// all coordinators have to be of a reference type
/// to make sure that we have the same instance of a coordinator each time
protocol ReferenceTypeConstraint: class {
    init()
}

/// a protocol that describes a coordinator node
/// basically, each coordinator is a node of its parent coordinator,
/// except the root one, due to it has no parents
protocol CoordinatorNode: ReferenceTypeConstraint {}

/// a generic protocol that describes any coordinator
/// also should be treated as an input stream
protocol Coordinator: CoordinatorNode {

    /// aparticular type that describes
    /// a dependency containes for this particular coordinator
    associatedtype Params

    typealias Flow = UIViewController
    
    /// method is used to start the coordinator with given params
    /// - parameter parameters: dependencies that have to be passed to the coordinator
    /// to be distributed among the generated flows
    /// - returns: a flow that has been built
    @discardableResult
    func start(parameters: Params) -> Flow

    /// a coordinator that is a parent of this particular one
    /// the root coordinator shouldn't have parent, that's why this variable is optional
    /// **this value has to be weak**
    var parent: CoordinatorNode? { get set }

    /// a set of child coordinators that are dependant on this one
    /// child coordinators has a weak reference on this one, as on its parent
    var nodes: [CoordinatorNode] { get set }

    /// append a coordinator with a specified type as a node coordinator
    /// and assign it's parent as `self`
    /// - parameter type: a type of a coordinator that should be added
    /// - returns: an instance of a coordinator by a specified type
    func linkNode<N: Coordinator>(_ type: N.Type) -> N

    /// remove a coordinator with a specific type from the list of nodes
    /// if this list contains this coordinator of course
    /// - parameter type: a type of a coordinator that should be removed
    func unlinkNode<N: Coordinator>(_ type: N.Type)
}

///  a default implementation of `link` and `unlink`
extension Coordinator {

    func linkNode<N: Coordinator>(_ type: N.Type) -> N {
        let coordinator = N()
        nodes.append(coordinator)
        coordinator.parent = self

        return coordinator
    }

    func unlinkNode<N: Coordinator>(_ type: N.Type) {
      guard let index = nodes.firstIndex(where: { $0 is N }) else { return }
        nodes.remove(at: index)
    }
}

/// a protocol that subscribes on the coordinator events,
/// in most of the cases – a parent coordinator
protocol CoordinatorOutput {

    /// a function that notifies the coordinator output
    /// that it's child coordinator has finished its job
    /// - parameter result: a result returned from the child coordinator before exit
    func coordinator<C: Coordinator>(_ coordinator: C, didFinish result: CoordinatorResult)
}

/// a protocol that  describesresult
/// returned from the child coordinator before exit
protocol CoordinatorResult {}

/// a default emplementation for an empty result
struct VoidResult: CoordinatorResult {}
