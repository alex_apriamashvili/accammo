
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  private var rootCoordinator: RootCoordinator?

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    rootCoordinator = RootCoordinator(window: window)
    _ = rootCoordinator?.start(parameters: nil)

    return true 
  }
}
