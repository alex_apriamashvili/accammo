#
# Be sure to run `pod lib lint Login.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Spots'
  s.version          = '0.1.0'
  s.summary          = 'A Spot List module'
  s.description      = 'A domain module that represents a list of recommended spots for a user.'
  s.homepage         = 'https://alex_apriamashvili@bitbucket.org/alex_apriamashvili/accommo.git'
  s.author           = { 'Alex Apriamashvili' => 'alexander.apriamashvili@gmail.com' }

  s.ios.deployment_target = '10.0'
  s.swift_version = '5'
  s.source = { :path => '' }
  s.source_files = 'CompileSources/**/*'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => "#{s.swift_version}", 'FRAMEWORK_SEARCH_PATHS' => '${BUILT_PRODUCTS_DIR}/../' }

  s.dependency 'Service'
  s.dependency 'Domain'
  s.dependency 'UI'
  
end
