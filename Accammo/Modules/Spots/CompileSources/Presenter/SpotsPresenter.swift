
import UI
import Domain

// MARK: - Public

final class SpotsPresenter {

    typealias Dependency = HasSpotsModuleStyleProvider

    weak var output: SpotsModuleOutput?
    weak var view: SpotsViewInput?
    var interactor: SpotsInteractorInput?
    var router: SpotsRouterInput?

    private(set) var searchCriteria: SearchCriteria
    private(set) var dependency: Dependency
    private(set) var viewLoaded: Bool = false
    private(set) var pendingViewController: UIViewController?

    init(searchCriteria: SearchCriteria, dependency: Dependency) {
        self.searchCriteria = searchCriteria
        self.dependency = dependency
    }
}

// MARK: - Private

private extension SpotsPresenter {

    func completelyReloadData() {
        view?.showPreloader()
        interactor?.fetchRecommended()
    }
}

// MARK: - SpotsInteractorOutput Implementation

extension SpotsPresenter: SpotsInteractorOutput {

    func presentResults(spots: [Spot]) {
        view?.hidePreloader()

        guard spots.count > 0 else {
            view?.showEmptyState()
            return
        }

        let viewModels = spots.map{ SpotModuleMapper.map(entity: $0, style: dependency.style) }
        view?.update(viewModels: viewModels)
    }

    func present(error: Error) {
        view?.hidePreloader()
        view?.showError(message: error.localizedDescription)
    }

    func present(spot: Spot) {
        output?.handleTapOnSpot(spot)
    }
}

// MARK: - SpotsViewOutput Implementation

extension SpotsPresenter: SpotsViewOutput {

    func viewIsReady() {
        viewLoaded = true
        if let vc = pendingViewController {
            router?.present(vc, animated: false)
        } else  {
            completelyReloadData()
        }
    }

    func reloadData() {
        completelyReloadData()
    }
}

// MARK: - SpotsModuleInput Implementation

extension SpotsPresenter: SpotsModuleInput {

    func dismissAnyPresentedFlows() {
        router?.dismiss()
    }

    func setNeedsMakeNetworkCall() {
        pendingViewController = nil
        guard viewLoaded else { return }
        completelyReloadData()
    }

    func show(login flow: UIViewController) {
        pendingViewController = flow
    }

    func show(detail flow: UIViewController) {
        router?.push(flow)
    }
}

// MARK: - SpotsCollectionViewSourceActionHandler Implementation

extension SpotsPresenter: SpotsCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int) {
        interactor?.fetchSpot(at: index)
    }
}

