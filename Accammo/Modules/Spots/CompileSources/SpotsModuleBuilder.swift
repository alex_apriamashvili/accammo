
import UI
import Domain
import Service

public final class SpotsModuleBuilder {

    public static func build(output: SpotsModuleOutput?,
                      searchCtiteria: SearchCriteria,
                      dependencies: SpotsModuleDependency) -> SpotsModule {
        let presenter = SpotsPresenter(searchCriteria: searchCtiteria, dependency: dependencies)
        let view = SpotsViewController(dependency: dependencies, output: presenter)
        let router = SpotsRouter(baseViewController: view)
        let service = SpotFinderServiceImpl(client: APIClientImpl())
        let interactor = SpotsInteractor(service: service)

        presenter.output = output
        presenter.view = view

        presenter.router = router

        interactor.output = presenter
        presenter.interactor = interactor

        return (controller: view, input: presenter)
    }
}
