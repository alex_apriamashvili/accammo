
import Foundation
import Domain
import Service

final class SpotsInteractor {

    weak var output: SpotsInteractorOutput?

    private let service: SpotFinderService?
    private var entities: [Spot] = []

    init(service: SpotFinderService) {
        self.service = service
    }
}

// MARK: - SpotsInteractorInput Implementation

extension SpotsInteractor: SpotsInteractorInput {

    func fetchRecommended() {
        service?.recommendedSpots(success: { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.entities = result.spotList.map{ SpotModuleMapper.map(model: $0) }
            strongSelf.output?.presentResults(spots: strongSelf.entities)
        }, failure: { [weak self] error in
            guard let strongSelf = self else { return }
            strongSelf.output?.present(error: error)
        })
    }

    func fetchSpot(at index: Int) {
        guard index < entities.count else {
            assertionFailure("index is out of bounds")
            return
        }
        let spot = entities[index]
        output?.present(spot: spot)
    }
}
