
import Foundation
import UI
import Domain

/// a typealias that represents the Spots Module
public typealias SpotsModule = (controller: SpotsViewController, input: SpotsModuleInput)

// MARK: - Dependencies

/// a typealias that combiles all the dependencies needed for this module
public typealias SpotsModuleDependency = HasSpotsModuleStyleProvider & HasSpotsModuleResourcesPovider

/// a protocol that describes an entity
/// that brings styling attributes to a module
public protocol HasSpotsModuleStyleProvider {

    /// a set of styling attruibutes
    var style: SpotsModuleStyleProvider { get }
}
/// a protocol that describes an entity
/// that brings resources into a module
public protocol HasSpotsModuleResourcesPovider {

    /// a set of resources required by the module
    var resources: SpotsModuleResourcesPovider { get }
}

/// a protocol that describes styles that are required by the module
public protocol SpotsModuleStyleProvider {

    /// a main background color
    var backgroundColor: UIColor { get }

    /// a main tint color
    var tintColor: UIColor { get }

    // Cell

    /// spot title text color
    var spotTitleColor: UIColor { get }

    /// spot title font
    var spotTitleFont: UIFont { get }

    /// spot description text color
    var spotDescriptionTextColor: UIColor { get }

    /// spot description font
    var spotDescriptionFont: UIFont { get }

    /// wage title text color
    var wageTitleColor: UIColor { get }

    /// wage title font
    var wageTitleFont: UIFont { get }

    /// wage base text color
    var wageBaseColor: UIColor { get }

    /// wage base font
    var wageBaseFont: UIFont { get }
}

/// a protocol that describes resources that are required by the module
public protocol SpotsModuleResourcesPovider {

    /// screen title
    var title: String { get }
}


// MARK: - Module I/O Protocols

/// an interface that holds methods to communicate with the module
public protocol SpotsModuleInput {

    /// tells the receiver that a Network Call could be performed
    /// in case if this method wasn't triggered – no network call would be performed
    func setNeedsMakeNetworkCall()

    /// tells receiver to present a login flow to let user get logged in first
    /// this action should be performed immidiately – no animation needed
    func show(login flow: UIViewController)

    /// tells a receiver to represent a given detail flow
    func show(detail flow: UIViewController)

    /// if any flow is presented at the moment above the current one,
    /// it will be dismissed by calling this function
    func dismissAnyPresentedFlows()
}

/// a protocol that contains methods to handle events
/// that may occur in the module
public protocol SpotsModuleOutput: class {

    /// notify the receiver, that user has selected an entry
    /// - parameter spot: the entry user has selected
    func handleTapOnSpot(_ spot: Spot)
}

// MARK: Interactor

/// a protocol that describes a communication interface
/// for the module interactor
protocol SpotsInteractorInput {

    /// tells the receiver to initiate a recommended spots search
    func fetchRecommended()

    /// obtain a persisted spot for a given index
    func fetchSpot(at index: Int)
}

/// a way for the Interactor to notify about changes that've taken place
protocol SpotsInteractorOutput: class {

    /// tells the receiver
    /// that the received results have to be represented
    /// - parameter spots: an array of domain entities taht describe search result
    func presentResults(spots: [Spot])

    /// tells the receiver that an error has occured
    /// and that the user needs to be notified
    func present(error: Error)

    /// tells the receiver that a particular search result has to be presented
    /// used to navigate to details
    func present(spot: Spot)
}

// MARK: - View

/// a ptorocol that defines actions that could been done to a view
protocol SpotsViewInput: class {

    /// tells the receiver to show a spinner
    func showPreloader()

    /// tells the receiver to hide a spinner
    func hidePreloader()

    /// tells the receiver that an emplty state has to be represented
    func showEmptyState()

    /// tells the receiver to represent a given error message
    /// - parameter message: a localized description of the error occured
    func showError(message: String)

    /// tells the receiver that the view models must be updated
    func update(viewModels: [SpotViewModel])
}

/// a protocol that holds the events that might happen for the view
/// basically it's the only way of how view might tell about it's changes
protocol SpotsViewOutput {

    /// tells the receiver that all views were being loaded
    /// and it's ready for the interaction
    func viewIsReady()

    /// tells the receiver that user wan't to see updated data
    func reloadData()
}

// MARK: - Router

/// a protocol that describes routing actions
protocol SpotsRouterInput {

    /// tell the receiver to hide
    /// all the additional flows presented on this module
    func dismiss()

    /// this method should be called if next view-countroller
    /// **shouldn't** be put into a navigation stack
    func present(_ viewController: UIViewController, animated: Bool)

    /// this method should be called if next view-countroller
    /// **needs** to be put into a navigation stack
    func push(_ viewController: UIViewController)
}

