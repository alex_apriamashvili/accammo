
import Foundation
import UI

final class SpotsRouter {

    weak var controller: UIViewController?

    init(baseViewController: UIViewController) {
        controller = baseViewController
    }
}

extension SpotsRouter: SpotsRouterInput {

    func dismiss() {
        controller?.dismiss(animated: true, completion: nil)
    }

    func present(_ viewController: UIViewController, animated: Bool) {
        controller?.present(viewController, animated: animated, completion: nil)
    }

    func push(_ viewController: UIViewController) {
        controller?.navigationController?.pushViewController(viewController, animated: true)
    }
}
