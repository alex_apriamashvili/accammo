
import UI
import SDWebImage

final class SpotPreviewCell: UICollectionViewCell {

    let image: UIImageView = UIImageView()
    let nameTitle: UILabel = UILabel()
    let descriptionText: UILabel = UILabel()
    let wageView: WageView = WageView()

    var imageHeightConstraint: NSLayoutConstraint?

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func configure(viewModel: SpotViewModel) {
        image.sd_setImage(with: viewModel.imageURL, completed: nil)
        nameTitle.text = viewModel.title
        descriptionText.text = viewModel.description
        imageHeightConstraint?.constant = viewModel.imageSize.height
        wageView.rateLabel.text = viewModel.wage
        wageView.basisLabel.text = viewModel.basis

        apply(style: viewModel.style)

        setNeedsLayout()
        layoutIfNeeded()
    }
}

private extension SpotPreviewCell {

    func configureUI() {
        contentView.backgroundColor = .white
        contentView.addSubview(image)
        contentView.addSubview(nameTitle)
        contentView.addSubview(descriptionText)
        contentView.addSubview(wageView)

        constrainViews()
    }

    func apply(style: SpotsModuleStyleProvider) {
        nameTitle.font = style.spotTitleFont
        nameTitle.textColor = style.spotTitleColor

        descriptionText.font = style.spotDescriptionFont
        descriptionText.textColor = style.spotDescriptionTextColor
        descriptionText.numberOfLines = 0
        descriptionText.lineBreakMode = .byWordWrapping

        wageView.rateLabel.font = style.wageTitleFont
        wageView.rateLabel.textColor = style.wageTitleColor

        wageView.basisLabel.font = style.wageBaseFont
        wageView.basisLabel.textColor = style.wageBaseColor
    }
}

private extension SpotPreviewCell {

    struct Constants {
        static let imageHeight: CGFloat = 173.0
        static let edgeOffset: CGFloat = 8.0
        static let wageViewWidth: CGFloat = 85.0
    }

    func constrainViews() {
        constrainImage()
        constrainNameLabel()
        constrainDescriptionLabel()
        constrainWageView()
    }

    func constrainImage() {
        image.translatesAutoresizingMaskIntoConstraints = false
        image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        image.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        image.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageHeightConstraint = image.heightAnchor.constraint(equalToConstant: 0)
        imageHeightConstraint?.isActive = true
    }

    func constrainNameLabel() {
        nameTitle.translatesAutoresizingMaskIntoConstraints = false
        nameTitle.topAnchor.constraint(equalTo: image.bottomAnchor, constant: Constants.edgeOffset).isActive = true
        nameTitle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.edgeOffset).isActive = true
        nameTitle.trailingAnchor.constraint(equalTo: wageView.leadingAnchor, constant: -Constants.edgeOffset).isActive = true
        nameTitle.bottomAnchor.constraint(equalTo: descriptionText.topAnchor, constant: -Constants.edgeOffset).isActive = true
    }

    func constrainDescriptionLabel() {
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.edgeOffset).isActive = true
        descriptionText.trailingAnchor.constraint(equalTo: wageView.leadingAnchor, constant: -Constants.edgeOffset).isActive = true
        descriptionText.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.edgeOffset).isActive = true
    }

    func constrainWageView() {
        wageView.translatesAutoresizingMaskIntoConstraints = false
        wageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.edgeOffset).isActive = true
        wageView.topAnchor.constraint(equalTo: nameTitle.topAnchor).isActive = true
        wageView.bottomAnchor.constraint(equalTo: descriptionText.bottomAnchor).isActive = true
        wageView.widthAnchor.constraint(equalToConstant: Constants.wageViewWidth).isActive = true
    }
}
