
import Foundation
import UI

public final class SpotsViewController: UIViewController {

    typealias Dependency = HasSpotsModuleStyleProvider & HasSpotsModuleResourcesPovider
    typealias Output = SpotsViewOutput & SpotsCollectionViewSourceActionHandler


    let output: Output

    private let dependency: Dependency
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private let source: SpotsCollectionViewSource
    private let refreshControl = UIRefreshControl()
    private let navigationBarImageView = UIImageView(image: #imageLiteral(resourceName: "travis_avatar"))

    // MARK: - Initialization

    init(dependency: Dependency, output: Output) {
        self.dependency = dependency
        self.output = output
        source = SpotsCollectionViewSource(actionHandler: self.output)
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    public override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarImageView.isHidden = false
      if #available(iOS 11.0, *) {
        navigationController?.navigationBar.prefersLargeTitles = true
      }
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output.viewIsReady()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        navigationBarImageView.isHidden = true
    }

    // MARK: - Actions

    @objc
    func pullToRefresh() {
        output.reloadData()
    }
}

// MARK: - SpotsViewInput Implementation

extension SpotsViewController: SpotsViewInput {

    func update(viewModels: [SpotViewModel]) {
        source.viewModels = viewModels
        collectionView.reloadData()
    }

    func showPreloader() {}

    func hidePreloader() {
        refreshControl.endRefreshing()
    }

    func showEmptyState() {}

    func showError(message: String) {}
}

// MARK: - Private

private extension SpotsViewController {

    func configureUI() {
        title = dependency.resources.title
        view.backgroundColor = dependency.style.backgroundColor
        configureCollectionView()
        configureNavigationBar()
        configureRefreshControl()
    }

    func configureNavigationBar() {
        navigationController?.navigationBar.addSubview(navigationBarImageView)
        navigationController?.navigationBar.tintColor = dependency.style.tintColor
        navigationBarImageView.clipsToBounds = true
        constrainNavigationBarImage()
    }

    func configureRefreshControl() {
        collectionView.alwaysBounceVertical = true
        refreshControl.tintColor = dependency.style.tintColor
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
}

// MARK: - CollectionView

private extension SpotsViewController {

    func configureCollectionView() {
        collectionView.backgroundColor = dependency.style.backgroundColor

        view.addSubview(collectionView)

        registerPreviewCell()

        collectionView.dataSource = source
        collectionView.delegate = source

        constrainCollectionView()
    }

    func registerPreviewCell() {
        collectionView.register(
            SpotPreviewCell.self,
            forCellWithReuseIdentifier: SpotsCollectionViewSource.spotCellIdentifier
        )
    }

    func constrainCollectionView() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

// MARK: - Navigation Bar Image

private extension SpotsViewController {

    struct Layout {
        static let imageSize: CGFloat = 44
        static let imageRightMargin: CGFloat = 16
        static let imageBottomMargin: CGFloat = 8
    }

    func constrainNavigationBarImage() {
        guard let navigationBar = navigationController?.navigationBar else { return }
        navigationBarImageView.layer.cornerRadius = Layout.imageSize / 2
        navigationBarImageView.translatesAutoresizingMaskIntoConstraints = false
        navigationBarImageView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -Layout.imageRightMargin).isActive = true
        navigationBarImageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Layout.imageBottomMargin).isActive = true
        navigationBarImageView.heightAnchor.constraint(equalToConstant: Layout.imageSize).isActive = true
        navigationBarImageView.widthAnchor.constraint(equalTo: navigationBarImageView.heightAnchor).isActive = true
    }
}
