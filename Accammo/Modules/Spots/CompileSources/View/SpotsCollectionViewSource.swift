
import UI

protocol SpotsCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int)
}

final class SpotsCollectionViewSource: NSObject {

    static let spotCellIdentifier = "com.accammo.spots.cell.identifier"

    var viewModels: [SpotViewModel] = []

    private let prototypeCell = SpotPreviewCell(frame: .zero)
    private let actionHandler: SpotsCollectionViewSourceActionHandler

    init(actionHandler: SpotsCollectionViewSourceActionHandler) {
        self.actionHandler = actionHandler
        super.init()
    }
}

private extension SpotsCollectionViewSource {

    func cellSize(_ viewModel: SpotViewModel, width: CGFloat) -> CGSize {
        let widthContraint = prototypeCell.contentView.widthAnchor.constraint(equalToConstant: width)
        widthContraint.isActive = true

        prototypeCell.contentView.translatesAutoresizingMaskIntoConstraints = false
        prototypeCell.configure(viewModel: viewModel)

        prototypeCell.contentView.setNeedsUpdateConstraints()
        prototypeCell.contentView.updateConstraintsIfNeeded()
        prototypeCell.contentView.setNeedsLayout()
        prototypeCell.contentView.layoutIfNeeded()

      let size = prototypeCell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        widthContraint.isActive = false
        prototypeCell.removeConstraint(widthContraint)

        return size
    }
}

extension SpotsCollectionViewSource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: SpotsCollectionViewSource.spotCellIdentifier,
            for: indexPath
        )
        guard let spotCell = cell as? SpotPreviewCell,
            indexPath.item < viewModels.count else { fatalError("wrong cell was dequeued") }
        let viewModel = viewModels[indexPath.item]
        spotCell.configure(viewModel: viewModel)

        return spotCell
    }
}

extension SpotsCollectionViewSource: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionHandler.handleCellTap(at: indexPath.item)
    }
}

extension SpotsCollectionViewSource: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewModel = viewModels[indexPath.item]
        let width = collectionView.bounds.width

        return cellSize(viewModel, width: width)
    }
}
