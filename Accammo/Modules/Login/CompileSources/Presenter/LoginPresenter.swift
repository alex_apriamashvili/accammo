
import Foundation
import UI

// MARK: - Public

final class LoginPresenter {

    typealias Dependency = HasLoginModuleStyleProvider

    weak var output: LoginModuleOutput?
    weak var view: LoginViewInput?
    var interactor: LoginInteractorInput?
    var router: LoginRouterInput?

    private(set) var dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
}

// MARK: - Private

private extension LoginPresenter {}

// MARK: - LoginInteractorOutput Implementation

extension LoginPresenter: LoginInteractorOutput {

    func authorize(token: String) {
        view?.hidePreloader()
        output?.handleLogin(token: token)
    }

    func present(error: Error) {
        view?.hidePreloader()
        let alert = AlertControl.errorAlert(message: error.localizedDescription)
        router?.present(alert)
    }
}

// MARK: - LoginViewOutput Implementation

extension LoginPresenter: LoginViewOutput {

    func handleTapOnLoginButton(login: String, handle: String) {
        view?.showPreloader()
        let credentials = Credentials(login: login, password: handle)
        interactor?.doLogin(credentials: credentials)
    }
}

// MARK: - LoginModuleInput Implementation

extension LoginPresenter: LoginModuleInput {}
