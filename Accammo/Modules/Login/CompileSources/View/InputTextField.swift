
@IBDesignable final class InputTextField: UITextField {

    @IBInspectable var insetX: CGFloat = 8
    @IBInspectable var insetY: CGFloat = 0

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
