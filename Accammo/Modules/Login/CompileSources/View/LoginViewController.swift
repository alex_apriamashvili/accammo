
import Foundation
import UI

public final class LoginViewController: UIViewController {

    typealias Dependency = HasLoginModuleStyleProvider & HasLoginModuleResourcesPovider
    typealias Output = LoginViewOutput

    let output: Output

    private let dependency: Dependency

    private let backgroundImageView: UIImageView = UIImageView()
    private let logoImageView: UIImageView = UIImageView()
    private let dataInputView: AuthorizationInputView = AuthorizationInputView()
    private var preloader: LoginOverlay = LoginOverlay()

    // MARK: - Initialization

    init(dependency: Dependency, output: Output) {
        self.dependency = dependency
        self.output = output
        super.init(nibName: nil, bundle: nil)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
}

// MARK: - SpotsViewInput Implementation

extension LoginViewController: LoginViewInput {

    func showPreloader() {
        guard preloader.isHidden else { return }
        preloader.isHidden = false
        preloader.spinner.startAnimating()
    }

    func hidePreloader() {
        guard !preloader.isHidden else { return }
        preloader.spinner.stopAnimating()
        preloader.isHidden = true
    }
}

// MARK: - AuthorizationInputViewOutput

extension LoginViewController: AuthorizationInputViewOutput {

    func dataIsReady(login: String, handle: String) {
        output.handleTapOnLoginButton(login: login, handle: handle)
    }
}

// MARK: - Private

private extension LoginViewController {

    struct Constants {

        static let blurAlpha: CGFloat = 0.9
    }

    func configureUI() {
        view.backgroundColor = dependency.style.backgroundColor
        configureBackgroundImage()
        applyBlurEffect()
        configureLogo()
        configureInputView()
        configureOverlay()
    }

    func configureBackgroundImage() {
        backgroundImageView.image = dependency.resources.backgroundImage
        view.addSubview(backgroundImageView)
        constrainBackGroundImage()
    }

    func applyBlurEffect() {
        let blur = UIBlurEffect(style: .regular)
        let blurView = UIVisualEffectView(effect: blur)
        blurView.alpha = Constants.blurAlpha
        blurView.frame = view.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurView)
    }

    func configureLogo() {
        view.addSubview(logoImageView)
        logoImageView.image = dependency.resources.logoImage
        constrainLogo()
    }

    func configureInputView() {
        dataInputView.output = self
        view.addSubview(dataInputView)
        constrainDataInput()
        dataInputView.apply(style: dependency.style)
        dataInputView.apply(resources: dependency.resources)
    }

    func configureOverlay() {
        preloader.backgroundColor = dependency.style.overlayColor
        view.addSubview(preloader)
        constrainOverlay()
        preloader.isHidden = true
    }
}

// MARK: - Layout

private extension LoginViewController {

    struct Layout {

        static let edgeOffset: CGFloat = 60.0
        static let inputHeight: CGFloat = 150.0
    }

    func constrainBackGroundImage() {
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false

        let ratio = UIScreen.main.bounds.size.height / (backgroundImageView.image?.size.height ?? 1)
        let imageWidth = ratio * (backgroundImageView.image?.size.width ?? 0)

        backgroundImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        backgroundImageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.height).isActive = true
        backgroundImageView.widthAnchor.constraint(equalToConstant: imageWidth).isActive = true
    }

    func constrainLogo() {
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        logoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.edgeOffset).isActive = true
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }

    func constrainDataInput() {
        dataInputView.translatesAutoresizingMaskIntoConstraints = false

        dataInputView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.edgeOffset).isActive = true
        dataInputView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.edgeOffset).isActive = true
        dataInputView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        dataInputView.heightAnchor.constraint(equalToConstant: Layout.inputHeight).isActive = true
    }

    func constrainOverlay() {
        preloader.translatesAutoresizingMaskIntoConstraints = false

        preloader.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        preloader.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        preloader.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        preloader.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
