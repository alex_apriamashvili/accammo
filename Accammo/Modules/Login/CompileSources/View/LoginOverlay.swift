
import UI

class LoginOverlay: UIView {

  let spinner: UIActivityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }
}

// MARK: - Configuration

private extension LoginOverlay {

    func configureUI() {
        addSubview(spinner)
        constrainSpinner()
    }
}

// MARK: - Layout

private extension LoginOverlay {

    func constrainSpinner() {
        spinner.translatesAutoresizingMaskIntoConstraints = false

        spinner.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
