

protocol AuthorizationInputViewOutput: class {

    func dataIsReady(login: String, handle: String)
}

final class AuthorizationInputView: UIView {

    private let loginInput: InputTextField = InputTextField()
    private let passwordInput: InputTextField = InputTextField()

    weak var output: AuthorizationInputViewOutput?

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func apply(style: LoginModuleStyleProvider) {
        loginInput.backgroundColor = style.backgroundColor
        loginInput.font = style.inputFont
        loginInput.textColor = style.inputTextColor
        loginInput.tintColor = style.tintColor

        passwordInput.backgroundColor = style.backgroundColor
        passwordInput.font = style.inputFont
        passwordInput.textColor = style.inputTextColor
        passwordInput.tintColor = style.tintColor
    }

    func apply(resources: LoginModuleResourcesPovider) {
        loginInput.placeholder = resources.loginPlaceholderText
        passwordInput.placeholder = resources.handlePlaceholderText
    }
}

// MARK: - UITextFieldDelegate Implementation

extension AuthorizationInputView: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case loginInput:
            return resolveFirstResponder(first: loginInput, second: passwordInput)
        case textField:
            return resolveFirstResponder(first: passwordInput, second: loginInput)
        default: return true
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let data = fulfilledData(first: loginInput, second: passwordInput) else { return }
        output?.dataIsReady(login: data.first, handle: data.second)
    }
}

// MARK: - Private

private extension AuthorizationInputView {

    func resolveFirstResponder(first: UITextField, second: UITextField) -> Bool {
        guard let firstText = first.text,
            !firstText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { return false }
        if let secondText = second.text, !secondText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            first.resignFirstResponder()
            return true
        } else {
            first.resignFirstResponder()
            second.becomeFirstResponder()
            return false
        }
    }

    func fulfilledData(first: UITextField, second: UITextField) -> (first: String, second: String)? {
        guard let firstText = first.text,
            !firstText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty,
            let secondText = second.text,
            !secondText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { return nil }

        return (
            first: firstText.trimmingCharacters(in: .whitespacesAndNewlines),
            second: secondText.trimmingCharacters(in: .whitespacesAndNewlines)
        )
    }
}

// MARK: - Configuration

private extension AuthorizationInputView {

    struct Constants {

        static let radius: CGFloat = 9
    }

    func configureUI() {
        configureLoginInput()
        configurePasswordInput()
        constrainViews()
    }

    func configureLoginInput() {
        addSubview(loginInput)

        loginInput.borderStyle = .none
        loginInput.layer.cornerRadius = Constants.radius
        loginInput.delegate = self
    }

    func configurePasswordInput() {
        addSubview(passwordInput)

        passwordInput.borderStyle = .none
        passwordInput.layer.cornerRadius = Constants.radius
        passwordInput.delegate = self
        passwordInput.isSecureTextEntry = true
    }
}

// MARK: - Layout

private extension AuthorizationInputView {

    struct Layout {

        static let elementHeight: CGFloat = 44.0
    }

    func constrainViews() {
        constrainLogin()
        constrainPass()
    }

    func constrainLogin() {
        loginInput.translatesAutoresizingMaskIntoConstraints = false

        loginInput.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor).isActive = true
        loginInput.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor).isActive = true
        loginInput.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor).isActive = true
        loginInput.heightAnchor.constraint(equalToConstant: Layout.elementHeight).isActive = true
    }

    func constrainPass() {
        passwordInput.translatesAutoresizingMaskIntoConstraints = false

        passwordInput.leadingAnchor.constraint(equalTo: loginInput.leadingAnchor).isActive = true
        passwordInput.trailingAnchor.constraint(equalTo: loginInput.trailingAnchor).isActive = true
        passwordInput.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor).isActive = true
        passwordInput.heightAnchor.constraint(equalToConstant: Layout.elementHeight).isActive = true
    }
}
