
import UI
import Service

public final class LoginModuleBuilder {

    public static func build(output: LoginModuleOutput?,
                      dependencies: LoginModuleDependency) -> LoginModule {
        let presenter = LoginPresenter(dependency: dependencies)
        let view = LoginViewController(dependency: dependencies, output: presenter)
        let router = LoginRouter(baseViewController: view)
        let service = LoginServiceImpl(client: APIClientImpl())
        let interactor = LoginInteractor(service: service)

        presenter.output = output
        presenter.view = view

        presenter.router = router

        interactor.output = presenter
        presenter.interactor = interactor

        return view
    }
}
