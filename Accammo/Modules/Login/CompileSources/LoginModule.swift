
public typealias LoginModule = LoginViewController

// MARK: - Dependencies

/// a typealias that describes all the dependencies which is reuired for a module
public typealias LoginModuleDependency = HasLoginModuleStyleProvider & HasLoginModuleResourcesPovider

/// a protocol that describes an entity
/// that brings styling attributes to a module
public protocol HasLoginModuleStyleProvider {

    /// a set of styling attruibutes
    var style: LoginModuleStyleProvider { get }
}

/// a protocol that describes an entity
/// that brings resources into a module
public protocol HasLoginModuleResourcesPovider {

    /// a set of resources required by the module
    var resources: LoginModuleResourcesPovider { get }
}

/// a protocol that describes styles that are required by the module
public protocol LoginModuleStyleProvider {

    /// a main background color
    var backgroundColor: UIColor { get }

    /// a main tint color
    var tintColor: UIColor { get }

    /// a font of a text in an input field
    var inputFont: UIFont { get }

    /// a color of a text in an input field
    var inputTextColor: UIColor { get }

    /// a collor of an overlay view
    /// that is being shown while the data requiest is in progress
    var overlayColor: UIColor { get }
}

/// a protocol that describes resources that are required by the module
public protocol LoginModuleResourcesPovider {

    /// a low-res background image
    /// that should Be shown under a bluring layer
    var backgroundImage: UIImage { get }

    /// an image that should be displayed as a logo one
    var logoImage: UIImage { get }

    /// a localized placeholder text that should be shown
    /// for a login field
    var loginPlaceholderText: String { get }

    /// a localized placeholder text that should be shown
    /// for a password field
    var handlePlaceholderText: String { get }
}

// MARK: - Module I/O Protocols

/// an interface that holds methods to communicate with the module
public protocol LoginModuleInput {}

/// a protocol that contains methods to handle events
/// that may occur in the module
public protocol LoginModuleOutput: class {

    /// notify a receiver that a logout action has been performed
    func handleLogin(token: String)
}

// MARK: Interactor

/// a protocol that describes a communication interface
/// for the module interactor
protocol LoginInteractorInput {

    /// notify interactor that user has filled credentials and needs to be logged in
    func doLogin(credentials: Credentials)
}

/// a way for the Interactor to notify about changes that've taken place
protocol LoginInteractorOutput: class {

    /// in case if user was successfully logged in
    /// this method brings a token which is assigned to the user
    func authorize(token: String)

    /// in case if an error occured during the user authorization,
    /// this method will be called to provide information about an error
    func present(error: Error)
}

// MARK: - View

/// a ptorocol that defines actions that could been done to a view
protocol LoginViewInput: class {

    /// tells receiver to show a preloader
    func showPreloader()

    /// tells receiver to hide a preloader
    func hidePreloader()
}

/// a protocol that holds the events that might happen for the view
/// basically it's the only way of how view might tell about it's changes
protocol LoginViewOutput {

    /// tells the receiver that the login button was tapped
    /// this method also provides the receiver with login credentials
    /// - parameter login: user log in
    /// - parameter handle: user handle
    func handleTapOnLoginButton(login: String, handle: String)
}

// MARK: - Router

/// a protocol that describes routing actions
protocol LoginRouterInput {

    /// this method should be called if next view-countroller
    /// **shouldn't** be put into a navigation stack
    func present(_ viewController: UIViewController)

    /// this method should be called if next view-countroller
    /// **needs** to be put into a navigation stack
    func push(_ viewController: UIViewController)
}

