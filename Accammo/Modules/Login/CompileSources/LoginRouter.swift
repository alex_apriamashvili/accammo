
import Foundation
import UI

final class LoginRouter {

    weak var controller: UIViewController?

    init(baseViewController: UIViewController) {
        controller = baseViewController
    }
}

extension LoginRouter: LoginRouterInput {

    func present(_ viewController: UIViewController) {
        controller?.present(viewController, animated: true, completion: nil)
    }

    func push(_ viewController: UIViewController) {
        controller?.navigationController?.pushViewController(viewController, animated: true)
    }
}
