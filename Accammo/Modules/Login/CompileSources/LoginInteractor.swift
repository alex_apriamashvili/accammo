
import Foundation
import Service

final class LoginInteractor {

    weak var output: LoginInteractorOutput?

    private let service: LoginService?

    init(service: LoginService) {
        self.service = service
    }
}

// MARK: - LoginInteractorInput Implementation

extension LoginInteractor: LoginInteractorInput {
    
    func doLogin(credentials: Credentials) {
        let authParams = LoginModuleMapper.map(credentials: credentials)
        service?.login(credentials: authParams, success: { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.output?.authorize(token: result.token)
        }, failure: { [weak self] error in
            guard let strongSelf = self else { return }
            strongSelf.output?.present(error: error)
        })
    }
}
