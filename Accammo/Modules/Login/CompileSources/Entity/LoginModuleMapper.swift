
import Foundation
import Service

final class LoginModuleMapper {

    static func map(credentials: Credentials) -> AuthorizationParams {
        return AuthorizationParams(
            login: credentials.login,
            password: credentials.password
        )
    }
}
