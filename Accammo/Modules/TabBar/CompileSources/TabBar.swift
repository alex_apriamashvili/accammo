
import UI

final class BottomTabBar: UITabBar {

    private let tabBarHeight: CGFloat = 55

    private let lineView = LineView()
    private var lineLeftConstaint: NSLayoutConstraint?
    private var lineWidthConstraint: NSLayoutConstraint?

    private(set) var selectedIndex: Int = 0
    private(set) var shouldApplyRTL: Bool = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func applyStyle(style: TabBarStyleProvider) {
        tintColor = style.activeColor
        lineView.color = style.lineColor
        lineView.isHidden = !style.shouldShowLine
    }

    func select(at index: Int) {
        if selectedIndex != index {
            selectedIndex = index
            layoutIfNeeded()
            UIView.animate(withDuration: LineView.animationDuration) { self.updateLineLayout() }
        }
    }

    private func setup() {
        backgroundColor = .white
        itemPositioning = .automatic
        setupBottomLine()
    }

    // MARK: - Custom height
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var size = super.sizeThatFits(size)
        var bottomInset: CGFloat = 0
        if #available(iOS 11.0, *), RespondChecker.supportsSafeAreaInsets() {
            bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        }
        size.height = tabBarHeight + bottomInset
        return size
    }

    override var traitCollection: UITraitCollection {
        /* iOS 11 introduced new text layout position for tab bar item.
         We should return `compact` to apply old behavior.
        */
        return UITraitCollection(horizontalSizeClass: .compact)
    }
}

// MARK: - Line View

extension BottomTabBar {

    override func layoutSubviews() {
        super.layoutSubviews()
        updateLineLayout()
    }

    override func setItems(_ items: [UITabBarItem]?, animated: Bool) {
        super.setItems(items, animated: animated)
        updateLineLayout()
    }

    private func setupBottomLine() {
        addSubview(lineView)
        lineLeftConstaint = lineView.leftAnchor.constraint(equalTo: leftAnchor)
        lineLeftConstaint?.isActive = true
        lineWidthConstraint = lineView.widthAnchor.constraint(equalToConstant: calculateItemWidth())
        lineWidthConstraint?.isActive = true

        if #available(iOS 11.0, *), RespondChecker.supportsSafeAreaLayoutGuide() {
            lineView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
    }

    private func updateLineLayout() {
        let itemWidth = calculateItemWidth()
        let itemCount = items?.count ?? 0
        let index = shouldApplyRTL ? itemCount - 1 - selectedIndex : selectedIndex
        lineLeftConstaint?.constant = CGFloat(index) * itemWidth
        lineWidthConstraint?.constant = itemWidth
    }

    private func calculateItemWidth() -> CGFloat {
        guard let count = items?.count, count > 0 else {
            return 0
        }
        return bounds.width / CGFloat(count)
    }
}
