
import UI

final class LineView: UIView {

    static let animationDuration: TimeInterval = 0.25
    private let lineHeight: CGFloat = 2

    var color: UIColor = .red {
        didSet {
            self.backgroundColor = color
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        color = .red
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: lineHeight).isActive = true
    }
}
