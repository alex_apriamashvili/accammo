
import UI

// MARK: - Public

/// a builder that assemblies a TabBarModule
public final class TabBarModuleBuilder {

    typealias Attributes = [NSAttributedString.Key: Any]

    public static func build(with items: [TabBarItemPresentable],
                             dependencies: TabBarModuleDependencies) -> TabBarModule? {
        let storyboardController = dependencies.resource.tabBarControllerStoryboard.instantiateInitialViewController()
        guard let controller = storyboardController as? TabBarViewController else { return nil }
        controller.applyDependencies(dependencies)
        controller.viewControllers = prepareViewControllers(with: items, style: dependencies.style)

        return controller
    }
}

// MARK: - Private

private extension TabBarModuleBuilder {

    static func prepareViewControllers(with items: [TabBarItemPresentable],
                                       style: TabBarStyleProvider) -> [UIViewController] {
        var viewControllers: [UIViewController] = []
        let normalAttributes: Attributes = [.foregroundColor: style.inactiveColor, .font: style.font]
        let selectedAttributes: Attributes = [.foregroundColor: style.activeColor, .font: style.font]

        for item in items {
            let barItem = tabBarItem(
                item: item,
                style: style,
                normalStateAttributes: normalAttributes,
                selectedStateAttributes: selectedAttributes
            )

            let viewController = item.controller
            viewController.tabBarItem = barItem
            viewControllers.append(viewController)
        }

        return viewControllers
    }

    static func tabBarItem(item: TabBarItemPresentable,
                   style: TabBarStyleProvider,
                   normalStateAttributes: Attributes,
                   selectedStateAttributes: Attributes) -> UITabBarItem {

        let barItem = UITabBarItem(
            title: item.title,
            image: item.style.image,
            selectedImage: item.style.selectedImage
        )
        barItem.setTitleTextAttributes(normalStateAttributes, for: .normal)
        barItem.setTitleTextAttributes(selectedStateAttributes, for: .selected)
        barItem.titlePositionAdjustment = style.textOffset

        return barItem
    }
}
