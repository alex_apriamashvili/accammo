
import UI

/// a typealias to simplify the definition of the module
public typealias TabBarModule = UITabBarController & TabBarInput

/// a typealiase to unify all the dependencies needed to create the module
public typealias TabBarModuleDependencies = HasTabBarStyleProvider & HasTabBarResourceProvider

// MARK: - Dependency Containers

/// a protocol which holds a module style dependency
public protocol HasTabBarStyleProvider {

    /// a module style dependency
    var style: TabBarStyleProvider { get }
}

/// a protocol which holds a module resources dependency
public protocol HasTabBarResourceProvider {

    /// a module resources dependency
    var resource: TabBarResourceProvider { get }
}

// MARK: - Dependency Descriptions

/// a description of a style dependency.
/// the dependency which is responsible for styling of the module
public protocol TabBarStyleProvider {

    /// a font that has to be applied on the tab titles
    /// this attribute is common and applied to all of the tabs
    /// if you need to specify font for a particular tab,
    /// use the same attribute of an instance that conforms `TabBarItemStyleProvider` protocol
    var font: UIFont { get }

    /// an offset from the tab edges for a title
    var textOffset: UIOffset { get }

    /// a color of an active state of the tab
    /// this attribute is common and applied to all of the tabs
    /// if you need to specify font for a particular tab,
    /// use the same attribute of an instance that conforms `TabBarItemStyleProvider` protocol
    var activeColor: UIColor { get }

    /// a color of an inactive state of the tab
    /// this attribute is common and applied to all of the tabs
    /// if you need to specify font for a particular tab,
    /// use the same attribute of an instance that conforms `TabBarItemStyleProvider` protocol
    var inactiveColor: UIColor { get }

    /// a flag that determines whether the bottom line should be shown or not
    var shouldShowLine: Bool { get }

    /// a color of the bottom line if it's shown
    var lineColor: UIColor { get }
}

/// a description of a resources provider
/// basically this provider is neede to keep all the resources out of the module,
/// to be able to make it a static lib
public protocol TabBarResourceProvider {

    /// a tabBar controller storyboard where the manipulations to nake it custom were made
    var tabBarControllerStoryboard: UIStoryboard { get }
}

/// a protocol that describes an item that might be represented on a tabBar
public protocol TabBarItemPresentable {

    /// a title of a single tab
    var title: String { get }

    /// a style that needs to be applied on a single tab
    var style: TabBarItemStyleProvider { get }

    /// a view-controller (navigation-controller)
    /// that represents a single tab
    var controller: UIViewController { get }
}

/// a protocol that describes style attributes
/// that might be applied on a particular tab
public protocol TabBarItemStyleProvider {

    /// an icon on a tab in a `.normal` state
    var image: UIImage { get }

    /// an icon on a tab in a `.selected` state
    var selectedImage: UIImage { get }

    /// **override!**
    /// a tint color of the tab in a selected state
    /// setting of this property to any value but nil,
    /// will override the value set in `TabBarStyleProvider` protocol
    var activeColor: UIColor? { get }

    /// **override!**
    /// a tint color of the tab in a normal state
    /// setting of this property to any value but nil,
    /// will override the value set in `TabBarStyleProvider` protocol
    var inactiveColor: UIColor? { get }

    /// **override!**
    /// a font of the title on the tab
    /// setting of this property to any value but nil,
    /// will override the value set in `TabBarStyleProvider` protocol
    var font: UIFont? { get }
}

/// an input of the TabBar module
/// the only way to interact with the module from the outside of the app
public protocol TabBarInput {

    /// select tab at specified index
    func selectTab(at index: Int)
}

/// an output protocol of the TabBar module
/// this is the only way of how to know about the events
/// which took place in the module, from outside of the module
public protocol TabBarOutput: UITabBarControllerDelegate {

    /// before selecting the item at tab bar it should ask it's output of is it possible or not
    ///
    /// - Parameters:
    ///     - tabBar: a tab bar where selection attempt has took place
    ///     - item: an item which is about to be shown
    ///     - index: an item index
    func tabBar(_ tabBar: UITabBar, shouldSelect item: UIViewController, at index: Int) -> Bool

    /// if some of the preparations are required to be done before selecting an item
    /// this method should be implemented
    /// when the preparations are done, `selectTab(at index: Int)` should be called manually
    func prepareSelection(for item: UIViewController, at index: Int)
}
