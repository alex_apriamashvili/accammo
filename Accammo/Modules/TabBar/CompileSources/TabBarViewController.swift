
import UI

final class TabBarViewController: UITabBarController {

    weak var output: TabBarOutput?

    override var delegate: UITabBarControllerDelegate? {
        didSet {
            guard let conformable = delegate as? TabBarOutput else { return }
            output = conformable
        }
    }

    override var tabBar: BottomTabBar {
        guard let tabBar = super.tabBar as? BottomTabBar else { fatalError("tabBar class should be `BottomTabBar`") }
        return tabBar
    }

    override var selectedIndex: Int {
        didSet {
            tabBar.select(at: selectedIndex)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
      if let index = tabBar.items?.firstIndex(of: item),
            let viewController = viewControllers?[index] {
            guard output?.tabBar(tabBar, shouldSelect: viewController, at: index) ?? true else {
                output?.prepareSelection(for: viewController, at: index)
                return
            }
            self.tabBar.select(at: index)
        }
    }

    func applyDependencies(_ dependencies: TabBarModuleDependencies) {
        tabBar.applyStyle(style: dependencies.style)
    }
}

// MARK: - TabBarInput Implementation

extension TabBarViewController: TabBarInput {

    func selectTab(at index: Int) {
        if let viewController = viewControllers?[index] {
            _ = output?.tabBarController?(self, shouldSelect: viewController)
        }
        selectedIndex = index
    }
}

// MARK: - TabBarOutput :: Default Implementation

extension TabBarOutput {

    func tabBar(_ tabBar: UITabBar, shouldSelect item: UIViewController, at index: Int) -> Bool {
        return true
    }

    func prepareSelection(for item: UIViewController, at index: Int) {}
}

// MARK: - Private

private extension TabBarViewController {

    func setupUI() {
        tabBar.isTranslucent = false

        view.accessibilityIdentifier = TabBarAccessibility.mainView.rawValue
        tabBar.accessibilityIdentifier = TabBarAccessibility.tabBar.rawValue
    }
}
