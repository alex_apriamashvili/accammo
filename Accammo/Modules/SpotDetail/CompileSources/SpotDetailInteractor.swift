
import Foundation
import Service

final class SpotDetailInteractor {

    weak var output: SpotDetailInteractorOutput?

    private let service: SpotFinderService?

    init(service: SpotFinderService) {
        self.service = service
    }
}

// MARK: - SpotDetailInteractorInput Implementation

extension SpotDetailInteractor: SpotDetailInteractorInput {

    func fetchDetails(id: Int) {

    }
}
