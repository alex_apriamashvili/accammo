
import UI
import Domain
import Service

public final class SpotDetailModuleBuilder {

    public static func build(output: SpotDetailModuleOutput?,
                      spot: Spot,
                      dependencies: SpotDetailModuleDependency) -> SpotDetailModule {
        let presenter = SpotDetailPresenter(dependency: dependencies, previewEntity: spot)
        let view = SpotDetailViewController(dependency: dependencies, output: presenter, title: spot.name)
        let router = SpotDetailRouter(baseViewController: view)
        let service = SpotFinderServiceImpl(client: APIClientImpl())
        let interactor = SpotDetailInteractor(service: service)

        presenter.output = output
        presenter.view = view

        presenter.router = router

        interactor.output = presenter
        presenter.interactor = interactor

        return view
    }
}
