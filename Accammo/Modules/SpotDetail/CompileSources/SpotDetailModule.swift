
import Foundation
import UI
import Domain

public typealias SpotDetailModule = SpotDetailViewController

// MARK: - Dependencies

/// a typealias that describes all the dependencies which is reuired for a module
public typealias SpotDetailModuleDependency = HasSpotDetailModuleStyleProvider & HasSpotDetailModuleResourcesPovider

/// a protocol that describes an entity
/// that brings styling attributes to a module
public protocol HasSpotDetailModuleStyleProvider {

    /// a set of styling attruibutes
    var style: SpotDetailModuleStyleProvider { get }
}

/// a protocol that describes an entity
/// that brings resources into a module
public protocol HasSpotDetailModuleResourcesPovider {

    /// a set of resources required by the module
    var resources: SpotDetailModuleResourcesPovider { get }
}

/// a protocol that describes styles that are required by the module
public protocol SpotDetailModuleStyleProvider {

    /// a main background color
    var backgroundColor: UIColor { get }

    /// a main tint color
    var tintColor: UIColor { get }

    // Cells

    var elementTitleFont: UIFont { get }
    var elementTitleColor: UIColor { get }
    var elementTextFont: UIFont { get }
    var elementTextColor: UIColor { get }
    var wageAmountFont: UIFont { get }
    var wageAmountColor: UIColor { get }
    var wageBaseFont: UIFont { get }
    var wageBaseColor: UIColor { get }
    var ratingValueFont: UIFont { get }
    var ratingValueColor: UIColor { get }
}

/// a protocol that describes resources that are required by the module
public protocol SpotDetailModuleResourcesPovider {

    /// a localized value of a description title
    var descriptionTitle: String { get }

    /// a localized value of a wage title
    var wageTitle: String { get }

    /// a localized value of a wage title
    var ratingTitle: String { get }
}

// MARK: - Module I/O Protocols

/// an interface that holds methods to communicate with the module
public protocol SpotDetailModuleInput {}

/// a protocol that contains methods to handle events
/// that may occur in the module
public protocol SpotDetailModuleOutput: class {

    /// tells a receiver that the presentation was finished
    /// and the view controller is about to de removed from the UI hierarchy
    func didFinishPresentation()
}

// MARK: Interactor

/// a protocol that describes a communication interface
/// for the module interactor
protocol SpotDetailInteractorInput {

    /// ask interactor to call a dedicated service
    /// to obtain information for a particular spot
    /// - parameter id: a spot unique identifier
    func fetchDetails(id: Int)
}

protocol SpotDetailInteractorOutput: class {

    /// in case if user details were successfully obtained
    /// this method brings a profile info to represent it
    func present(spot: SpotDetail)

    /// in case if an error occured during the user info fetching,
    /// this method will be called to provide information about an error
    func present(error: Error)
}

// MARK: - View

/// a ptorocol that defines actions that could been done to a view
protocol SpotDetailViewInput: class {

    /// tells the receiver to update a profile
    /// according to a provided view-model
    /// - parameter profile: a view-model that describes the data that should be represented on a screen
    func update(spot: SpotDetailViewModel)

    /// tells the reciver that an error has occured
    /// - parameter message: a localized error description that needs to be shown
    func showError(message: String)
}

/// a protocol that holds the events that might happen for the view
/// basically it's the only way of how view might tell about it's changes
protocol SpotDetailViewOutput {

    /// tells the reCeiver that the view is ready to be changed
    /// at this moment, all view components are loaded into memory
    func viewIsReady()

    /// tells the receiver that
    /// a reload action has been performed and needs to be handeled
    func handleReloadAction()

    /// tells the receiver that user wants to dismiss the view without any changes applied
    func handleDismiss()
}

// MARK: - Router

/// a protocol that describes routing actions
protocol SpotDetailRouterInput {

    /// this method should be called if next view-countroller
    /// **shouldn't** be put into a navigation stack
    func present(_ viewController: UIViewController)

    /// this method should be called if next view-countroller
    /// **needs** to be put into a navigation stack
    func push(_ viewController: UIViewController)
}

