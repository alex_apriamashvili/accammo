
import Foundation
import Domain

// MARK: - Public

final class SpotDetailPresenter {

    typealias Dependency = HasSpotDetailModuleStyleProvider & HasSpotDetailModuleResourcesPovider

    weak var output: SpotDetailModuleOutput?
    weak var view: SpotDetailViewInput?
    var interactor: SpotDetailInteractorInput?
    var router: SpotDetailRouterInput?

    private(set) var dependency: Dependency
    private let previewEntity: Spot

    init(dependency: Dependency, previewEntity: Spot) {
        self.dependency = dependency
        self.previewEntity = previewEntity
    }
}

// MARK: - Private

private extension SpotDetailPresenter {}

// MARK: - SpotDetailInteractorOutput Implementation

extension SpotDetailPresenter: SpotDetailInteractorOutput {
    
    func present(spot: SpotDetail) {
        let viewModel = SpotDetailModuleMapper.map(
            entity: spot,
            style: dependency.style,
            resource: dependency.resources)
        view?.update(spot: viewModel)
    }

    func present(error: Error) {
        view?.showError(message: error.localizedDescription)
    }
    
}

// MARK: - SpotDetailViewOutput Implementation

extension SpotDetailPresenter: SpotDetailViewOutput {

    func viewIsReady() {
        let previewDetail = SpotDetailModuleMapper.map(
            preview: previewEntity,
            style: dependency.style,
            resource: dependency.resources
        )
        view?.update(spot: previewDetail)
        interactor?.fetchDetails(id: previewEntity.identifier)

    }

    func handleReloadAction() {
        interactor?.fetchDetails(id: previewEntity.identifier)
    }

    func handleDismiss() {
        output?.didFinishPresentation()
    }
}

// MARK: - SpotDetailModuleInput Implementation

extension SpotDetailPresenter: SpotDetailModuleInput {}

// MARK: - SpotDetailCollectionViewSourceActionHandler Implementation

extension SpotDetailPresenter: SpotDetailCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int) {}
}

