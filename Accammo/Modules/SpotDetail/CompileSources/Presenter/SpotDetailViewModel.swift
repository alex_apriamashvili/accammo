
import UI

struct SpotDetailViewModel {

    /// a view model that describes a particular spot info
    var spot: SpotDetailViewModel.Spot

    /// a style that has to be applied on the elements
    var style: SpotDetailModuleStyleProvider

    /// a set of resources that are needed by the view components
    var resource: SpotDetailModuleResourcesPovider
}

extension SpotDetailViewModel {

    struct Spot {

        /// a title of a spot
        /// basically just a name of it
        let title: String

        /// spot description
        /// few words about the spot itself to lure the musition
        let description: String

        /// an amount of money
        /// that is offered an a compensation for an assistance
        let wage: String

        /// a wage basis (e.g.: per hour, per set, etc.)
        let basis: String

        /// a URL to the image of the spot
        /// a fancy image that takes almost the whole cell
        /// to catch an eye of a mucician
        let imageURL: URL

        /// a size of an image
        let imageSize: CGSize

        /// a type of a spot that needs to be displayed
        /// (e.g.: Bar, Studio, etc.)
        let type: String

        /// a number from 0 to 5
        /// that represents a rating for a particular spot
        let rating: String
    }

}
