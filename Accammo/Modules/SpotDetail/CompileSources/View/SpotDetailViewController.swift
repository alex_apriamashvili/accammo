
import Foundation
import UI

public final class SpotDetailViewController: UIViewController {

    typealias Dependency = HasSpotDetailModuleStyleProvider & HasSpotDetailModuleResourcesPovider
    typealias Output = SpotDetailViewOutput & SpotDetailCollectionViewSourceActionHandler


    let output: Output

    private let dependency: Dependency
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private let source: SpotDetailCollectionViewSource
    private let refreshControl = UIRefreshControl()

    // MARK: - Initialization

    init(dependency: Dependency, output: Output, title: String) {
        self.dependency = dependency
        self.output = output
        source = SpotDetailCollectionViewSource(actionHandler: output)
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        output.viewIsReady()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      if #available(iOS 11.0, *) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
      }
    }

    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        output.handleDismiss()
    }

    // MARK: - Actions

    @objc
    func pullToRefresh() {
        output.handleReloadAction()
    }
}

// MARK: - SpotsViewInput Implementation

extension SpotDetailViewController: SpotDetailViewInput {

    func update(spot: SpotDetailViewModel) {
        refreshControl.endRefreshing()
        source.viewModel = spot
        collectionView.reloadData()
    }

    func showError(message: String) {}
}

// MARK: - Private

private extension SpotDetailViewController {

    func configureUI() {
        view.backgroundColor = dependency.style.backgroundColor
        configureCollectionView()
        configureNavigationBar()
        configureRefreshControl()
    }

    func configureNavigationBar() {
        navigationController?.navigationBar.tintColor = dependency.style.tintColor
    }

    func configureRefreshControl() {
        collectionView.alwaysBounceVertical = true
        refreshControl.tintColor = dependency.style.tintColor
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
}

// MARK: - CollectionView

private extension SpotDetailViewController {

    func configureCollectionView() {
        collectionView.backgroundColor = dependency.style.backgroundColor

        view.addSubview(collectionView)

        registerHeader()
        registerPriceCell()
        registerRatingCell()
        registerDescriptionCell()

        collectionView.dataSource = source
        collectionView.delegate = source

        constrainCollectionView()
    }

    func registerHeader() {
        collectionView.register(
            SpotDetailHeaderView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.header
        )
    }

    func registerPriceCell() {
        collectionView.register(
            SpotDetailPriceCell.self,
            forCellWithReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.price
        )
    }

    func registerRatingCell() {
        collectionView.register(
            SpotDetailRatingCell.self,
            forCellWithReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.rating
        )
    }

    func registerDescriptionCell() {
        collectionView.register(
            SpotDetailDescriptionCell.self,
            forCellWithReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.description
        )
    }

    func constrainCollectionView() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
