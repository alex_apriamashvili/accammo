
import UI

protocol SpotDetailCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int)
}

final class SpotDetailCollectionViewSource: NSObject {

    struct CellIdentifiers {

        static let header = "com.accammo.spot.detail.header.cell.identifier"
        static let price = "com.accammo.spot.detail.price.cell.identifier"
        static let rating = "com.accammo.spot.detail.rating.cell.identifier"
        static let description = "com.accammo.spot.detail.description.cell.identifier"
    }

    var viewModel: SpotDetailViewModel?

    private let actionHandler: SpotDetailCollectionViewSourceActionHandler
    private let pricePrototypeCell: SpotDetailPriceCell = SpotDetailPriceCell()
    private let ratingPrototypeCell: SpotDetailRatingCell = SpotDetailRatingCell()
    private let descriptionPrototypeCell: SpotDetailDescriptionCell = SpotDetailDescriptionCell()

    init(actionHandler: SpotDetailCollectionViewSourceActionHandler) {
        self.actionHandler = actionHandler
        super.init()
    }

    enum CellType: Int {
        case price = 0
        case rating
        case description
    }
}

extension SpotDetailCollectionViewSource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cellType = CellType(rawValue: indexPath.item) else { fatalError("cannot initiate a cell type") }
        switch cellType {
        case .price:
            return configurePriceCell(collectionView: collectionView, indexPath: indexPath)
        case .rating:
            return configureRatingCell(collectionView: collectionView, indexPath: indexPath)
        case .description:
            return configureDescriptionCell(collectionView: collectionView, indexPath: indexPath)
        }
    }
}

extension SpotDetailCollectionViewSource: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionHandler.handleCellTap(at: indexPath.item)
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.header,
            for: indexPath
        )

        guard let header = headerView as? SpotDetailHeaderView,
            let viewModel = viewModel else { return headerView }
        header.configure(viewModel: viewModel.spot)

        return headerView
    }
}

extension SpotDetailCollectionViewSource: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cellType = CellType(rawValue: indexPath.item) else { fatalError("cannot initiate a cell type") }
        guard let viewModel = viewModel else { return .zero }
        let width = collectionView.bounds.width

        switch cellType {
        case .price:
            return sizeForPrice(viewModel: viewModel, width: width)
        case .rating:
            return sizeForRating(viewModel: viewModel, width: width)
        case .description:
            return sizeForDescription(viewModel: viewModel, width: width)
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = collectionView.bounds.width
        guard let viewModel = viewModel?.spot else { return .zero }
        return headerSize(viewModel, width: width)
    }
}

// MARK: - Sizing

private extension SpotDetailCollectionViewSource {

    func sizeForPrice(viewModel: SpotDetailViewModel, width: CGFloat) -> CGSize {
        let widthContraint = pricePrototypeCell.contentView.widthAnchor.constraint(equalToConstant: width)
        widthContraint.isActive = true

        pricePrototypeCell.contentView.translatesAutoresizingMaskIntoConstraints = false
        pricePrototypeCell.configure(
            price: viewModel.spot.wage,
            basis: viewModel.spot.basis,
            style: viewModel.style,
            resource: viewModel.resource
        )

        pricePrototypeCell.contentView.setNeedsUpdateConstraints()
        pricePrototypeCell.contentView.updateConstraintsIfNeeded()
        pricePrototypeCell.contentView.setNeedsLayout()
        pricePrototypeCell.contentView.layoutIfNeeded()

      let size = pricePrototypeCell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        widthContraint.isActive = false
        pricePrototypeCell.removeConstraint(widthContraint)

        return CGSize(width: width, height: size.height)
    }

    func sizeForRating(viewModel: SpotDetailViewModel, width: CGFloat) -> CGSize {
        let widthContraint = ratingPrototypeCell.contentView.widthAnchor.constraint(equalToConstant: width)
        widthContraint.isActive = true

        ratingPrototypeCell.contentView.translatesAutoresizingMaskIntoConstraints = false
        ratingPrototypeCell.configure(
            rating: viewModel.spot.rating,
            style: viewModel.style,
            resource: viewModel.resource
        )

        ratingPrototypeCell.contentView.setNeedsUpdateConstraints()
        ratingPrototypeCell.contentView.updateConstraintsIfNeeded()
        ratingPrototypeCell.contentView.setNeedsLayout()
        ratingPrototypeCell.contentView.layoutIfNeeded()

      let size = ratingPrototypeCell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        widthContraint.isActive = false
        ratingPrototypeCell.removeConstraint(widthContraint)

        return CGSize(width: width, height: size.height)
    }

    func sizeForDescription(viewModel: SpotDetailViewModel, width: CGFloat) -> CGSize {
        let widthContraint = descriptionPrototypeCell.contentView.widthAnchor.constraint(equalToConstant: width)
        widthContraint.isActive = true

        descriptionPrototypeCell.contentView.translatesAutoresizingMaskIntoConstraints = false
        descriptionPrototypeCell.configure(
            description: viewModel.spot.description,
            style: viewModel.style,
            resource: viewModel.resource
        )

        descriptionPrototypeCell.contentView.setNeedsUpdateConstraints()
        descriptionPrototypeCell.contentView.updateConstraintsIfNeeded()
        descriptionPrototypeCell.contentView.setNeedsLayout()
        descriptionPrototypeCell.contentView.layoutIfNeeded()

      let size = descriptionPrototypeCell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        widthContraint.isActive = false
        descriptionPrototypeCell.removeConstraint(widthContraint)

        return CGSize(width: width, height: size.height)
    }

    func headerSize(_ viewModel: SpotDetailViewModel.Spot, width: CGFloat) -> CGSize {
        return viewModel.imageSize
    }
}

// MARK: - Configuration

private extension SpotDetailCollectionViewSource {

    func configurePriceCell(collectionView: UICollectionView, indexPath: IndexPath) -> SpotDetailPriceCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.price,
            for: indexPath
        )
        guard let priceCell = cell as? SpotDetailPriceCell else { fatalError("cell must be of type SpotDetailPriceCell") }
        guard let vm = viewModel else { fatalError("view model must be set") }
        priceCell.configure(
            price: vm.spot.wage,
            basis: vm.spot.basis,
            style: vm.style,
            resource: vm.resource
        )
        return priceCell
    }

    func configureRatingCell(collectionView: UICollectionView, indexPath: IndexPath) -> SpotDetailRatingCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.rating,
            for: indexPath
        )
        guard let ratingCell = cell as? SpotDetailRatingCell else { fatalError("cell must be of type SpotDetailRatingCell") }
        guard let vm = viewModel else { fatalError("view model must be set") }

        ratingCell.configure(
            rating: vm.spot.rating,
            style: vm.style,
            resource: vm.resource
        )

        return ratingCell
    }

    func configureDescriptionCell(collectionView: UICollectionView, indexPath: IndexPath) -> SpotDetailDescriptionCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: SpotDetailCollectionViewSource.CellIdentifiers.description,
            for: indexPath
        )
        guard let descriptionCell = cell as? SpotDetailDescriptionCell else { fatalError("cell must be of type SpotDetailDescriptionCell") }
        guard let vm = viewModel else { fatalError("view model must be set") }

        descriptionCell.configure(
            description: vm.spot.description,
            style: vm.style,
            resource: vm.resource
        )

        return descriptionCell
    }
}
