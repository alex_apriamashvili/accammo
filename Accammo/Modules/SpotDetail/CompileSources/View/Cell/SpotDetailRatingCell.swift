
import UI

class SpotDetailRatingCell: UICollectionViewCell {

    private let titleLabel: UILabel = UILabel()
    private let valueLabel: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func configure(rating: String,
                   style: SpotDetailModuleStyleProvider,
                   resource: SpotDetailModuleResourcesPovider) {
        titleLabel.text = resource.ratingTitle
        valueLabel.text = rating

        apply(style: style)

//        setNeedsLayout()
//        layoutIfNeeded()
    }
}

private extension SpotDetailRatingCell {

    func configureUI() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(valueLabel)

        constrainViews()
    }

    func apply(style: SpotDetailModuleStyleProvider) {
        titleLabel.textColor = style.elementTitleColor
        titleLabel.font = style.elementTitleFont

        valueLabel.textColor = style.ratingValueColor
        valueLabel.font = style.ratingValueFont
    }
}

private extension SpotDetailRatingCell {

    struct Layout {

        static let elementSpacing: CGFloat = 8.0
    }

    func constrainViews() {
        constrainTitle()
        constrainValue()
    }

    func constrainTitle() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: valueLabel.leadingAnchor, constant: -Layout.elementSpacing).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor).isActive = true
    }

    func constrainValue() {
        valueLabel.translatesAutoresizingMaskIntoConstraints = false

        valueLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor).isActive = true
        valueLabel.topAnchor.constraint(equalTo: titleLabel.topAnchor).isActive = true
        valueLabel.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
    }
}
