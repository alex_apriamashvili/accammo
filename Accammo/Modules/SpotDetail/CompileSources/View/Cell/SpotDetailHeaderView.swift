
import UI
import SDWebImage

final class SpotDetailHeaderView: UICollectionReusableView {

    let image: UIImageView = UIImageView()
    var imageHeightConstraint: NSLayoutConstraint?
    static let maxAvailableHeight: CGFloat = 350

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func configure(viewModel: SpotDetailViewModel.Spot) {
        image.sd_setImage(with: viewModel.imageURL, completed: nil)
        imageHeightConstraint?.constant = viewModel.imageSize.height

        setNeedsLayout()
        layoutIfNeeded()
    }
}

private extension SpotDetailHeaderView {

    func configureUI() {
        self.clipsToBounds = true
        addSubview(image)
        constrainViews()
    }
}

private extension SpotDetailHeaderView {

    func constrainViews() {
        constrainImage()
    }

    func constrainImage() {
        image.translatesAutoresizingMaskIntoConstraints = false
        image.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        image.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        image.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageHeightConstraint = image.heightAnchor.constraint(equalToConstant: 1)
        imageHeightConstraint?.isActive = true
    }
}
