
import UI

class SpotDetailDescriptionCell: UICollectionViewCell {

    private let titleLabel: UILabel = UILabel()
    private let textLabel: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func configure(description: String,
                   style: SpotDetailModuleStyleProvider,
                   resource: SpotDetailModuleResourcesPovider) {
        titleLabel.text = resource.descriptionTitle
        textLabel.text = description

        apply(style: style)

        setNeedsLayout()
        layoutIfNeeded()
    }
}

private extension SpotDetailDescriptionCell {

    func configureUI() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(textLabel)

        constrainViews()
    }

    func apply(style: SpotDetailModuleStyleProvider) {
        titleLabel.textColor = style.elementTitleColor
        titleLabel.font = style.elementTitleFont

        textLabel.textColor = style.elementTextColor
        textLabel.font = style.elementTextFont
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byWordWrapping
    }
}

private extension SpotDetailDescriptionCell {

    func constrainViews() {
        constrainTitle()
        constrainText()
    }

    func constrainTitle() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: textLabel.topAnchor).isActive = true
    }

    func constrainText() {
        textLabel.translatesAutoresizingMaskIntoConstraints = false

        textLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        textLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor).isActive = true
    }
}
