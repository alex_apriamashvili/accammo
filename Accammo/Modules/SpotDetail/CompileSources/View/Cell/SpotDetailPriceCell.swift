
import UI

class SpotDetailPriceCell: UICollectionViewCell {

    private let titleLabel: UILabel = UILabel()
    private let moneyLabel: UILabel = UILabel()
    private let baseLabel: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func configure(price: String,
                   basis: String,
                   style: SpotDetailModuleStyleProvider,
                   resource: SpotDetailModuleResourcesPovider) {
        titleLabel.text = resource.wageTitle
        moneyLabel.text = price
        baseLabel.text = basis

        apply(style: style)

//        setNeedsLayout()
//        layoutIfNeeded()
    }
}

private extension SpotDetailPriceCell {

    func configureUI() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(moneyLabel)
        contentView.addSubview(baseLabel)

        constrainViews()
    }

    func apply(style: SpotDetailModuleStyleProvider) {
        titleLabel.textColor = style.elementTitleColor
        titleLabel.font = style.elementTitleFont

        moneyLabel.textColor = style.wageAmountColor
        moneyLabel.font = style.wageAmountFont

        baseLabel.textColor = style.wageBaseColor
        baseLabel.font = style.wageBaseFont
        baseLabel.numberOfLines = 2
        baseLabel.lineBreakMode = .byWordWrapping
    }
}

private extension SpotDetailPriceCell {

    struct Layout {
        static let elementSpacing: CGFloat = 8.0
    }

    func constrainViews() {
        constrainTitle()
        constrainMoney()
        constrainBase()
    }

    func constrainTitle() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: moneyLabel.leadingAnchor, constant: -Layout.elementSpacing).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor).isActive = true
    }

    func constrainMoney() {
        moneyLabel.translatesAutoresizingMaskIntoConstraints = false

        moneyLabel.trailingAnchor.constraint(equalTo: baseLabel.leadingAnchor, constant: -Layout.elementSpacing).isActive = true
        moneyLabel.topAnchor.constraint(equalTo: titleLabel.topAnchor).isActive = true
        moneyLabel.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
    }

    func constrainBase() {
        baseLabel.translatesAutoresizingMaskIntoConstraints = false

        baseLabel.setContentHuggingPriority(.required, for: .horizontal)
        baseLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor).isActive = true
        baseLabel.bottomAnchor.constraint(equalTo: moneyLabel.bottomAnchor).isActive = true
        baseLabel.topAnchor.constraint(equalTo: moneyLabel.topAnchor).isActive = true
    }
}
