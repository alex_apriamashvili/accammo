
import Foundation

public struct SpotDetail {

    public var spot: Spot

    public init(spot: Spot) {
        self.spot = spot
    }
}
