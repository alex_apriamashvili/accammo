
import Foundation

public protocol SearchCriteriaContainer {

    func currentSearchCriteria() -> SearchCriteria
    func update(filter: SearchCriteria.Filter)
}

public final class SearchCriteriaContainerImpl: SearchCriteriaContainer {

    lazy var searchCriteria = defaultSearchCriteria()

    public init() {}

    public func currentSearchCriteria() -> SearchCriteria {
        return searchCriteria
    }

    public func update(filter: SearchCriteria.Filter) {
        searchCriteria.filter = filter
    }
}

private extension SearchCriteriaContainerImpl {

    struct Defaults {

        typealias FilterRange = SearchCriteria.Filter.Range

        static let spotInfo: Spot.SpotType = .studio
        static let locationName: String = "Middle Earth"
        static let latitude: Double = 0.0
        static let longitude: Double = 0.0
        static let rating: [Int] = []
        static let skills: [SearchCriteria.Skill] = []
        static let priceRange: FilterRange = FilterRange(min: 0.0, selectedMin: 0.0, max: 1000, selectedMax: 1000)
    }

    func defaultSearchCriteria() -> SearchCriteria {
        return SearchCriteria(
            spotInfo: Defaults.spotInfo,
            filter: defaultFilter()
        )
    }

    func defaultFilter() -> SearchCriteria.Filter {
        return SearchCriteria.Filter(
            spotName: String(),
            rating: Defaults.rating,
            wage: Defaults.priceRange,
            skills: Defaults.skills
        )
    }
}
