
import Foundation

public struct Profile {

    public var avatarImageURL: URL
    public var avatarWidth: Double
    public var avatarHeight: Double
    public var experienceMonths: Int
    public var firstName: String
    public var lastName: String
    public var title: String
    public var biography: String

    public init(avatarImageURL: URL,
                avatarWidth: Double,
                avatarHeight: Double,
                experienceMonths: Int,
                firstName: String,
                lastName: String,
                title: String,
                biography: String) {
        self.avatarImageURL = avatarImageURL
        self.avatarWidth = avatarWidth
        self.avatarHeight = avatarHeight
        self.experienceMonths = experienceMonths
        self.firstName = firstName
        self.lastName = lastName
        self.title = title
        self.biography = biography
    }
}
