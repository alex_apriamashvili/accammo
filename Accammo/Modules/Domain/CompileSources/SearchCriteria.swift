
import Foundation

public struct SearchCriteria {

    public var spotInfo: Spot.SpotType
    public var filter: Filter

    public init(spotInfo: Spot.SpotType,
                filter: Filter) {
        self.spotInfo = spotInfo
        self.filter = filter
    }
}

// MARK: - Filter

public extension SearchCriteria {

    struct Filter {

        public var spotName: String
        public var rating: [Int]
        public var wage: Range
        public var skills: [Skill]

        public init(spotName: String,
                    rating: [Int],
                    wage: Range,
                    skills: [Skill]) {
            self.spotName = spotName
            self.rating = rating
            self.wage = wage
            self.skills = skills
        }
    }
}

// MARK: - Skills

public extension SearchCriteria {

    enum Skill: String {

        case singing
        case guitar
        case bass
        case drums
        case piano
        case sax
    }
}

// MARK: - Range

public extension SearchCriteria.Filter {

    struct Range {

        public var min: Double
        public var selectedMin: Double
        public var max: Double
        public var selectedMax: Double

        public init(min: Double,
                    selectedMin: Double,
                    max: Double,
                    selectedMax: Double) {
            self.min = min
            self.max = max
            self.selectedMin = selectedMin
            self.selectedMax = selectedMax
        }
    }
}
