
import Foundation

/// a structure that describes a spot
/// that a musician could be interested in
public struct Spot {

    /// an enum that provides all possible types of spots
    public enum SpotType: String {
        case bar
        case club
        case apartment
        case studio
        case band
    }

    /// a unique identifier of the spot
    public let identifier: Int

    /// a name of a spot.
    /// basically represents the title that should Be shown in an ad
    public let name: String

    /// a description of a spot.
    /// few words about the place and benefits
    public let description: String

    /// a rating of a spot.
    /// the value of how people do trust in this spot
    public let rating: Double

    /// an image that shoild be displayed
    public let image: Image

    /// a wadge that spot offers.
    /// an amout of many for a given basis
    /// that a misician might get by joining the spot
    public let wage: Wage

    /// a type of a spot
    /// whether the club or a music studio or an apartment
    public let type: SpotType

    /// a public initializer for Spot
    public init(identifier: Int,
                name: String,
                description: String,
                rating: Double,
                image: Image,
                wage: Wage,
                type: SpotType) {
        self.identifier = identifier
        self.name = name
        self.description = description
        self.rating = rating
        self.image = image
        self.wage = wage
        self.type = type
    }
}

/// a structure that describes a wage
/// that is offered or required
public struct Wage {

    /// an enum that describes all possible type of basis
    /// that a wage can be calculated on
    public enum Basis {
        case set
        case day
        case hour
        case tour
    }

    /// an amount of money that is offered as a wage
    public let amount: Double

    /// a currency of the amount which is offered
    public let currency: Currency

    /// a basis of the wage
    public let basis: Basis

    /// a public initializer for Wage
    public init(amount: Double,
                currency: Currency,
                basis: Basis) {
        self.amount = amount
        self.currency = currency
        self.basis = basis
    }
}

/// a structure that describes currency
public struct Currency {

    /// a currency symbol that might be displayed
    public let symbol: String

    /// a currency code
    public let code: String

    /// a public initializer for Currency
    public init(symbol: String,
                code: String) {
        self.symbol = symbol
        self.code = code
    }
}

/// a structure that describes a spot picture
public struct Image {

    /// /// a URL to the photo that needs to be displayed
    public let imageURL: URL

    /// original width of a photo
    public let width: Double

    /// original height of a photo
    public let height: Double

    /// a public initializer for Currency
    public init(imageURL: URL,
                width: Double,
                height: Double) {
        self.imageURL = imageURL
        self.width = width
        self.height = height
    }
}
