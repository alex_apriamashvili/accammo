
import UI

public final class SearchFilterViewController: UIViewController {

    typealias Dependencies = HasSearchFilterStyleProvider & HasSearchFilterTranslationProvider
    var dependencies: Dependencies?

    var presenter: SearchFilterViewOutput?
    var collectionViewAdapter: CollectionViewAdapter?

    var bottomViewBottomConstraint: NSLayoutConstraint?
    var bottomView: FilterBottomView?
    let animationDuration: TimeInterval = 0.2
    let animationSpeedAcceleration: Float = 1.5
    var keyboardViewEndFrame: CGRect = CGRect.zero

    private var pendingLayoutOperations: [LayoutOperation] = []
    private var canExecuteLayoutOperations: Bool = false
    private var shouldExecuteLayoutOperations: Bool = true
    private var collectionView: UICollectionView = UICollectionView(
        frame: CGRect.zero,
        collectionViewLayout: CustomAnimationFlowLayout()
    )

    private var isChangesSubmitted: Bool = false

    // MARK: - View LifeCycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        presenter?.viewIsReady()
        setupKeyboardNotifications()
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      if #available(iOS 11.0, *) {
        navigationController?.navigationBar.prefersLargeTitles = false
      }
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
        if !isChangesSubmitted {
            presenter?.handleDismiss()
        }

    }

    override public func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if !pendingLayoutOperations.isEmpty &&
            shouldExecuteLayoutOperations {
            shouldExecuteLayoutOperations = false
            pendingLayoutOperations.forEach { $0() }
            pendingLayoutOperations.removeAll()
        }
        canExecuteLayoutOperations = true
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override public func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
    }
}

// MARK: - SearchFilterViewInput

extension SearchFilterViewController: SearchFilterViewInput {

    func updateContents(_ contents: [SectionViewModel]) {
        collectionViewAdapter?.apply(contents, on: collectionView)
    }

    func hideKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - CollectionViewAdapterDelegate

extension SearchFilterViewController: CollectionViewAdapterDelegate {

    private struct SectionGroup {
        private static let sort: Int = 0
        static func isSort(_ indexPath: IndexPath) -> Bool {
            return indexPath.section == sort
        }
    }

    public func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               didSelect viewModel: Any,
                               at indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
    }

    public func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               scrollViewDidScroll: UIScrollView) {
        hideKeyboard()
    }
}

// MARK: - Keyboard

private extension SearchFilterViewController {

    func setupKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(SearchFilterViewController.adjustForKeyboard),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        notificationCenter.addObserver(
            self,
            selector: #selector(SearchFilterViewController.adjustForKeyboard),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil
        )
    }

    @objc func adjustForKeyboard(notification: Notification) {
        guard let userInfo = notification.userInfo,
          let keyboardScreenEndFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame.cgRectValue, from: view.window)

      if notification.name == UIResponder.keyboardWillHideNotification {
            collectionView.contentInset = UIEdgeInsets.zero
        } else {
            collectionView.contentInset = UIEdgeInsets(
                top: 0,
                left: 0,
                bottom: keyboardViewEndFrame.height,
                right: 0
            )
        }
        collectionView.scrollIndicatorInsets = collectionView.contentInset
    }
}

// MARK: - Setup Views

private extension SearchFilterViewController {

    func setupViews() {
        view.backgroundColor = dependencies?.styleProvider.backgroundColor

        setupCollectionView()
        registerCells()
        setupBottomView()
        title = dependencies?.translationProvider.screenTitle
        navigationController?.navigationBar.tintColor = dependencies?.styleProvider.tintColor
        guard let buttonTitle = dependencies?.translationProvider.filterText else { return }
        bottomView?.configureFilter(title: buttonTitle)
        executeLayoutOperationIfPossiible(showView)
    }

    func setupCollectionView() {
        view.addSubview(collectionView)

        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = dependencies?.styleProvider.backgroundColor

        collectionView.dataSource = collectionViewAdapter
        collectionView.delegate = collectionViewAdapter
        collectionViewAdapter?.delegate = self

        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }

    func registerCells() {
        registerSectionTitleCell()
        registerDescriptionCell()
        registerSpotNameCell()
        registerStarRatingCell()
        registerWageCell()
        registerCloudTagCell()
    }

    func registerSectionTitleCell() {
        collectionViewAdapter?.register(
            cell: SectionTitleCell.self,
            viewModel: SectionTitleViewModel.self,
            on: collectionView
        )
    }

    func registerSpotNameCell() {
        collectionViewAdapter?.register(
            cell: SpotNameCell.self,
            viewModel: SpotNameViewModel.self,
            on: collectionView
        )
    }

    func registerStarRatingCell() {
        collectionViewAdapter?.register(
            cell: StarRatingCell.self,
            viewModel: StarRatingViewModel.self,
            on: collectionView
        )
    }

    func registerCloudTagCell() {
        collectionViewAdapter?.register(
            cell: CloudTagCell.self,
            viewModel: CloudTagViewModel.self,
            on: collectionView
        )
    }

    func registerWageCell() {
        collectionViewAdapter?.register(
            cell: WageCell.self,
            viewModel: WageCellViewModel.self,
            on: collectionView)
    }

    func registerDescriptionCell() {
        collectionViewAdapter?.register(
            cell: DescriptionCell.self,
            viewModel: DescriptionViewModel.self,
            on: collectionView)
    }
}

// MARK: - bottom view

extension SearchFilterViewController: FilterBottomViewOutput {

    struct Layout {
        static let defaultConstraint: CGFloat = 0.0
        static let padding: CGFloat = 8.0
    }

    private func setupBottomView() {

        let bottomViewStyle: FilterBottomView.FilterBottomViewStyle = .single
        bottomView = FilterBottomView(style: bottomViewStyle)

        guard let bottomView = bottomView,
            let style = dependencies?.styleProvider
            else { return }

        bottomView.output = self
        bottomView.configure(with: style)

        view.addSubview(bottomView)
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.layoutIfNeeded()

        bottomView.leadingAnchor.constraint(
            equalTo: view.leadingAnchor
            ).isActive = true

        bottomView.trailingAnchor.constraint(
            equalTo: view.trailingAnchor
            ).isActive = true

        bottomView.topAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true

        bottomViewBottomConstraint = bottomView.bottomAnchor.constraint(
            equalTo: view.bottomAnchor,
            constant: bottomView.frame.height)

        bottomViewBottomConstraint?.isActive = true
    }

    private func showView() {

        let safeAreaBottom: CGFloat
        if #available(iOS 11.0, *) {
            safeAreaBottom = self.view.safeAreaInsets.bottom
        } else {
            safeAreaBottom = 0
        }

        view.setNeedsLayout()
        view.layoutIfNeeded()
        bottomViewBottomConstraint?.constant = Layout.defaultConstraint - safeAreaBottom
        UIView.animate(withDuration: animationDuration) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }

    private func hideView() {
        view.setNeedsLayout()
        view.layoutIfNeeded()
        self.bottomViewBottomConstraint?.constant = self.bottomView?.frame.height ?? 0
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        })
    }

    public func didTapFilter(button: UIButton) {
        isChangesSubmitted = true
        presenter?.applyFilter()
        hideKeyboard()
    }

    public func didTapClear() {}
}

private extension SearchFilterViewController {

    typealias LayoutOperation = () -> ()

    func executeLayoutOperationIfPossiible(_ operation: @escaping LayoutOperation) {
        if canExecuteLayoutOperations {
            operation()
        } else {
            pendingLayoutOperations.append(operation)
        }
    }
}
