
import UI

/// an output for a filter button
public protocol FilterBottomViewOutput: class {

    func didTapFilter(button: UIButton)
    func didTapClear()
}

final class FilterBottomView: UIView {

    enum FilterBottomViewStyle {
        case single
        case coupled
    }

    private var filterButton: UIButton = UIButton()
    private var clearButton: UIButton?
    private var filterButtonStyle: ButtonStyleImpl = ButtonStyleImpl.defaultStyle()
    private var clearButtonStyle: ButtonStyleImpl = ButtonStyleImpl.defaultStyle()
    private let buttonStyle: FilterBottomViewStyle

    weak var output: FilterBottomViewOutput?

    init(style: FilterBottomViewStyle) {
        buttonStyle = style
        super.init(frame: .zero)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        buttonStyle = .single
        super.init(coder: aDecoder)
        setup()
    }

    func configure(with style: SearchFilterStyleProvider) {
        filterButtonStyle = ButtonStyleImpl(
            textColor: style.whiteColor,
            titleFont: style.veryBigLightFont,
            backgroundColor: style.blueColor
        )

        clearButtonStyle = ButtonStyleImpl(
            textColor: style.blueColor,
            titleFont: style.veryBigLightFont,
            backgroundColor: style.whiteColor
        )

        applyStylesOnFilterButton()
        applyStylesOnClearButton()
    }

    func configureFilter(title: String) {
        filterButton.setTitle(title, for: .normal)
    }

    func configureClear(title: String) {
        clearButton?.setTitle(title, for: .normal)
    }

    @objc func tapFilter() {
        output?.didTapFilter(button: filterButton)
    }

    @objc func tapClear() {
        output?.didTapClear()
    }
}

// MARK: - Configuration

private extension FilterBottomView {

    func setup() {
        setupFilterButton()
        constrainFilterButton()

        if buttonStyle == .coupled {
            setupClearButton()
            constrainClearButton()
        }
    }

    func setupFilterButton() {
        filterButton.addTarget(self, action: #selector(tapFilter), for: .touchUpInside)
        addSubview(filterButton)

        applyStylesOnFilterButton()
    }

    func applyStylesOnFilterButton() {
        filterButton.setTitleColor(filterButtonStyle.textColor, for: .normal)
        filterButton.titleLabel?.font = filterButtonStyle.titleFont
        filterButton.backgroundColor = filterButtonStyle.backgroundColor
    }

    func setupClearButton() {
        let cButton = UIButton()
        cButton.addTarget(self, action: #selector(tapClear), for: .touchUpInside)
        addSubview(cButton)

        clearButton = cButton

        applyStylesOnClearButton()
    }

    func applyStylesOnClearButton() {
        clearButton?.setTitleColor(clearButtonStyle.textColor, for: .normal)
        clearButton?.titleLabel?.font = clearButtonStyle.titleFont
        clearButton?.backgroundColor = clearButtonStyle.backgroundColor
        clearButton?.layer.borderColor = clearButtonStyle.textColor.cgColor
        clearButton?.layer.borderWidth = clearButtonStyle.borderWidth
    }
}

// MARK: - Layout

private extension FilterBottomView {

    struct Layout {
        static let padding: CGFloat = 8
        static let height: CGFloat = 44
        static let clearToFilterRatio: CGFloat = 0.6
    }

    func constrainFilterButton() {
        filterButton.translatesAutoresizingMaskIntoConstraints = false

        filterButton.leadingAnchor.constraint(
            equalTo: leadingAnchor,
            constant: Layout.padding
            ).isActive = true

        if buttonStyle == .single {
            filterButton.trailingAnchor.constraint(
                equalTo: trailingAnchor,
                constant: -Layout.padding
                ).isActive = true
        }

        filterButton.topAnchor.constraint(
            equalTo: topAnchor,
            constant: Layout.padding
            ).isActive = true

        filterButton.bottomAnchor.constraint(
            equalTo: bottomAnchor,
            constant: -Layout.padding
            ).isActive = true

        filterButton.heightAnchor.constraint(greaterThanOrEqualToConstant: Layout.height).isActive = true
    }

    func constrainClearButton() {
        clearButton?.translatesAutoresizingMaskIntoConstraints = false

        clearButton?.leadingAnchor.constraint(
            equalTo: filterButton.trailingAnchor,
            constant: Layout.padding
            ).isActive = true

        clearButton?.widthAnchor.constraint(
            equalTo: filterButton.widthAnchor,
            multiplier: Layout.clearToFilterRatio
            ).isActive = true

        clearButton?.heightAnchor.constraint(equalTo: filterButton.heightAnchor, multiplier: 1.0).isActive = true
        clearButton?.centerYAnchor.constraint(equalTo: filterButton.centerYAnchor).isActive = true

        clearButton?.trailingAnchor.constraint(
            equalTo: trailingAnchor,
            constant: -Layout.padding
            ).isActive = true
    }
}

private struct ButtonStyleImpl {

    let textColor: UIColor
    let titleFont: UIFont
    let backgroundColor: UIColor
    let borderWidth: CGFloat = 1.0

    static func defaultStyle() -> ButtonStyleImpl {
        return ButtonStyleImpl(
            textColor: .clear,
            titleFont: .boldSystemFont(ofSize: 14),
            backgroundColor: .clear
        )
    }
}
