
import UI

// a way of interaction between the cell and it's action handler
protocol WageCellOutput: class {

    /// tells the receiver that the wage range
    /// has been changed to a specified values (min & max percentage)
    ///
    /// - parameter minPercentage: a new minimal edge for the wage range
    /// - parameter maxPercentage: a new maximal edge for the wage range
    func wageDidChange(minValue minPercentage: Double, maxValue maxPercentage: Double)

    /// tells the receiver that the price range has been change by applying a new min value
    ///
    /// - parameter minPercentage: a new minimal edge for the price range
    func wageDidChange(minValue minPercentage: Double)

    /// tells the receiver that the price range has been change by applying a new max value
    ///
    /// - parameter maxPercentage: a new maximal edge for the price range
    func wageDidChange(maxValue maxPercentage: Double)

    func minWageWillChange(to minPercentage: Double) -> String
    func maxWageWillChange(to maxPercentage: Double) -> String
}

final class WageCell: UICollectionViewCell {

    typealias Dependencies = HasSearchFilterStyleProvider
    private var dependencies: Dependencies?
    private weak var output: WageCellOutput?

    private let priceControl: SliderControlComponent.Element =
        SliderControlComponentImpl().doubleSidedSlider(lhsValue: 0.0, rhsValue: 100.0, step: 0.1,
                                                       context: SearchFilterCellIdentifier.wage.rawValue)
    private let minLabel: UILabel = UILabel()
    private let maxLabel: UILabel = UILabel()

    private var selectedMax = 0.0
    private var selectedMin = 0.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    private func setUp() {
        priceControl.register(output: self)
        contentView.addSubview(minLabel)
        contentView.addSubview(maxLabel)
        contentView.addSubview(priceControl)
        constraintViews()
    }
}

// MARK: - CellConfigurable Implementation

extension WageCell: CellConfigurable {

    func configure(with viewModel: Any, indexPath: IndexPath) {
        guard let vm = viewModel as? WageCellViewModel else { return }
        output = vm.actionHandler
        selectedMin = vm.selectedMin
        selectedMax = vm.selectedMax
        apply(vm.dependencies)
        apply(selectedMin: vm.selectedMin, selectedMax: vm.selectedMax)
        minLabel.text = vm.minResult
        maxLabel.text = vm.maxResult
        minLabel.isHidden = vm.disabled
        maxLabel.isHidden = vm.disabled
        priceControl.setDisabled(vm.disabled)
    }
}

// MARK: - Configuration

private extension WageCell {

    func apply(_ dependencies: WageCellViewModel.Dependencies) {
        self.dependencies = dependencies
        let style = dependencies.styleProvider

        let styleAdapter = SliderControlStyle(
            activeRunnerColor: style.blueColor,
            inactiveRunnerColor: style.lightGrayColor,
            thumbBorderColor: style.blueColor,
            thumbShadowColor: style.lightGrayColor,
            thumbColor: style.whiteColor,
            shouldUseRTL: false
        )

        minLabel.font = style.mediumLightFont
        minLabel.textColor = style.grayTextColor
        maxLabel.font = style.mediumLightFont
        maxLabel.textColor = style.grayTextColor

        priceControl.setStyle(styleAdapter)
        priceControl.redraw()
    }

    func apply(selectedMin: Double, selectedMax: Double) {
        priceControl.setSelectedMinValue(selectedMin)
        priceControl.setSelectedMaxValue(selectedMax)
    }
}

// MARK: - Layout

private extension WageCell {

    struct Layout {
        static let padding: CGFloat = 16
        static let longPadding: CGFloat = 20
        static let height: CGFloat = 20
        static let labelPadding: CGFloat = 4
    }

    func constraintViews() {
        translatesAutoresizingMaskIntoConstraints = false
        constraintContentView()
        constraintMinLabel()
        constraintMaxLabel()
        constraintSliderControl()
    }

    func constraintContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    func constraintMinLabel() {
        minLabel.translatesAutoresizingMaskIntoConstraints = false

        minLabel.leadingAnchor.constraint(
            equalTo: contentView.leadingAnchor,
            constant: Layout.padding
            ).isActive = true

        minLabel.topAnchor.constraint(
            equalTo: contentView.topAnchor,
            constant: Layout.labelPadding
            ).isActive = true
    }

    func constraintMaxLabel() {
        maxLabel.translatesAutoresizingMaskIntoConstraints = false

        maxLabel.leadingAnchor.constraint(
            greaterThanOrEqualTo: minLabel.trailingAnchor,
            constant: Layout.padding
            ).isActive = true

        maxLabel.trailingAnchor.constraint(
            equalTo: contentView.trailingAnchor,
            constant: -Layout.longPadding
            ).isActive = true

        maxLabel.topAnchor.constraint(
            equalTo: contentView.topAnchor,
            constant: Layout.labelPadding
            ).isActive = true
    }

    func constraintSliderControl() {
        priceControl.translatesAutoresizingMaskIntoConstraints = false

        priceControl.leadingAnchor.constraint(
            equalTo: contentView.leadingAnchor,
            constant: Layout.padding)
            .isActive = true

        priceControl.trailingAnchor.constraint(
            equalTo: contentView.trailingAnchor,
            constant: -Layout.padding
            ).isActive = true

        priceControl.topAnchor.constraint(
            equalTo: minLabel.bottomAnchor,
            constant: Layout.padding
            ).isActive = true

        priceControl.bottomAnchor.constraint(
            equalTo: contentView.bottomAnchor,
            constant: -Layout.padding
            ).isActive = true
    }
}

// MARK: - SliderControlOutput

extension WageCell: SliderControlOutput {

    func slider(_ slider: SliderControlOutput.Slider, didChangeMaximalValue value: Double) {
        maxLabel.text = output?.maxWageWillChange(to: value)
    }

    func slider(_ slider: SliderControlOutput.Slider, didChangeMinimalValue value: Double) {
        minLabel.text = output?.minWageWillChange(to: value)
    }

    func slider(_ slider: SliderControlOutput.Slider,
                didFinishUserInteractionWithSelectedMinimalValue minValue: Double,
                selectedMaximalValue maxValue: Double) {
        output?.wageDidChange(minValue: minValue, maxValue: maxValue)
    }

    func slider(_ slider: SliderControlOutput.Slider,
                didFinishUserInteractionWithMinEdgeThumbWithValue value: Double) {
        output?.wageDidChange(minValue: value)
    }

    func slider(_ slider: SliderControlOutput.Slider,
                didFinishUserInteractionWithMaxEdgeThumbWithValue value: Double) {
        output?.wageDidChange(maxValue: value)
    }
}
