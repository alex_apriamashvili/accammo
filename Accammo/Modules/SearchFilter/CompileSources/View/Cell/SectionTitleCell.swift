
import UI

class SectionTitleCell: UICollectionViewCell {

    private let titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    private func setUp() {
        configureViews()
        constraintViews()
    }
}

// MARK: - CellConfigurable Implementation

extension SectionTitleCell: CellConfigurable {

    func configure(with viewModel: Any, indexPath: IndexPath) {
        guard let vm = viewModel as? SectionTitleViewModel else { return }
        apply(vm.dependencies.styleProvider)
        titleLabel.text = vm.title
    }
}

// MARK: - Configuration

private extension SectionTitleCell {

    func configureViews() {
        configureTitleLabel()
    }

    func configureTitleLabel() {
        contentView.addSubview(titleLabel)
    }

    func apply(_ style: SearchFilterStyleProvider) {
        titleLabel.font = style.mediumBoldFont
        titleLabel.textColor = style.textColor
        titleLabel.textAlignment = .center
    }
}

// MARK: - Layout

private extension SectionTitleCell {

    struct Layout {
        static let offset: CGFloat = 8
        static let padding: CGFloat = 16
    }

    func constraintViews() {
        translatesAutoresizingMaskIntoConstraints = false
        constraintContentView()
        constraintTitle()
    }

    func constraintContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    func constraintTitle() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(
            equalTo: contentView.leadingAnchor,
            constant: Layout.padding
            ).isActive = true

        titleLabel.trailingAnchor.constraint(
            equalTo: contentView.trailingAnchor,
            constant: -Layout.padding
            ).isActive = true

        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Layout.padding).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Layout.offset).isActive = true
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
    }
}
