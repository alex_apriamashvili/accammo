
import UI

/// a protocol that reepresernts `CloudTagCell` output stream
/// in other words, the only way of how to tell any entity about events that've took place on the view
protocol CloudTagCellOutput: class {

    /// tells the receiver that the tag cloud view
    /// has recognized tap on one ofe it's tags at specified index
    ///
    /// - parameter tagCloud: a particulat tag-cloud view
    /// - parameter tag: the tag that has been tapped
    /// - parameter index: an index of a selected tag
    func tagCloud(_ tagCloud: CloudTagCell, didTapOnTag tag: CloudTagItem, at index: Int)

    /// notifies the receiver that
    /// a disclosure button has been tapped
    ///
    /// - parameter tagCloud: a particulat tag-cloud view
    func didTapDisclosureButton(_ tagCloud: CloudTagCell)
}

class CloudTagCell: UICollectionViewCell {

    private let cloudTagView = CloudTagView()
    private weak var output: CloudTagCellOutput?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    private func setUp() {
        configureViews()
        constraintViews()
    }
}

// MARK: - CellConfigurable Implementation

extension CloudTagCell: CellConfigurable {

    func configure(with viewModel: Any, indexPath: IndexPath) {
        guard let vm = viewModel as? CloudTagViewModel else { return }

        cloudTagView.update(vm.identifier)

        apply(vm.dependencies)
        apply(vm)
        cloudTagView.redraw()
    }
}

// MARK: - Configuration

private extension CloudTagCell {

    func configureViews() {
        configureCloudTagView()
    }

    func configureCloudTagView() {
        cloudTagView.output = self
        contentView.addSubview(cloudTagView)
    }

    func apply(_ dependencies: CloudTagViewModel.Dependencies) {
        let style = dependencies.styleProvider
        let cloudTagViewStyle = CloudTagViewStyle(
            font: style.mediumLightFont,
            textColor: style.textColor,
            backgroundColor: style.whiteColor,
            borderColor: style.lightGrayColor,
            selectedTextColor: style.whiteColor,
            selectedBackgroundColor: style.selectedElementColor,
            selectedBorderColor: style.selectedElementColor,
            disabledTextColor: style.whiteColor,
            disabledBackgroundColor: style.lightGrayColor,
            disabledBorderColor: style.lightGrayColor,
            moreFont: style.mediumLightFont,
            moreTextColor: style.textColor,
            moreBackgroundColor: style.whiteColor,
            moreBorderColor: .clear
        )

        cloudTagView.apply(cloudTagViewStyle)
    }

    func apply(_ data: CloudTagViewModel) {
        output = data.actionHandler
        var items = [CloudTagItem]()

        if data.ids.count == data.tags.count {
            for i in 0..<data.tags.count {
                let item = CloudTagItem(text: data.tags[i], itemId: data.ids[i])
                items.append(item)
            }
        }
        cloudTagView.setItems(items)
        cloudTagView.setSelectedIndexes(data.selectedIndexes)
    }
}

// MARK: - Layout

private extension CloudTagCell {

    struct Layout {
        static let offset: CGFloat = 8
        static let padding: CGFloat = 16
    }

    func constraintViews() {
        translatesAutoresizingMaskIntoConstraints = false
        constraintContentView()
        constraintCloudTagView()
    }

    func constraintContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    func constraintCloudTagView() {
        cloudTagView.translatesAutoresizingMaskIntoConstraints = false
        cloudTagView.leadingAnchor.constraint(
            equalTo: contentView.leadingAnchor,
            constant: Layout.padding
            ).isActive = true
        cloudTagView.trailingAnchor.constraint(
            equalTo: contentView.trailingAnchor,
            constant: -Layout.padding
            ).isActive = true
        cloudTagView.topAnchor.constraint(
            equalTo: contentView.topAnchor,
            constant: Layout.offset
            ).isActive = true
        cloudTagView.bottomAnchor.constraint(
            equalTo: contentView.bottomAnchor,
            constant: -Layout.offset
            ).isActive = true
    }
}

// MARK: - CloudTagViewOutput Implementation

extension CloudTagCell: CloudTagViewOutput {

    func tagCloud(_ tagCloud: CloudTagView, didSelectItemAt index: Int) {
        guard tagCloud == cloudTagView,
            let tag = tagCloud.item(at: index) else { return }
        output?.tagCloud(self, didTapOnTag: tag, at: index)
    }

    func didTapMoreButton(tagCloud: CloudTagView) {
        guard tagCloud == cloudTagView else { return }
        output?.didTapDisclosureButton(self)
    }
}

