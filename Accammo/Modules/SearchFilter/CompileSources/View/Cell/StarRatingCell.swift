
import UI

/// a way of interaction between cell and a handler
protocol StarRatingCellOutput: class {

    /// convenience typealias
    /// has the type of `[Int]` inside
    typealias Rating = StarRatingViewModel.Rating

    /// tells receiver to update the selected star rat5ing value for a particular cell
    ///
    /// - parameter starRatingCell: a paticular cell which data should be updated
    /// - parameter value: an upcoming values that should be applied
    /// - parameter newValue: an new value user selected
    func starRating(_ starRatingCell: StarRatingCell, didChangeValue value: Rating, newValue: Int)
}

class StarRatingCell: UICollectionViewCell {

    weak var output: StarRatingCellOutput?

    private var starContainer: FilterRatingComponent.Container
    private let numberOfElementsInContainer = 5

    override init(frame: CGRect) {
        let component = FilterRatingComponentImpl()
        starContainer = component.filterContainer(numberOfElementsInContainer)
        super.init(frame: frame)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        let component = FilterRatingComponentImpl()
        starContainer = component.filterContainer(numberOfElementsInContainer)
        super.init(coder: aDecoder)
        setUp()
    }

    private func setUp() {
        starContainer.output = self
        contentView.addSubview(starContainer)
        constraintViews()
    }
}

// MARK: - CellConfigurable Implementation

extension StarRatingCell: CellConfigurable {

    func configure(with viewModel: Any, indexPath: IndexPath) {
        guard let vm = viewModel as? StarRatingViewModel else { return }
        output = vm.actionHandler
        apply(vm.dependencies)
        starContainer.setRating(vm.rating)
    }
}

// MARK: - FilterRatingContainerComponentOutput Implementation

extension StarRatingCell: FilterRatingContainerComponentOutput {

    func didSelect(_ element: FilterRatingComponent.Element, at index: Int) {
        output?.starRating(self, didChangeValue: starContainer.value, newValue: element.value)
    }
}


// MARK: - Configuration

private extension StarRatingCell {

    func apply(_ dependencies: StarRatingViewModel.Dependencies) {
        let style = dependencies.styleProvider
        starContainer.setStyle(
            FilterRatingStyleProvider(icon: style.starImage,
                                      activeColor: style.selectedElementColor,
                                      activeContentColor: style.whiteColor,
                                      inactiveColor: style.whiteColor,
                                      inactiveContentColor: style.lightGrayColor,
                                      borderColor: style.lightGrayColor,
                                      inactiveTextColor: style.grayTextColor,
                                      titleFont: style.mediumBoldFont,
                                      radii: style.starButtonRadii)
        )
        starContainer.redraw()
    }
}

// MARK: - Layout

private extension StarRatingCell {

    struct Layout {
        static let offset: CGFloat = 8
        static let padding: CGFloat = 16
    }

    func constraintViews() {
        translatesAutoresizingMaskIntoConstraints = false
        constraintContentView()
        constraintStarsContainer()
    }

    func constraintContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    func constraintStarsContainer() {
        starContainer.translatesAutoresizingMaskIntoConstraints = false

        starContainer.leadingAnchor.constraint(
            equalTo: contentView.leadingAnchor,
            constant: Layout.padding
            ).isActive = true

        starContainer.trailingAnchor.constraint(
            equalTo: contentView.trailingAnchor,
            constant: -Layout.padding
            ).isActive = true

        starContainer.topAnchor.constraint(
            equalTo: contentView.topAnchor,
            constant: Layout.offset
            ).isActive = true

        starContainer.bottomAnchor.constraint(
            equalTo: contentView.bottomAnchor,
            constant: -Layout.offset
            ).isActive = true
    }
}
