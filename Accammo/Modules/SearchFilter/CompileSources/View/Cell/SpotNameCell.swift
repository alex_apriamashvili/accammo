
import UI

protocol SpotNameCellOutput: class {

    /// notifies the receiver that user has finished typing in the text field
    /// and committed a search result (hit 'Enter')
    ///
    /// - parameter name: a user-commited property name
    func didCommitSpotName(_ name: String?)

    /// asks the receiver whether or not editing is restricted.
    /// by the default this method returns `true`, which means that editing is allowed
    func shouldAllowEditing() -> Bool

    /// asks the receiver whether or not the substring is prohibitted for insertion.
    /// the default return value is `true`
    ///
    /// - parameter input: a substring that is pending
    /// - parameter currentString: a text that is currently in the text-field
    /// - parameter range: an insertion range
    func shouldAllow(input: String, into currentString: String, in range: NSRange) -> Bool

    /// notifies the receiver that user tap on the text field
    func didTapSpotName()

    func didEndEditing()
}

extension SpotNameCellOutput {

    func shouldAllowEditing() -> Bool {
        return true
    }

    func shouldAllow(input: String, into currentString: String, in range: NSRange) -> Bool {
        return true
    }
}

class SpotNameCell: UICollectionViewCell {

    private let textField = UITextField()
    private let separator = UIView()

    weak var output: SpotNameCellOutput?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    private func setUp() {
        configureViews()
        constraintViews()
    }
}

// MARK: - CellConfigurable Implementation

extension SpotNameCell: CellConfigurable {

    func configure(with viewModel: Any, indexPath: IndexPath) {
        guard let vm = viewModel as? SpotNameViewModel else { return }
        apply(vm.dependencies.styleProvider)
        textField.text = vm.text
        textField.attributedPlaceholder = NSAttributedString(
            string: vm.placeholder,
            attributes: [
              NSAttributedString.Key.foregroundColor: vm.dependencies.styleProvider.imageTintColor
            ]
        )
        output = vm.actionHandler
    }
}

// MARK: - UITextFieldDelegate Implementation

extension SpotNameCell: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard textField == self.textField else { return true }
        output?.didTapSpotName()
        return output?.shouldAllowEditing() ?? true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard textField == self.textField else { return true }
        return textField.resignFirstResponder()
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        guard textField == self.textField else { return true }
        let currentText = textField.text ?? ""
        return output?.shouldAllow(input: string, into: currentText, in: range) ?? true
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        output?.didCommitSpotName(textField.text)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        output?.didEndEditing()
    }
}

// MARK: - Configuration

private extension SpotNameCell {

    func configureViews() {
        contentView.addSubview(textField)
        textField.delegate = self

        let attribute = semanticContentAttribute
        let layoutDirection = UIView.userInterfaceLayoutDirection(for: attribute)
        textField.autocorrectionType = .no
        textField.textAlignment = layoutDirection == .rightToLeft ? .right : .left
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        contentView.addSubview(separator)
    }

    func apply(_ style: SearchFilterStyleProvider) {
        textField.textColor = style.textColor
        textField.font = style.mediumLightFont
        separator.backgroundColor = style.lightGrayColor
    }
}

// MARK: - Layout

private extension SpotNameCell {

    struct Layout {
        static let offset: CGFloat = 8
        static let padding: CGFloat = 16
        static let imageWidth: CGFloat = 20
        static let imageheight: CGFloat = 20
        static let separatorHeight: CGFloat = 1
    }

    func constraintViews() {
        translatesAutoresizingMaskIntoConstraints = false
        constraintContentView()
        constraintSeparator()
        constraintTextField()
    }

    func constraintContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    func constraintSeparator() {
        separator.translatesAutoresizingMaskIntoConstraints = false

        separator.leadingAnchor.constraint(
            equalTo: contentView.leadingAnchor,
            constant: Layout.padding
            ).isActive = true

        separator.trailingAnchor.constraint(
            equalTo: contentView.trailingAnchor,
            constant: -Layout.padding
            ).isActive = true

        separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
                                          constant: -Layout.padding).isActive = true

        separator.heightAnchor.constraint(equalToConstant: Layout.separatorHeight).isActive = true
    }

    func constraintTextField() {
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leadingAnchor.constraint(equalTo: separator.leadingAnchor).isActive = true
        textField.trailingAnchor.constraint(equalTo: separator.trailingAnchor).isActive = true
        textField.bottomAnchor.constraint(equalTo: separator.bottomAnchor, constant: -Layout.offset).isActive = true
        textField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Layout.padding).isActive = true
    }
}
