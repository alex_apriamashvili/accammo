
import Domain
import UI

public typealias SearchFilterModule = SearchFilterViewController

public protocol SearchFilterModuleInput {}

/// an interface which handles the module output events
/// like applying or resetting of filters
public protocol SearchFilterModuleOutput: class {

    /// apply current filter on the `SearchCriteria`
    ///
    /// - parameter filter: a filter data that has to be applied on the `SearchCriteria`
    func applyFilter(_ filter: SearchCriteria.Filter)

    /// tells the receiver that user wants to dismiss
    /// the module without applying any changes
    func dismissWithoutAnyChanges()
}

protocol SearchFilterInteractorInput {

    /// sets the entered spot name into an entity
    /// if spot name has to be erased – set this value to `nil`
    ///
    /// - parameter name: a name of the spot
    func set(spotName name: String?)

    /// sets / unsets the skill option at index
    ///
    /// - parameter skill: a skill that should be set / unset
    func set(skill: SearchCriteria.Skill)

    /// sets the wage range
    ///
    /// - parameter min: a new minimal selected value
    /// - parameter max: a new maximal selected value
    func set(wage min: Double, max: Double)

    /// sets the spot rating filter value
    ///
    /// - parameter rating: an array of ratings (stars) to filter
    func set(starRating rating: [Int])

    /// this function is being called to provide a state update
    /// the state update is a situation when data has not been changed,
    /// but presentation stil needs to be reconfigurated
    /// the interactor will call `update(_ data: ModuleEntity)` with the same `data`
    func updateState()

    /// ask an interactor to provide with a current range
    func fetchCurrentWage() -> SearchCriteria.Filter.Range

    /// ask an interactor to provide the latest version of filter
    func fetchLatestFilter() -> SearchCriteria.Filter
}

protocol SearchFilterInteractorOutput: class {

    /// tells receiver to perform updates according to the changed data set
    /// the changes would be diffed on a view data-source level
    ///
    /// - parameter data: an entity which describes the module data state
    func update(_ data: SearchCriteria.Filter)
}

protocol SearchFilterRouterInput {

    func dismiss()
}

protocol SearchFilterRouterOutput: class {}

/// a protocol which provides a `PRESENTER` -> `VIEW` interation
protocol SearchFilterViewInput: class {

    /// update view contents by applying `SectionViewModel`s
    ///
    /// - parameter contents: an array of SectionViewModel objects which in their turn describes each view-section
    func updateContents(_ contents: [SectionViewModel])

    /// hide keyboard
    func hideKeyboard()
}

/// a protocol which provides a `VIEW` -> `PRESENTER` interation
protocol SearchFilterViewOutput: class {

    /// the way of how to notify the receiver that the view is ready to be changed / presented
    func viewIsReady()

    /// tells the receiver that current filters has to be applied
    func applyFilter()

    /// tells the receiver that user wants to dismiss the view without any changes applied
    func handleDismiss()
}
