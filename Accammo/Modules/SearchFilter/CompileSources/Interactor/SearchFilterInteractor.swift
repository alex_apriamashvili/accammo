
import Foundation
import Domain

class SearchFilterInteractor {

    weak var output: SearchFilterInteractorOutput?

    private var filter: SearchCriteria.Filter

    init(filter: SearchCriteria.Filter) {
        self.filter = filter
    }
}

// MARK: - SearchFilterInteractorInput

extension SearchFilterInteractor: SearchFilterInteractorInput {

    func set(spotName name: String?) {
        filter.spotName = name ?? ""
    }

    func set(skill: SearchCriteria.Skill) {
      if let index = filter.skills.firstIndex(of: skill) {
            filter.skills.remove(at: index)
        } else {
            filter.skills.append(skill)
        }
        output?.update(filter)
    }

    func set(wage min: Double, max: Double) {
        filter.wage.selectedMin = min
        filter.wage.selectedMax = max
        output?.update(filter)
    }

    func set(starRating rating: [Int]) {
        filter.rating = rating
        output?.update(filter)
    }

    func updateState() {
        output?.update(filter)
    }

    func fetchCurrentWage() -> SearchCriteria.Filter.Range {
        return filter.wage
    }

    func fetchLatestFilter() -> SearchCriteria.Filter {
        return filter
    }
}
