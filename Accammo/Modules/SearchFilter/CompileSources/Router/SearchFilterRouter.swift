
import UI

class SearchFilterRouter {

    weak var view: UIViewController?
}

extension SearchFilterRouter: SearchFilterRouterInput {

    func dismiss() {
        view?.dismiss(animated: true, completion: nil)
    }
}
