
import UI

public typealias SearchFilterModuleDependencies = HasSearchFilterStyleProvider &
                                                  HasSearchFilterTranslationProvider &
                                                  HasCollectionViewAdapter

public protocol SearchFilterStyleProvider {

    var starImage: UIImage { get }

    // MARK: - Colors

    var backgroundColor: UIColor { get }
    var tintColor: UIColor { get }
    var blueColor: UIColor { get }
    var grayTextColor: UIColor { get }
    var imageTintColor: UIColor { get }
    var lightBlueColor: UIColor { get }
    var lightGrayColor: UIColor { get }
    var selectedElementColor: UIColor { get }
    var selectedTextColor: UIColor { get }
    var textColor: UIColor { get }
    var whiteColor: UIColor { get }

    // MARK: - Fonts

    var smallBoldFont: UIFont { get }
    var mediumLightFont: UIFont { get }
    var mediumBoldFont: UIFont { get }
    var bigBoldFont: UIFont { get }
    var veryBigLightFont: UIFont { get }

    // MARK: - Sizes

    var starButtonRadii: CGFloat { get }
    var cellWidth: CGFloat { get }
}

public protocol SearchFilterTranslationProvider {

    /// screen title
    var screenTitle: String { get }

    /// spot name section title
    var filterBySpotNameText: String { get }

    /// spot name section placeholder
    var spotNamePlaceholderText: String { get }

    /// star rating section
    var starRatingTitle: String { get }

    /// wage section
    var wageTitle: String { get }

    /// skills section
    var skillsTitle: String { get }

    /// a value that is represented on a filter button
    var filterText: String { get }
}

// MARK: - Has Protocols

public protocol HasSearchFilterStyleProvider {
    var styleProvider: SearchFilterStyleProvider { get }
}

public protocol HasSearchFilterTranslationProvider {
    var translationProvider: SearchFilterTranslationProvider { get }
}

public protocol HasCollectionViewAdapter {
    var collectionViewAdapter: CollectionViewAdapter { get }
}
