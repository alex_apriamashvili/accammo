
import Domain
import UI

typealias SearchFilterPresenterDependencies = HasSearchFilterTranslationProvider &
                                              HasSearchFilterStyleProvider

class SearchFilterPresenter {

    typealias Dependencies = SearchFilterPresenterDependencies
    private var dependencies: Dependencies

    weak var view: SearchFilterViewInput?
    var router: SearchFilterRouterInput?
    var interactor: SearchFilterInteractorInput?
    weak var moduleOutput: SearchFilterModuleOutput?

    private let sectionBuilder: SectionViewModelBuilder

    private var sectionViewModels: [SectionViewModel] = []

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        sectionBuilder = SectionViewModelBuilder(dependencies: dependencies)
    }
}

// MARK: - SearchFilterViewOutput

extension SearchFilterPresenter: SearchFilterViewOutput {

    func viewIsReady() {
        interactor?.updateState()
    }

    func applyFilter() {
        guard let filter = interactor?.fetchLatestFilter() else { return }
        moduleOutput?.applyFilter(filter)
    }

    func handleDismiss() {
        moduleOutput?.dismissWithoutAnyChanges()
    }
}

// MARK: - SearchFilterInteractorOutput

extension SearchFilterPresenter: SearchFilterInteractorOutput {

    func update(_ data: SearchCriteria.Filter) {
        sectionViewModels = sectionBuilder.buildSections(data, actionHandler: self)
        view?.updateContents(sectionViewModels)
    }
}

// MARK: - SearchFilterModuleInput

extension SearchFilterPresenter: SearchFilterModuleInput {}

// MARK: - Section Actions Handling

// MARK: - SpotNameCellOutput Implementation

extension SearchFilterPresenter: SpotNameCellOutput {

    func didTapSpotName() {
        interactor?.updateState()
    }
    func didCommitSpotName(_ name: String?) {
        interactor?.set(spotName: name)
    }

    func didEndEditing() {
        interactor?.updateState()
    }
}

// MARK: - CloudTagCellOutput Implementation

extension SearchFilterPresenter: CloudTagCellOutput {

    func tagCloud(_ tagCloud: CloudTagCell, didTapOnTag tag: CloudTagItem, at index: Int) {
        view?.hideKeyboard()
        interactor?.set(skill: SectionViewModelBuilder.skill(at: index))
    }

    func didTapDisclosureButton(_ tagCloud: CloudTagCell) {}
}

// MARK: - WageCellOutput Implementation

extension SearchFilterPresenter: WageCellOutput {

    func minWageWillChange(to minPercentage: Double) -> String {
        if let range = interactor?.fetchCurrentWage() {
            let selectedValue = SectionViewModelBuilder.selectedRangeValue(
                percentage: minPercentage,
                min: range.min,
                max: range.max
            )
            return SectionViewModelBuilder.minPriceText(min:selectedValue)
        }
        return ""
    }

    func maxWageWillChange(to maxPercentage: Double) -> String {
        if let range = interactor?.fetchCurrentWage() {
            let selectedValue = SectionViewModelBuilder.selectedRangeValue(
                percentage: maxPercentage,
                min: range.min,
                max: range.max
            )
            return SectionViewModelBuilder.maxPriceText(max: selectedValue)
        }
        return ""
    }

    func wageDidChange(minValue minPercentage: Double, maxValue maxPercentage: Double) {
        if let range = interactor?.fetchCurrentWage() {
            let minSelectedValue = SectionViewModelBuilder.selectedRangeValue(
                percentage: minPercentage,
                min: range.min,
                max: range.max
            )
            let maxSelectedValue = SectionViewModelBuilder.selectedRangeValue(
                percentage: maxPercentage,
                min: range.min,
                max: range.max
            )
            view?.hideKeyboard()
            interactor?.set(wage: minSelectedValue, max: maxSelectedValue)
        }
    }

    func wageDidChange(minValue minPercentage: Double) {}

    func wageDidChange(maxValue maxPercentage: Double) {}
}

// MARK: - StarRatingCellOutput Implementation

extension SearchFilterPresenter: StarRatingCellOutput {

    func starRating(_ starRatingCell: StarRatingCell, didChangeValue value: StarRatingCellOutput.Rating, newValue: Int) {
        view?.hideKeyboard()
        interactor?.set(starRating: value)
    }
}
