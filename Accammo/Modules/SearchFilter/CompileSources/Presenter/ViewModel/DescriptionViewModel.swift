import UI

struct DescriptionViewModel: CellViewModel {

    typealias Dependencies = HasSearchFilterStyleProvider

    let dependencies: Dependencies

    let identifier: String
    let title: String
}

extension DescriptionViewModel: Equatable {
    
    func isEqual(to viewModel: CellViewModel) -> Bool {
        guard let vm = viewModel as? DescriptionViewModel else { return false }
        return self == vm
    }

    static func ==(lhs: DescriptionViewModel, rhs: DescriptionViewModel) -> Bool {
        return lhs.identifier == rhs.identifier &&
               lhs.title == rhs.title

    }
}
