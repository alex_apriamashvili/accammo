
import UI

struct WageCellViewModel: CellViewModel {

    typealias Dependencies = HasSearchFilterStyleProvider

    let dependencies: Dependencies

    let identifier: String
    let minResult: String
    let maxResult: String

    let selectedMin: Double
    let selectedMax: Double

    let disabled: Bool

    weak var actionHandler: WageCellOutput?
}

extension WageCellViewModel: Equatable {

    public func isEqual(to viewModel: CellViewModel) -> Bool {
        guard let viewModel = viewModel as? WageCellViewModel else { return false }
        return self == viewModel
    }

    static func ==(lhs: WageCellViewModel, rhs: WageCellViewModel) -> Bool {
        return lhs.identifier == rhs.identifier &&
            lhs.selectedMin == rhs.selectedMin &&
            lhs.selectedMax == rhs.selectedMax &&
            lhs.minResult == rhs.minResult &&
            lhs.maxResult == rhs.maxResult
    }
}
