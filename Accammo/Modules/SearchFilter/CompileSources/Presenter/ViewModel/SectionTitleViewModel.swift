import UI

struct SectionTitleViewModel: CellViewModel {

    typealias Dependencies = HasSearchFilterStyleProvider

    let identifier: String
    let dependencies: Dependencies
    let title: String
}

extension SectionTitleViewModel: Equatable {

    func isEqual(to viewModel: CellViewModel) -> Bool {
        guard let vm = viewModel as? SectionTitleViewModel else { return false }
        return self == vm
    }

    static func ==(lhs: SectionTitleViewModel, rhs: SectionTitleViewModel) -> Bool {
        return lhs.identifier == rhs.identifier &&
            lhs.title == rhs.title
    }
}
