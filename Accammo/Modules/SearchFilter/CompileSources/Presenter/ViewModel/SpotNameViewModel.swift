import UI

struct SpotNameViewModel: CellViewModel {

    typealias Dependencies = HasSearchFilterStyleProvider

    let identifier: String
    let dependencies: Dependencies
    let text: String
    let placeholder: String

    weak var actionHandler: SpotNameCellOutput?
}

extension SpotNameViewModel: Equatable {

    func isEqual(to viewModel: CellViewModel) -> Bool {
        guard let vm = viewModel as? SpotNameViewModel else { return false }
        return self == vm
    }

    static func ==(lhs: SpotNameViewModel, rhs: SpotNameViewModel) -> Bool {
        return lhs.identifier == rhs.identifier &&
            lhs.text == rhs.text &&
            lhs.placeholder == rhs.placeholder
    }
}
