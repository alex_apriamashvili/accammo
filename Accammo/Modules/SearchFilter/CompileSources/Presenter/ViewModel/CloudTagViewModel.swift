import UI

struct CloudTagViewModel: CellViewModel {

    typealias Dependencies = HasSearchFilterStyleProvider

    let dependencies: Dependencies

    let identifier: String
    let tags: [String]
    let ids: [Int]
    let selectedIndexes: Set<Int>

    weak var actionHandler: CloudTagCellOutput?
}

extension CloudTagViewModel: Equatable {

    func isIdentical(to viewModel: Identifiable) -> Bool {
        guard let vm = viewModel as? CloudTagViewModel else { return false }
        return  identifier == vm.identifier
    }

    func isEqual(to viewModel: CellViewModel) -> Bool {
        guard let vm = viewModel as? CloudTagViewModel else { return false }
        return self == vm
    }

    static func ==(lhs: CloudTagViewModel, rhs: CloudTagViewModel) -> Bool {
        return  lhs.identifier == rhs.identifier &&
                lhs.tags == rhs.tags &&
                lhs.selectedIndexes == rhs.selectedIndexes
    }
}
