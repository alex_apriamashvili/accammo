import UI

struct StarRatingViewModel: CellViewModel {

    typealias Dependencies = HasSearchFilterStyleProvider
    typealias Rating = [Int]

    let identifier: String
    let dependencies: Dependencies
    let rating: Rating

    weak var actionHandler: StarRatingCellOutput?
}

extension StarRatingViewModel: Equatable {

    func isEqual(to viewModel: CellViewModel) -> Bool {
        guard let vm = viewModel as? StarRatingViewModel else { return false }
        return self == vm
    }

    static func ==(lhs: StarRatingViewModel, rhs: StarRatingViewModel) -> Bool {
        return lhs.identifier == rhs.identifier &&
            lhs.rating == rhs.rating
    }
}
