
import UI

public enum SearchFilterCellIdentifier: AccessibilityIdentifiable {

    case spotName
    case starRating
    case review
    case wage
    case skills

    public enum SpotName: AccessibilityIdentifiable {
        case header
        case title
        case textField
    }

    public enum StarRating: AccessibilityIdentifiable {
        case header
        case title
        case stars
    }

    public enum Review: AccessibilityIdentifiable {
        case title
        case runner
    }

    public enum Wage: AccessibilityIdentifiable {
        case title
        case runner
    }

    public enum Skills: AccessibilityIdentifiable {
        case title
        case tagCloud
    }
}
