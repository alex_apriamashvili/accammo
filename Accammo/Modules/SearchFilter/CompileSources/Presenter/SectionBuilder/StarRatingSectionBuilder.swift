
import UI

extension SectionViewModelBuilder {

    func buildStarRatingSection(data: [Int], handler: ActionHandler) -> SectionViewModel {
        let title = SectionTitleViewModel(
            identifier: SearchFilterCellIdentifier.StarRating.title.rawValue,
            dependencies: dependencies,
            title: dependencies.translationProvider.starRatingTitle
        )
        
        let starRatingViewModel = StarRatingViewModel(
            identifier: SearchFilterCellIdentifier.StarRating.stars.rawValue,
            dependencies: dependencies,
            rating: data,
            actionHandler: handler
        )
    
        return SectionViewModel(
            viewModels: [
                title,
                starRatingViewModel
            ],
            id: SearchFilterCellIdentifier.starRating.rawValue,
            contentWidth: dependencies.styleProvider.cellWidth
        )
    }
}
