
import Domain
import UI

extension SectionViewModelBuilder {

    func buildWageSection(range: SearchCriteria.Filter.Range, handler: ActionHandler) -> SectionViewModel {
        let title = SectionTitleViewModel(
            identifier: SearchFilterCellIdentifier.Wage.title.rawValue,
            dependencies: dependencies,
            title: dependencies.translationProvider.wageTitle
        )

        let minSelected = SectionViewModelBuilder.minPercentageValue(range)
        let maxSelected = SectionViewModelBuilder.maxPercentageValue(range)
        let minResult = SectionViewModelBuilder.minPriceText(min: range.selectedMin)
        let maxResult = SectionViewModelBuilder.maxPriceText(max: range.selectedMax)

        let priceViewModel = WageCellViewModel(
            dependencies: dependencies,
            identifier: SearchFilterCellIdentifier.Wage.runner.rawValue,
            minResult: minResult,
            maxResult: maxResult,
            selectedMin: minSelected,
            selectedMax: maxSelected,
            disabled: false,
            actionHandler: handler
        )

        return SectionViewModel(
            viewModels: [
                title,
                priceViewModel
            ],
            id: SearchFilterCellIdentifier.wage.rawValue,
            contentWidth: dependencies.styleProvider.cellWidth
        )
    }
}

extension SectionViewModelBuilder {

    static func minPriceText(min: Double) -> String {
        return String(format:"%.2f", min)
    }

    static func maxPriceText(max: Double) -> String {
        return String(format:"%.2f", max)
    }

    // MARK: - convert to value

    static func selectedRangeValue(percentage: Double, min: Double, max: Double) -> Double {
        let hundredPercentValue = hundredPercent(min: min, max: max)
        let offsetValue = hundredPercentValue * (percentage / Constant.maxPercentage)
        return min + offsetValue
    }
}

private extension SectionViewModelBuilder {

    // MARK: - convert to percentage

    enum Constant {
        static let minPercentage: Double = 0.0
        static let maxPercentage: Double = 100.0
        static let stepMultiplier: Double = 0.001
        static let extremelySmallValue = Double.leastNonzeroMagnitude
    }

    static func minPercentageValue(_ range: SearchCriteria.Filter.Range) -> Double {
        let hundredPercentValue = hundredPercent(min: range.min, max: range.max)
        let currentOffset = range.selectedMin - range.min
        return currentOffset * Constant.maxPercentage / hundredPercentValue
    }

    static func maxPercentageValue(_ range: SearchCriteria.Filter.Range) -> Double {
        let hundredPercentValue = hundredPercent(min: range.min, max: range.max)
        let currentOffset = range.selectedMax - range.min
        return currentOffset * Constant.maxPercentage / hundredPercentValue
    }

    static func hundredPercent(min: Double, max: Double) -> Double {
        let candidate = max - min
        if candidate > 0 { return candidate }

        return Constant.extremelySmallValue
    }
}
