
import UI
import Domain

final class SectionViewModelBuilder {

    typealias Dependencies =    HasSearchFilterTranslationProvider &
                                HasSearchFilterStyleProvider

    typealias ActionHandler =   SpotNameCellOutput &
                                CloudTagCellOutput &
                                WageCellOutput &
                                StarRatingCellOutput

    typealias SectionList = [SectionViewModel]

    let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func buildSections(_ data: SearchCriteria.Filter, actionHandler: ActionHandler) -> SectionList {
        return [
            buildSpotNameSection(name: data.spotName, actionHandler: actionHandler),
            buildStarRatingSection(data: data.rating, handler: actionHandler),
            buildWageSection(range: data.wage, handler: actionHandler),
            buildSkillsSection(data: data.skills, actionHandler: actionHandler)
        ]
    }
}
