
import Domain
import UI

extension SectionViewModelBuilder {

    typealias ViewState = CloudTagView.State

    func buildSkillsSection(data: [SearchCriteria.Skill], actionHandler: ActionHandler) -> SectionViewModel {
        let title = sectionTitle(
            title: dependencies.translationProvider.skillsTitle,
            id: SearchFilterCellIdentifier.Skills.title.rawValue
        )

        let cloudTagViewModel = tagCloud(
            id: SearchFilterCellIdentifier.Skills.tagCloud.rawValue,
            data: data,
            handler: actionHandler
        )

        return SectionViewModel(
            viewModels: [
                title,
                cloudTagViewModel
            ],
            id: SearchFilterCellIdentifier.skills.rawValue,
            contentWidth: dependencies.styleProvider.cellWidth
        )
    }
}

// MARK: - Private Helpers

private extension SectionViewModelBuilder {

    func sectionTitle(title: String, id: String) -> CellViewModel {
        return SectionTitleViewModel(
            identifier: id,
            dependencies: dependencies,
            title: title
        )
    }

    func tagCloud(id: String, data: [SearchCriteria.Skill], handler: ActionHandler) -> CellViewModel {
        return CloudTagViewModel(
            dependencies: dependencies,
            identifier: id,
            tags: allSkillNames(),
            ids: allIds(),
            selectedIndexes: selectedIndexes(data),
            actionHandler: handler
        )
    }

    func allSkillNames() -> [String] {
        return [
            SearchCriteria.Skill.singing.rawValue.capitalized,
            SearchCriteria.Skill.guitar.rawValue.capitalized,
            SearchCriteria.Skill.bass.rawValue.capitalized,
            SearchCriteria.Skill.drums.rawValue.capitalized,
            SearchCriteria.Skill.piano.rawValue.capitalized,
            SearchCriteria.Skill.sax.rawValue.capitalized
        ]
    }

    func allIds() -> [Int] {
        return [
            0,
            1,
            2,
            3,
            4,
            5
        ]
    }

    static func allSkills() -> [SearchCriteria.Skill] {
        return [
            .singing,
            .guitar,
            .bass,
            .drums,
            .piano,
            .sax
        ]
    }

    func selectedIndexes(_ skills: [SearchCriteria.Skill]) -> Set<Int> {
        let names = allSkillNames()
        let ids = allIds()
        var set: Set<Int> = []
        guard ids.count == names.count else { return set }
        for skill in skills {
          guard let index = names.firstIndex(of: skill.rawValue.capitalized) else { continue }
            set.insert(ids[index])
        }
        return set
    }
}

extension SectionViewModelBuilder {

    static func skill(at index: Int) -> SearchCriteria.Skill {
        return allSkills()[index]
    }
}
