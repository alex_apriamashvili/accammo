import UI

extension SectionViewModelBuilder {
    
    func buildSpotNameSection(name: String, actionHandler: ActionHandler) -> SectionViewModel {

        let propertyName = SpotNameViewModel(
            identifier: SearchFilterCellIdentifier.SpotName.textField.rawValue,
            dependencies: dependencies,
            text: name,
            placeholder: dependencies.translationProvider.spotNamePlaceholderText,
            actionHandler: actionHandler
        )

        let title = SectionTitleViewModel(
            identifier: SearchFilterCellIdentifier.SpotName.title.rawValue,
            dependencies: dependencies,
            title: dependencies.translationProvider.filterBySpotNameText
        )

        return SectionViewModel(
            viewModels: [
                title,
                propertyName
            ],
            id: SearchFilterCellIdentifier.spotName.rawValue,
            contentWidth: dependencies.styleProvider.cellWidth
        )
    }
}
