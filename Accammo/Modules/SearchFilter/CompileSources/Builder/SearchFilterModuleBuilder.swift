
import UI
import Domain

public class SearchFilterModuleBuilder {

    private let dependencies: SearchFilterModuleDependencies

    public init(dependencies: SearchFilterModuleDependencies) {
        self.dependencies = dependencies
    }

    public func build(with moduleOutput: SearchFilterModuleOutput, filter: SearchCriteria.Filter) -> UIViewController {
        let interactor = SearchFilterInteractor(filter: filter)
        let presenter = SearchFilterPresenter(dependencies: dependencies)
        let router = SearchFilterRouter()

        let view = SearchFilterViewController()
        view.hidesBottomBarWhenPushed = true

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        presenter.moduleOutput = moduleOutput

        interactor.output = presenter

        router.view = view

        view.presenter = presenter
        view.dependencies = dependencies
        view.collectionViewAdapter = dependencies.collectionViewAdapter

        return view
    }
}
