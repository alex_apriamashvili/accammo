
import UI
import Domain
import Service

final class ProfileModuleMapper {

    static func map(entity: Profile, style: ProfileModuleStyleProvider) -> ProfileViewModel {
        return ProfileViewModel(
            header: ProfileViewModel.Header(
                imageURL: entity.avatarImageURL,
                imageSizeRatio: mapImageRatio(entity: entity)
            ),
            info: mapInfo(entity: entity),
            style: style
        )
    }

    static func map(model: UserDetailResponse.UserInfo) -> Profile {
        return Profile(
            avatarImageURL: URL(string: model.imageURL)!,
            avatarWidth: model.imageW,
            avatarHeight: model.imageH,
            experienceMonths: model.expMonths,
            firstName: model.fName,
            lastName: model.lName,
            title: model.title,
            biography: model.bio
        )
    }
}

private extension ProfileModuleMapper {

    struct Titles {
        static let name = "Name"
        static let occupation = "Occupation"
        static let experience = "Experience"
        static let bio = "Biography"
    }

    struct Constatnts {
        static let monthsInYear = 12
        static let yearsString = "years"
        static let monthsString = "months"
    }

    static func mapImageRatio(entity: Profile) -> CGFloat {
        return CGFloat(entity.avatarHeight / entity.avatarWidth)
    }

    static func mapInfo(entity: Profile) -> [ProfileViewModel.Info] {
        return [
            mapName(entity: entity),
            mapTitle(entity: entity),
            mapExperience(entity: entity),
            mapBio(entity: entity)
        ]
    }

    static func mapName(entity: Profile) -> ProfileViewModel.Info {
        let value = "\(entity.firstName) \(entity.lastName)"
        return ProfileViewModel.Info(title: Titles.name, description: value)
    }

    static func mapTitle(entity: Profile) -> ProfileViewModel.Info {
        let value = entity.title
        return ProfileViewModel.Info(title: Titles.occupation, description: value)
    }

    static func mapExperience(entity: Profile) -> ProfileViewModel.Info {
        let years = entity.experienceMonths / Constatnts.monthsInYear
        let months = entity.experienceMonths % Constatnts.monthsInYear
        var value = "\(years) \(Constatnts.yearsString)"
        if months > 0 {
            value = "\(value), \(months) \(Constatnts.monthsString)"
        }
        return ProfileViewModel.Info(title: Titles.experience, description: value)
    }

    static func mapBio(entity: Profile) -> ProfileViewModel.Info {
        let value = entity.biography
        return ProfileViewModel.Info(title: Titles.bio, description: value)
    }
}
