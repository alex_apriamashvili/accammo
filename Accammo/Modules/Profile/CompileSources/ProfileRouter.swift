
import Foundation
import UI

final class ProfileRouter {

    weak var controller: UIViewController?

    init(baseViewController: UIViewController) {
        controller = baseViewController
    }
}

extension ProfileRouter: ProfileRouterInput {

    func dismiss() {
        controller?.dismiss(animated: true, completion: nil)
    }

    func present(_ viewController: UIViewController) {
        controller?.present(viewController, animated: true, completion: nil)
    }

    func push(_ viewController: UIViewController) {
        controller?.navigationController?.pushViewController(viewController, animated: true)
    }
}
