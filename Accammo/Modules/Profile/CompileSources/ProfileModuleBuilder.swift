
import UI
import Service

public final class ProfileModuleBuilder {

    public static func build(output: ProfileModuleOutput?,
                      dependencies: ProfileModuleDependency) -> ProfileModule {
        let presenter = ProfilePresenter(dependency: dependencies)
        let view = ProfileViewController(dependency: dependencies, output: presenter)
        let router = ProfileRouter(baseViewController: view)
        let service = UserProfileServiceImpl(client: APIClientImpl())
        let interactor = ProfileInteractor(service: service)

        presenter.output = output
        presenter.view = view

        presenter.router = router

        interactor.output = presenter
        presenter.interactor = interactor

        return (controller: view, input: presenter) 
    }
}
