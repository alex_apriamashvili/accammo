
import Foundation
import UI
import Domain

public typealias ProfileModule = (controller: ProfileViewController, input: ProfileModuleInput)

// MARK: - Dependencies

/// a typealias that describes all the dependencies which is reuired for a module
public typealias ProfileModuleDependency = HasProfileModuleStyleProvider & HasProfileModuleResourcesPovider & HasUserToken

/// a protocol that describes an entity
/// that brings styling attributes to a module
public protocol HasProfileModuleStyleProvider {

    /// a set of styling attruibutes
    var style: ProfileModuleStyleProvider { get }
}

/// a protocol that describes an entity
/// that brings resources into a module
public protocol HasProfileModuleResourcesPovider {

    /// a set of resources required by the module
    var resources: ProfileModuleResourcesPovider { get }
}

/// a protocol that specifies a dependency of this module on a user-token
public protocol HasUserToken {

    /// a read-only value of a user token
    var token: String { get }
}

/// a protocol that describes styles that are required by the module
public protocol ProfileModuleStyleProvider {

    /// a main background color
    var backgroundColor: UIColor { get }

    /// a main tint color
    var tintColor: UIColor { get }

    /// a tint color of a log out button
    var logoutTintColor: UIColor { get }

    // Info Cell
    var titleColor: UIColor { get }
    var titleFont: UIFont { get }
    var descriptionTextColor: UIColor { get }
    var descriptionFont: UIFont { get }
}

/// a protocol that describes resources that are required by the module
public protocol ProfileModuleResourcesPovider {

    /// a string that should be presented while user name is loading
    var moduleLoadingTitle: String { get }

    /// an image that should be shown as a logout icon
    var logoutIconImage: UIImage { get }
}


// MARK: - Module I/O Protocols

/// an interface that holds methods to communicate with the module
public protocol ProfileModuleInput {

    typealias InputDependency = HasProfileModuleStyleProvider & HasUserToken

    /// tells the receiver to show a login flow
    /// - parameter flow: a flow that needs to be presented
    func show(login flow: UIViewController)

    /// if any flow is presented at the moment above the current one,
    /// it will be dismissed by calling this function
    func dismissAnyPresentedFlows()
}

/// a protocol that contains methods to handle events
/// that may occur in the module
public protocol ProfileModuleOutput: class {

    /// notify a receiver that a logout action has been performed
    func handleLogout()
}

// MARK: Interactor

/// a protocol that describes a communication interface
/// for the module interactor
protocol ProfileInteractorInput {

    /// ask interactor to call a dedicated service
    /// to obtain information for a particular user
    /// - parameter token: a token to identify the user
    func fetchUserDetails(token: String)

    /// notify interactor that user has done a log-out action
    /// a dedicated service will be called to notify remote as well
    func doLogout(token: String)
}

protocol ProfileInteractorOutput: class {

    /// in case if user details were successfully obtained
    /// this method brings a profile info to represent it
    func present(profile: Profile)

    /// in case if an error occured during the user info fetching,
    /// this method will be called to provide information about an error
    func present(error: Error)
}

// MARK: - View

/// a ptorocol that defines actions that could been done to a view
protocol ProfileViewInput: class {

    /// tells the receiver to update a profile
    /// according to a provided view-model
    /// - parameter profile: a view-model that describes the data that should be represented on a screen
    func update(profile: ProfileViewModel)

    /// tells the reciver that an error has occured
    /// - parameter message: a localized error description that needs to be shown
    func showError(message: String)
}

/// a protocol that holds the events that might happen for the view
/// basically it's the only way of how view might tell about it's changes
protocol ProfileViewOutput {

    /// tells the reCeiver that the view is ready to be changed
    /// at this moment, all view components are loaded into memory
    func viewIsReady()

    /// tells the receiver that the logout button was tapped
    func handleTapOnLogoutButton()

    /// tells the receiver that
    /// a reload action has been performed and needs to be handeled
    func handleReloadAction()
}

// MARK: - Router

/// a protocol that describes routing actions
protocol ProfileRouterInput {

    /// tell the receiver to hide
    /// all the additional flows presented on this module
    func dismiss()

    /// this method should be called if next view-countroller
    /// **shouldn't** be put into a navigation stack
    func present(_ viewController: UIViewController)

    /// this method should be called if next view-countroller
    /// **needs** to be put into a navigation stack
    func push(_ viewController: UIViewController)
}

