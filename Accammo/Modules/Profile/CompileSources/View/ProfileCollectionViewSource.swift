
import UI

protocol ProfileCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int)
}

final class ProfileCollectionViewSource: NSObject {

    struct CellIdentifiers {

        static let header = "com.accammo.profile.header.cell.identifier"
        static let info = "com.accammo.profile.info.cell.identifier"
    }

    var viewModel: ProfileViewModel?

    private let actionHandler: ProfileCollectionViewSourceActionHandler
    private let prototypeCell: ProfileInfoCell = ProfileInfoCell()

    init(actionHandler: ProfileCollectionViewSourceActionHandler) {
        self.actionHandler = actionHandler
        super.init()
    }
}

extension ProfileCollectionViewSource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel?.info.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ProfileCollectionViewSource.CellIdentifiers.info,
            for: indexPath
        )

        guard let infoCell = cell as? ProfileInfoCell,
            let viewModel = viewModel,
            indexPath.item < viewModel.info.count else { fatalError("wrong cell was dequeued") }
        let vm = viewModel.info[indexPath.item]
        infoCell.configure(viewModel: vm, style: viewModel.style)

        return infoCell
    }
}

extension ProfileCollectionViewSource: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionHandler.handleCellTap(at: indexPath.item)
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: ProfileCollectionViewSource.CellIdentifiers.header,
            for: indexPath
        )

        guard let header = headerView as? ProfileHeaderView,
            let viewModel = viewModel else { return headerView }
        header.configure(viewModel: viewModel.header)

        return headerView
    }
}

extension ProfileCollectionViewSource: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let viewModel = viewModel,
            viewModel.info.count > indexPath.item  else { return .zero }

        let infoViewModel = viewModel.info[indexPath.item]
        let width = collectionView.bounds.width
        return cellSize(infoViewModel, style: viewModel.style, width: width)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = collectionView.bounds.width
        guard let headerViewModel = viewModel?.header else { return .zero }
        return headerSize(headerViewModel, width: width)
    }
}

// MARK: - Sizing

private extension ProfileCollectionViewSource {

    func cellSize(_ viewModel: ProfileViewModel.Info,
                  style: ProfileModuleStyleProvider,
                  width: CGFloat) -> CGSize {
        let widthContraint = prototypeCell.contentView.widthAnchor.constraint(equalToConstant: width)
        widthContraint.isActive = true

        prototypeCell.contentView.translatesAutoresizingMaskIntoConstraints = false
        prototypeCell.configure(viewModel: viewModel, style: style)

        prototypeCell.contentView.setNeedsUpdateConstraints()
        prototypeCell.contentView.updateConstraintsIfNeeded()
        prototypeCell.contentView.setNeedsLayout()
        prototypeCell.contentView.layoutIfNeeded()

        let size = prototypeCell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        widthContraint.isActive = false
        prototypeCell.removeConstraint(widthContraint)

        return size
    }

    func headerSize(_ viewModel: ProfileViewModel.Header, width: CGFloat) -> CGSize {
        let imageHeight = width * viewModel.imageSizeRatio
        return CGSize(
            width: width,
            height: min(imageHeight, ProfileHeaderView.maxAvailableHeight)
        )
    }
}
