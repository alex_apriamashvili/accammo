
import Foundation
import UI

public final class ProfileViewController: UIViewController {

    typealias Dependency = HasProfileModuleStyleProvider & HasProfileModuleResourcesPovider
    typealias Output = ProfileViewOutput & ProfileCollectionViewSourceActionHandler


    let output: Output

    private let dependency: Dependency
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private let source: ProfileCollectionViewSource
    private let refreshControl = UIRefreshControl()

    // MARK: - Initialization

    init(dependency: Dependency, output: Output) {
        self.dependency = dependency
        self.output = output
        source = ProfileCollectionViewSource(actionHandler: output)
        super.init(nibName: nil, bundle: nil)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        output.viewIsReady()
    }

    // MARK: - Actions

    @objc
    func pullToRefresh() {
        output.handleReloadAction()
    }

    @objc
    func tapLogout() {
        output.handleTapOnLogoutButton()
    }
}

// MARK: - SpotsViewInput Implementation

extension ProfileViewController: ProfileViewInput {

    func update(profile: ProfileViewModel) {
        refreshControl.endRefreshing()
        source.viewModel = profile
        collectionView.reloadData()
    }

    func showError(message: String) {}
}

// MARK: - Private

private extension ProfileViewController {

    func configureUI() {
        title = dependency.resources.moduleLoadingTitle
        view.backgroundColor = dependency.style.backgroundColor
        configureCollectionView()
        configureTabBar()
        configureRefreshControl()
    }

    func configureTabBar() {
        let barButton = UIBarButtonItem(
            image: dependency.resources.logoutIconImage,
            style: .plain,
            target: self,
            action: #selector(tapLogout)
        )
        barButton.tintColor = dependency.style.logoutTintColor
        navigationItem.setRightBarButtonItems([barButton], animated: true)
    }

    func configureRefreshControl() {
        collectionView.alwaysBounceVertical = true
        refreshControl.tintColor = dependency.style.tintColor
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
}

// MARK: - CollectionView

private extension ProfileViewController {

    func configureCollectionView() {
        collectionView.backgroundColor = dependency.style.backgroundColor

        view.addSubview(collectionView)

        registerHeader()
        registerInfoCell()

        collectionView.dataSource = source
        collectionView.delegate = source

        constrainCollectionView()
    }

    func registerHeader() {
        collectionView.register(
            ProfileHeaderView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: ProfileCollectionViewSource.CellIdentifiers.header
        )
    }

    func registerInfoCell() {
        collectionView.register(
            ProfileInfoCell.self,
            forCellWithReuseIdentifier: ProfileCollectionViewSource.CellIdentifiers.info
        )
    }

    func constrainCollectionView() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
