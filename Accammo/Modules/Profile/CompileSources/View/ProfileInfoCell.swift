
import UI

class ProfileInfoCell: UICollectionViewCell {

    private let titleLabel: UILabel = UILabel()
    private let textLabel: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func configure(viewModel: ProfileViewModel.Info, style: ProfileModuleStyleProvider) {
        titleLabel.text = viewModel.title
        textLabel.text = viewModel.description

        apply(style: style)

        setNeedsLayout()
        layoutIfNeeded()
    }
}

private extension ProfileInfoCell {

    func configureUI() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(textLabel)

        constrainViews()
    }

    func apply(style: ProfileModuleStyleProvider) {
        titleLabel.textColor = style.titleColor
        titleLabel.font = style.titleFont

        textLabel.textColor = style.descriptionTextColor
        textLabel.font = style.descriptionFont
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byWordWrapping
    }
}

private extension ProfileInfoCell {

    func constrainViews() {
        constrainTitle()
        constrainText()
    }

    func constrainTitle() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: textLabel.topAnchor).isActive = true
    }

    func constrainText() {
        textLabel.translatesAutoresizingMaskIntoConstraints = false

        textLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        textLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor).isActive = true
    }
}
