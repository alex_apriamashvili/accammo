
import UI
import SDWebImage

final class ProfileHeaderView: UICollectionReusableView {

    let image: UIImageView = UIImageView()
    var imageHeightConstraint: NSLayoutConstraint?
    static let maxAvailableHeight: CGFloat = 350

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }

    func configure(viewModel: ProfileViewModel.Header) {
        image.sd_setImage(with: viewModel.imageURL, completed: nil)
        imageHeightConstraint?.constant = bounds.size.width * viewModel.imageSizeRatio

        setNeedsLayout()
        layoutIfNeeded()
    }
}

private extension ProfileHeaderView {

    func configureUI() {
        self.clipsToBounds = true
        addSubview(image)
        constrainViews()
    }
}

private extension ProfileHeaderView {

    func constrainViews() {
        constrainImage()
    }

    func constrainImage() {
        image.translatesAutoresizingMaskIntoConstraints = false
        image.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        image.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        image.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageHeightConstraint = image.heightAnchor.constraint(equalToConstant: 1)
        imageHeightConstraint?.isActive = true
    }
}
