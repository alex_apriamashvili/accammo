
import Service

final class ProfileInteractor {

    weak var output: ProfileInteractorOutput?

    private let service: UserProfileService?

    init(service: UserProfileService) {
        self.service = service
    }
}

// MARK: - ProfileInteractorInput Implementation

extension ProfileInteractor: ProfileInteractorInput {

    func fetchUserDetails(token: String) {
        service?.fetchUserInfo(token: token, success: { [weak self] userInfo in
            guard let strongSelf = self else { return }
            let profile = ProfileModuleMapper.map(model: userInfo.info)
            strongSelf.output?.present(profile: profile)
        }, failure: { [weak self] error in
            guard let strongSelf = self else { return }
            strongSelf.output?.present(error: error)
        })
    }

    func doLogout(token: String) {
        service?.logout(token: token)
    }
}
