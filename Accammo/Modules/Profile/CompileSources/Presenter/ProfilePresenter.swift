
import Foundation
import Domain

// MARK: - Public

final class ProfilePresenter {

    typealias Dependency = HasProfileModuleStyleProvider & HasUserToken

    weak var output: ProfileModuleOutput?
    weak var view: ProfileViewInput?
    var interactor: ProfileInteractorInput?
    var router: ProfileRouterInput?

    private(set) var dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
}

// MARK: - Private

private extension ProfilePresenter {}

// MARK: - ProfileInteractorOutput Implementation

extension ProfilePresenter: ProfileInteractorOutput {
    
    func present(profile: Profile) {
        let viewModel = ProfileModuleMapper.map(entity: profile, style: dependency.style)
        view?.update(profile: viewModel)
    }

    func present(error: Error) {
        view?.showError(message: error.localizedDescription)
    }
    
}

// MARK: - ProfileViewOutput Implementation

extension ProfilePresenter: ProfileViewOutput {

    func viewIsReady() {
        interactor?.fetchUserDetails(token: dependency.token)
    }

    func handleTapOnLogoutButton() {
        interactor?.doLogout(token: dependency.token)
        output?.handleLogout()
    }

    func handleReloadAction() {
        interactor?.fetchUserDetails(token: dependency.token)
    }
}

// MARK: - ProfileModuleInput Implementation

extension ProfilePresenter: ProfileModuleInput {

    func show(login flow: UIViewController) {
        router?.present(flow)
    }

    func dismissAnyPresentedFlows() {
        router?.dismiss()
    }
}

// MARK: - ProfileCollectionViewSourceActionHandler Implementation

extension ProfilePresenter: ProfileCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int) {}
}

