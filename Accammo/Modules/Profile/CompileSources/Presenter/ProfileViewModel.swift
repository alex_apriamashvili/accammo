
import UI

struct ProfileViewModel {

    /// header description attributes
    let header: ProfileViewModel.Header

    /// an array of info pieces that describes user
    let info: [ProfileViewModel.Info]

    /// a style that must be applied on UI elements
    let style: ProfileModuleStyleProvider
}

extension ProfileViewModel {

    /// a structure that describes a header cell with an image
    struct Header {

        /// a URL to the image of a user
        let imageURL: URL

        ///  a ratio of an image
        /// to be able to calculate a resulting size
        let imageSizeRatio: CGFloat
    }
}

extension ProfileViewModel {

    /// a structure that describes an informational cell
    struct Info {

        /// a title of an info cell
        /// (ex. Name, Occupation, etc)
        var title: String

        /// a description value
        /// that should be shown for each particular title
        var description: String
    }
}
