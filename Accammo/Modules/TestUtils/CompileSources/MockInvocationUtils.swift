
import Foundation

public protocol MockInvocable {

    associatedtype InvocationType

    var invocations: [InvocationType] { get set }

    mutating func reset()
    func isCalled(_ invocation: InvocationType, times: Int) -> Bool
    func isCalledWithParameter(_ invocation: InvocationType, times: Int) -> Bool
    func isExactOrder(invocations: [InvocationType]) -> Bool
}

public extension MockInvocable where InvocationType: MockInvocationEnum {

    mutating func reset() {
        invocations.removeAll()
    }

    func isCalled(_ invocation: InvocationType, times: Int = 1) -> Bool {
        return invocations.filter({ $0 ~= invocation }).count == times
    }

    func isCalledWithParameter(_ invocation: InvocationType, times: Int = 1) -> Bool {
        return invocations.filter({ $0 == invocation }).count == times
    }

    func isExactOrder(invocations: [InvocationType]) -> Bool {
        return self.invocations == invocations
    }
}

public protocol MockInvocationEnum: RawRepresentable, Hashable {
}

public extension MockInvocationEnum {

    init?(rawValue: RawValue) { return nil }

    var rawValue: String {
        return "\(self)"
    }

    var rawValueWithoutParameter: String {
        guard let range = rawValue.range(of: "(") else { return String(rawValue) }
        let result = String(rawValue[..<range.lowerBound])
        return result
    }

    static func ~=(lhs: Self, rhs: Self) -> Bool {
        return lhs.rawValueWithoutParameter == rhs.rawValueWithoutParameter
    }

    var hashValue: Int {
        return rawValue.hashValue
    }
}

protocol MockInvocation: Equatable { }

protocol Mock {
    associatedtype InvocationType: Equatable
    var invocations: [InvocationType] { get }
}

func ==<T: MockInvocation>(l: T?, r: T) -> Bool {
    guard case let .some(l) = l else {
        return false
    }

    return l == r
}
