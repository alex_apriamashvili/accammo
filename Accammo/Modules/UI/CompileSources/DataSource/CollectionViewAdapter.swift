
public protocol CollectionViewAdapterDelegate: class {

    func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               didSelect viewModel: Any,
                               at indexPath: IndexPath)

    func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               scrollViewDidScroll: UIScrollView)

    func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               scrollViewDidEndScollingAnimation scrollView: UIScrollView)

    func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               scrollViewDidEndDecelerating: UIScrollView)
}

public extension CollectionViewAdapterDelegate {

    func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               scrollViewDidScroll scrollView: UIScrollView) {
        // empty default implementation
    }

    func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               scrollViewDidEndScollingAnimation scrollView: UIScrollView) {
        // empty default implementation
    }

    func collectionViewAdapter(_ adapter: CollectionViewAdapter,
                               scrollViewDidEndDecelerating scrollView: UIScrollView) {
        // empty default implementation
    }
}

public class CollectionViewAdapter: NSObject {

    public typealias Dependencies = HasUIComponentsDifferAdapter
    public var dependencies: Dependencies

    public weak var delegate: CollectionViewAdapterDelegate?

    internal var isFirstUpdate = true
    private var cellManager: CellManager = CellManager()
    private(set) var sections: [SectionViewModel] = []

    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    public func register(cell: CollectionCellType.Type,
                         viewModel: Any.Type,
                         on collectionView: UICollectionView) {
        cellManager.register(cell: cell, viewModel: viewModel, on: collectionView)
    }

    public func register(header: CollectionCellType.Type,
                         viewModel: Any.Type,
                         on collectionView: UICollectionView) {
        cellManager.register(header: header, viewModel: viewModel, on: collectionView)
    }

    public func register(footer: CollectionCellType.Type,
                         viewModel: Any.Type,
                         on collectionView: UICollectionView) {
        cellManager.register(footer: footer, viewModel: viewModel, on: collectionView)
    }

    public func resetData(with data: [SectionViewModel]) {
        sections = data
    }

    public func cellViewModel(for indexPath: IndexPath) -> CellViewModel? {
        guard sections.count > indexPath.section else { return nil }
        let section = sections[indexPath.section]
        guard section.count > indexPath.row else { return nil }
        return section[indexPath.row]
    }

    public func indexPath(for cellViewModel: CellViewModel) -> IndexPath? {
        var row: Int? = nil
      let section = sections.firstIndex { section in
        row = section.firstIndex { $0.identifier == cellViewModel.identifier }
            return row != nil
        }
        if let section = section, let row = row {
            return .init(row: row, section: section)
        }
        return nil
    }
}

// MARK: - UICollectionViewDataSource Configuring

extension CollectionViewAdapter: UICollectionViewDataSource {

    // MARK: Metrics

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }

    public func collectionView(_ collectionView: UICollectionView,
                               numberOfItemsInSection section: Int) -> Int {
        let rows = sections[section]
        return rows.count
    }

    // MARK: Views

    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = sections[indexPath.section][indexPath.row]

        let cellIdentifier = cellManager.identifier(for: type(of: viewModel))

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                      for: indexPath)

        if let cellConfigurable = cell as? CellConfigurable {
            cellConfigurable.configure(with: viewModel, indexPath: indexPath)
        }
        return cell
    }

    public func collectionView(_ collectionView: UICollectionView,
                               viewForSupplementaryElementOfKind kind: String,
                               at indexPath: IndexPath) -> UICollectionReusableView {

        var viewModel: Any? = nil
      if kind == UICollectionView.elementKindSectionHeader {
            viewModel = sections[indexPath.section].sectionHeaderViewModel
      } else if kind == UICollectionView.elementKindSectionFooter {
            viewModel = sections[indexPath.section].sectionFooterViewModel
        }

        if let viewModel = viewModel {
            let cellIdentifier = cellManager.identifier(for: type(of: viewModel))
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                       withReuseIdentifier: cellIdentifier,
                                                                       for: indexPath)

            if let view = view as? CellConfigurable {
                view.configure(with: viewModel, indexPath: indexPath)
            }
            return view
        }
        return UICollectionReusableView()
    }
}

// MARK: - UICollectionViewDelegate

extension CollectionViewAdapter: UICollectionViewDelegate {

    // MARK: Selected Cells

    public func collectionView(_ collectionView: UICollectionView,
                               didSelectItemAt indexPath: IndexPath) {
        let viewModel = sections[indexPath.section][indexPath.row]
        delegate?.collectionViewAdapter(self,
                                        didSelect: viewModel,
                                        at: indexPath)
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.collectionViewAdapter(self, scrollViewDidScroll: scrollView)
    }

    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.collectionViewAdapter(self, scrollViewDidEndDecelerating: scrollView)
    }

    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        delegate?.collectionViewAdapter(self, scrollViewDidEndScollingAnimation: scrollView)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension CollectionViewAdapter: UICollectionViewDelegateFlowLayout {

    // MARK: Sizes

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let section = sections[indexPath.section]
        let viewModel = section[indexPath.row]
        return cellManager.cellSize(for: viewModel, width: section.contentWidth, height: nil)
    }

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               referenceSizeForHeaderInSection section: Int) -> CGSize {
        let section = sections[section]
        if let viewModel = section.sectionHeaderViewModel {
            return cellManager.cellSize(for: viewModel, width: section.contentWidth, height: nil)
        }
        return .zero
    }

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               referenceSizeForFooterInSection section: Int) -> CGSize {
        let section = sections[section]
        if let viewModel = section.sectionFooterViewModel {
            return cellManager.cellSize(for: viewModel, width: section.contentWidth, height: nil)
        }
        return .zero
    }

    // MARK: Spacing

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAt section: Int) -> UIEdgeInsets {
        return sections[section].inset
    }

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sections[section].minimumLineSpacing
    }

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sections[section].minimumInteritemSpacing
    }
}

public protocol HasUIComponentsDifferAdapter {
    var differAdapter: UIComponentsDifferAdapter { get }
}

public protocol UIComponentsDifferAdapter {

    typealias SectionEquality = (SectionViewModel, SectionViewModel) -> Bool
    typealias ElementEquality = (CellViewModel, CellViewModel) -> Bool

    func diff(previous: [SectionViewModel],
              update: [SectionViewModel],
              isEqualSection: @escaping SectionEquality,
              isEqualElement: @escaping ElementEquality) -> [UIComponentsDifferElement]
}
