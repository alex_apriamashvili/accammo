
public typealias CollectionCellType = UICollectionViewCell & CellConfigurable

class CellManager {

    private var register: [String: CollectionCellSizeCalculator] = [:]

    public func register(cell: CollectionCellType.Type,
                         viewModel: Any.Type,
                         on collectionView: UICollectionView) {
        let cellIdentifier = identifier(for: viewModel)
        collectionView.register(cell.self, forCellWithReuseIdentifier: cellIdentifier)
        register[cellIdentifier] = CollectionCellSizeCalculator(cell: cell.init())
    }

    public func register(header: CollectionCellType.Type,
                         viewModel: Any.Type,
                         on collectionView: UICollectionView) {
        let cellIdentifier = identifier(for: viewModel)
        collectionView.register(header.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: cellIdentifier)
        register[cellIdentifier] = CollectionCellSizeCalculator(cell: header.init())
    }

    public func register(footer: CollectionCellType.Type,
                         viewModel: Any.Type,
                         on collectionView: UICollectionView) {
        let cellIdentifier = identifier(for: viewModel)
        collectionView.register(footer.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                withReuseIdentifier: cellIdentifier)
        register[cellIdentifier] = CollectionCellSizeCalculator(cell: footer.init())
    }

    public func cellSize(for viewModel: CellSizeHashable, width: CGFloat?, height: CGFloat?) -> CGSize {
        let calculator = register[identifier(for: type(of: viewModel))]
        return calculator?.size(for: viewModel, fixedWidth: width, fixedHeight: height) ?? .zero
    }

    public func identifier(for viewModel: Any.Type) -> String {
        let identifier = String(describing: type(of: viewModel.self))
        return identifier
    }
}
