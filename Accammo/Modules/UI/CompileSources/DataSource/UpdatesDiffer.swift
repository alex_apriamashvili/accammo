
public enum UIComponentsDifferElement {
    case deleteSection(Int)
    case insertSection(Int)
    case moveSection(from: Int, to: Int)
    case deleteElement(Int, section: Int)
    case insertElement(Int, section: Int)
    case moveElement(from: (item: Int, section: Int), to: (item: Int, section: Int))
}

public extension CollectionViewAdapter {

    typealias Updates = (diff: [UIComponentsDifferElement], reloadPaths: [IndexPath])

    func updates(_ updates: [SectionViewModel]) -> Updates {
        let diff = dependencies.differAdapter.diff(
            previous: sections,
            update: updates,
            isEqualSection: equateSection(lhs:rhs:),
            isEqualElement: equateItem(lhs:rhs:)
        )

        return (
            diff: diff,
            reloadPaths: reloadIndexPath(from: updates)
        )
    }

    /// Apply the new view models on the collection with animations
    ///
    /// - Parameters:
    ///   - sectionViewModels: Tne new section view model to set on the data source for the collection view
    ///   - collectionView: The collection view to perfrom the changes with an animation
    func apply(_ sectionViewModels: [SectionViewModel], on collectionView: UICollectionView) {
        guard !isFirstUpdate else {
            isFirstUpdate = false
            resetData(with: sectionViewModels)
            collectionView.reloadData()
            return
        }

        let itemsToUpdate = updates(sectionViewModels)

        collectionView.performBatchUpdates({
            resetData(with: sectionViewModels)
            let extract = extractChanges(from: itemsToUpdate.diff)
            guard hasAnyChanges(extract) else { return }
            collectionView.insertSections(extract.sectionInsertions)
            collectionView.deleteSections(extract.sectionDeletions)
            extract.sectionMoves.forEach { collectionView.moveSection($0.from, toSection: $0.to) }
            collectionView.deleteItems(at: extract.itemDeletions)
            collectionView.insertItems(at: extract.itemInsertions)
            extract.itemMoves.forEach { collectionView.moveItem(at: $0.from, to: $0.to) }
        }, completion: { _ in
            CATransaction.begin()
            CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
            collectionView.reloadItems(at: itemsToUpdate.reloadPaths)
            CATransaction.commit()
        })
    }
}

// MARK: - Differ Comparison

private extension CollectionViewAdapter {

    func equateSection(lhs: SectionViewModel, rhs: SectionViewModel) -> Bool {
        return lhs.isIdentical(to: rhs)
    }

    func equateItem(lhs: CellViewModel, rhs: CellViewModel) -> Bool {
        return lhs.isIdentical(to: rhs)
    }
}

// MARK: Index Paths To Reload

private extension CollectionViewAdapter {

    typealias Changes = (
        sectionDeletions: IndexSet,
        sectionInsertions: IndexSet,
        sectionMoves: [(from: Int, to: Int)],
        itemDeletions: [IndexPath],
        itemInsertions: [IndexPath],
        itemMoves: [(from: IndexPath, to: IndexPath)]
    )
    func extractChanges(from diff: [UIComponentsDifferElement]) -> Changes {
        var itemDeletions: [IndexPath] = []
        var itemInsertions: [IndexPath] = []
        var itemMoves: [(IndexPath, IndexPath)] = []
        var sectionDeletions: IndexSet = []
        var sectionInsertions: IndexSet = []
        var sectionMoves: [(from: Int, to: Int)] = []

        diff.forEach { element in
            switch element {
            case let .deleteElement(at, section):
                itemDeletions.append(IndexPath(item: at, section: section))
            case let .insertElement(at, section):
                itemInsertions.append(IndexPath(item: at, section: section))
            case let .moveElement(from, to):
                itemMoves.append((IndexPath(item: from.item, section: from.section),
                                  IndexPath(item: to.item, section: to.section)))
            case let .deleteSection(at):
                sectionDeletions.insert(at)
            case let .insertSection(at):
                sectionInsertions.insert(at)
            case let .moveSection(move):
                sectionMoves.append((move.from, move.to))
            }
        }
        return (sectionDeletions, sectionInsertions, sectionMoves, itemDeletions, itemInsertions, itemMoves)
    }

    func hasAnyChanges(_ changes: Changes) -> Bool {
        let hasChanges: Bool = changes.itemDeletions.count > 0 ||
            changes.itemInsertions.count > 0 ||
            changes.sectionMoves.count > 0 ||
            changes.itemMoves.count > 0 ||
            changes.sectionDeletions.count > 0 ||
            changes.sectionInsertions.count > 0
        return hasChanges
    }

    /// a function that determines index paths of the elements which should be reloaded
    /// after the diffing algorithm applied
    func reloadIndexPath(from viewModels: [SectionViewModel]) -> [IndexPath] {

        var indexPaths: [IndexPath] = []
        for (sectionIndex, newSection) in viewModels.enumerated() {
            guard let previousSection = sections.first(
                where: { $0.identifier == newSection.identifier }
                ) else {
                continue
            }
            for (rowIndex, newRow) in newSection.cellViewModels.enumerated() {
                guard let previousRow = previousSection.cellViewModels.first(
                    where: { $0.identifier == newRow.identifier }
                    ) else {
                    continue
                }
                if !newRow.isEqual(to: previousRow) {
                    indexPaths.append(.init(row: rowIndex, section: sectionIndex))
                }
            }
        }

        return indexPaths
    }
}
