

public protocol EquatableViewModel {

    func isIdentical(to viewModel: Self) -> Bool
    func isEqual(to viewModel: Self) -> Bool
}

public protocol CellSizeHashable {
    // This is to cache cell sizes for view models, e.g. if cell size depends only on
    // title of cell then it should return title.hashValue, it means that
    // size is not being recalculated each time if titles is equal.
    // If sizeHash is nil then size recalcs for each view model.
    var sizeHash: Int? { get }
}

public extension CellSizeHashable {

    var sizeHash: Int? { return nil }
}

public protocol Identifiable {

    var identifier: String { get }
    func isIdentical(to viewModel: Identifiable) -> Bool
}

extension Identifiable {

    public func isIdentical(to viewModel: Identifiable) -> Bool {
        return self.identifier == viewModel.identifier
    }
}

public protocol HeaderViewModel: Identifiable, CellSizeHashable {

    func isEqual(to viewModel: HeaderViewModel) -> Bool
}

extension HeaderViewModel {

    public func isEqual(to viewModel: HeaderViewModel) -> Bool {
        return self.identifier == viewModel.identifier
    }
}

public protocol FooterViewModel: Identifiable, CellSizeHashable {

    func isEqual(to viewModel: FooterViewModel) -> Bool
}

extension FooterViewModel {

    public func isEqual(to viewModel: FooterViewModel) -> Bool {
        return self.identifier == viewModel.identifier
    }
}

public protocol CellViewModel: Identifiable, CellSizeHashable {

    func isEqual(to viewModel: CellViewModel) -> Bool
}

extension CellViewModel {

    public func isEqual(to viewModel: CellViewModel) -> Bool {
        return self.identifier == viewModel.identifier
    }
}

public struct SectionViewModel: Identifiable {

    public let identifier: String
    public let cellViewModels: [CellViewModel]
    public let sectionHeaderViewModel: HeaderViewModel?
    public let sectionFooterViewModel: FooterViewModel?
    public let inset: UIEdgeInsets
    public let minimumLineSpacing: CGFloat
    public let minimumInteritemSpacing: CGFloat
    public var contentWidth: CGFloat

    public struct Default {
        static let inset = UIEdgeInsets.zero
        static let zero: CGFloat = 0
    }

    public init(viewModels: [CellViewModel],
                header: HeaderViewModel? = nil,
                footer: FooterViewModel? = nil,
                id: String = "",
                inset: UIEdgeInsets = UIEdgeInsets.zero,
                minimumLineSpacing: CGFloat = 0,
                minimumInteritemSpacing: CGFloat = 0,
                contentWidth: CGFloat = UIScreen.main.bounds.width) {
        cellViewModels = viewModels
        sectionHeaderViewModel = header
        sectionFooterViewModel = footer
        identifier = id
        self.inset = inset
        self.minimumLineSpacing = minimumLineSpacing
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.contentWidth = contentWidth
    }
}

extension SectionViewModel: EquatableViewModel {

    public func isIdentical(to viewModel: SectionViewModel) -> Bool {
        return  identifier == viewModel.identifier &&
            identicalHeaders(lhs: sectionHeaderViewModel, rhs: viewModel.sectionHeaderViewModel) &&
            identicalFooters(lhs: sectionFooterViewModel, rhs: viewModel.sectionFooterViewModel)
    }

    private func identicalHeaders(lhs: HeaderViewModel?, rhs: HeaderViewModel?) -> Bool {
        if let lhs = lhs, let rhs = rhs {
            return lhs.isIdentical(to: rhs)
        }
        return lhs == nil && rhs == nil
    }

    private func identicalFooters(lhs: FooterViewModel?, rhs: FooterViewModel?) -> Bool {
        if let lhs = lhs, let rhs = rhs {
            return lhs.isIdentical(to: rhs)
        }
        return lhs == nil && rhs == nil
    }

    public func isEqual(to viewModel: SectionViewModel) -> Bool {
        return  self == viewModel
    }

    private func equateHeaders(lhs: HeaderViewModel?, rhs: HeaderViewModel?) -> Bool {
        if let lhs = lhs, let rhs = rhs {
            return lhs.isEqual(to: rhs)
        }
        return lhs == nil && rhs == nil
    }

    private func equateFooters(lhs: FooterViewModel?, rhs: FooterViewModel?) -> Bool {
        if let lhs = lhs, let rhs = rhs {
            return lhs.isEqual(to: rhs)
        }
        return lhs == nil && rhs == nil
    }
}

extension SectionViewModel: Equatable {

    public static func == (lhs: SectionViewModel, rhs: SectionViewModel) -> Bool {
        return lhs.identifier == rhs.identifier &&
            lhs.inset == rhs.inset &&
            lhs.minimumLineSpacing == rhs.minimumLineSpacing &&
            lhs.minimumInteritemSpacing == rhs.minimumInteritemSpacing &&
            lhs.contentWidth == rhs.contentWidth &&
            lhs.equateHeaders(lhs: lhs.sectionHeaderViewModel, rhs: rhs.sectionHeaderViewModel) &&
            lhs.equateFooters(lhs: lhs.sectionFooterViewModel, rhs: rhs.sectionFooterViewModel)
        // cellViewModels are missing because it will be check later through the reload of rows.
        // if added here it will reload the whole section instead of a single row.
    }
}

extension SectionViewModel: Collection {

    public typealias Index = Int
    public typealias Element = CellViewModel

    public var startIndex: Index { return cellViewModels.startIndex }
    public var endIndex: Index { return cellViewModels.endIndex }

    public subscript(index: Index) -> Element {
        get { return cellViewModels[index] }
    }

    public func index(after i: Index) -> Index {
        return cellViewModels.index(after: i)
    }

    public var count: Int {
        return cellViewModels.count
    }
}
