

import Foundation

public protocol CellConfigurable {
    func configure(with viewModel: Any, indexPath: IndexPath)
}
