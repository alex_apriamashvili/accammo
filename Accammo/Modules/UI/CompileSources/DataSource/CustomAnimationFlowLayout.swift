
open class CustomAnimationFlowLayout: UICollectionViewFlowLayout {
    public typealias Animation = (UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes

    public var insertingInitAninmation: Animation = CustomAnimationFlowLayout.insertInitAnimation
    public var deletingInitAninmation: Animation = CustomAnimationFlowLayout.deleteInitAnimation
    public var reloadingInitAninmation: Animation = CustomAnimationFlowLayout.reloadInitAnimation

    public var insertingFinalAninmation: Animation = CustomAnimationFlowLayout.insertFinalAnimation
    public var deletingFinalAninmation: Animation = CustomAnimationFlowLayout.deleteFinalAnimation
    public var reloadingFinalAninmation: Animation = CustomAnimationFlowLayout.reloadFinalAnimation

    private var insertingIndexPaths = [IndexPath]()
    private var deletingIndexPaths = [IndexPath]()
    private var reloadingIndexPaths = [IndexPath]()

    open override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)

        insertingIndexPaths.removeAll()
        deletingIndexPaths.removeAll()
        reloadingIndexPaths.removeAll()

        for update in updateItems {
            if let indexPath = update.indexPathAfterUpdate {
                switch update.updateAction {
                case .insert:
                    insertingIndexPaths.append(indexPath)
                case .delete:
                    deletingIndexPaths.append(indexPath)
                case .reload:
                    reloadingIndexPaths.append(indexPath)
                case .move:
                    if let previousIndexPath = update.indexPathBeforeUpdate {
                        deletingIndexPaths.append(previousIndexPath)
                    }
                    insertingIndexPaths.append(indexPath)
                default:
                  break
}
            }
        }
    }

    open override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath)
        -> UICollectionViewLayoutAttributes? {
        guard let attributes = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath) else {
            return nil
        }

        if insertingIndexPaths.contains(itemIndexPath) {
            return insertingInitAninmation(attributes)
        }
        if deletingIndexPaths.contains(itemIndexPath) {
            return deletingInitAninmation(attributes)
        }
        if reloadingIndexPaths.contains(itemIndexPath) {
            return reloadingInitAninmation(attributes)
        }

        return attributes
    }

    open override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath)
        -> UICollectionViewLayoutAttributes? {
            guard let attributes = super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath) else {
                return nil
            }

            if insertingIndexPaths.contains(itemIndexPath) {
                return insertingFinalAninmation(attributes)
            }
            if deletingIndexPaths.contains(itemIndexPath) {
                return deletingFinalAninmation(attributes)
            }
            if reloadingIndexPaths.contains(itemIndexPath) {
                return reloadingFinalAninmation(attributes)
            }

            return attributes
    }

    open override func finalizeCollectionViewUpdates() {
        super.finalizeCollectionViewUpdates()

        insertingIndexPaths.removeAll()
        deletingIndexPaths.removeAll()
        reloadingIndexPaths.removeAll()
    }

    // MARK: - default anminations

    private static func insertInitAnimation(attributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
        return attributes
    }

    private static func deleteInitAnimation(attributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
        return attributes
    }

    private static func reloadInitAnimation(attributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
            attributes.alpha = 1.0
        return attributes
    }

    private static func insertFinalAnimation(attributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
            return attributes
    }

    private static func deleteFinalAnimation(attributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
            return attributes
    }

    private static func reloadFinalAnimation(attributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
            attributes.alpha = 1.0
            return attributes
    }
}
