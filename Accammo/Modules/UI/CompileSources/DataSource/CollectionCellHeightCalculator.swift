
public class CollectionCellSizeCalculator {

    public let cell: CollectionCellType

    public init(cell: CollectionCellType) {
        self.cell = cell
    }

    private var sizeCache: [Int: CGSize] = [:]

    public func size(for viewModel: CellSizeHashable, fixedWidth: CGFloat?, fixedHeight: CGFloat?) -> CGSize {

        guard let key = viewModel.sizeHash else {
            return calculateConstrainedSize(for: viewModel, width: fixedWidth, height: fixedHeight)
        }

        if let size = sizeCache[key] {
            return size
        }

        let size = calculateConstrainedSize(for: viewModel, width: fixedWidth, height: fixedHeight)
        sizeCache[key] = size
        return size
    }

    private func calculateConstrainedSize(for viewModel: CellSizeHashable,
                                          width: CGFloat?,
                                          height: CGFloat?) -> CGSize {

        let widthContraint = width.map(cell.contentView.widthAnchor.constraint(equalToConstant:))
        let heightContraint = height.map(cell.contentView.heightAnchor.constraint(equalToConstant:))
        widthContraint?.isActive = true
        heightContraint?.isActive = true

        cell.configure(with: viewModel, indexPath: .init(row: 0, section: 0))

        cell.contentView.setNeedsUpdateConstraints()
        cell.contentView.updateConstraintsIfNeeded()
        cell.contentView.setNeedsLayout()
        cell.contentView.layoutIfNeeded()
      let size = cell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        widthContraint.map(cell.contentView.removeConstraint)
        heightContraint.map(cell.contentView.removeConstraint)

        return size
    }
}
