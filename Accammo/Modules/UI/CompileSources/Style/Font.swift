
public extension Style {

    struct Font {
        
        public static let smallTitle = UIFont.systemFont(ofSize: 10, weight: .semibold)
        public static let mediumSubtitle = UIFont.systemFont(ofSize: 12, weight: .semibold)
        public static let mediumTitle = UIFont.systemFont(ofSize: 14, weight: .semibold)
        public static let mediumText = UIFont.systemFont(ofSize: 12, weight: .thin)
        public static let emphasisedLargeText = UIFont.systemFont(ofSize: 14, weight: .bold)
        public static let regularText = UIFont.systemFont(ofSize: 14, weight: .regular)
        public static let hugeInput = UIFont.systemFont(ofSize: 21, weight: .medium)
        public static let hugeTitle = UIFont.systemFont(ofSize: 21, weight: .semibold)
    }
}
