
public extension Style {

    struct Color {

        public static let green = Style.Color.rgb(26, 204, 101)
        public static let black = Style.Color.rgb(65, 65, 65)
        public static let darkGray = Style.Color.rgb(95, 95, 95)
        public static let gray = Style.Color.rgb(106, 106, 106)
        public static let red = Style.Color.rgb(226, 65, 65)
        public static let white = Style.Color.rgb(255, 255, 255)
    }
}

private extension Style.Color {

    struct Spectrum {
        static let full: CGFloat = 255.0
    }

    static func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
        return UIColor(
            red: red / Spectrum.full,
            green: green / Spectrum.full,
            blue: blue / Spectrum.full,
            alpha: 1)
    }

    static func rgba(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {
        return UIColor(
            red: red / Spectrum.full,
            green: green / Spectrum.full,
            blue: blue / Spectrum.full,
            alpha: alpha)
    }
}
