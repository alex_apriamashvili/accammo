
import Foundation

public struct Style {

    public let color: Style.Color = Style.Color()
    public let font: Style.Font = Style.Font()
}
