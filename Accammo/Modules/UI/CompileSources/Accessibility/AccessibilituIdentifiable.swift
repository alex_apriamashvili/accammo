import Foundation

// use this protocol for all AccessibilityIdentifier enums
// the raw value contains the module and all parent structures in the identifier.
public protocol AccessibilityIdentifiable: RawRepresentable {}

public extension AccessibilityIdentifiable where RawValue == String {

    init?(rawValue: String) { return nil }

    var rawValue: String {
        let full = String(reflecting: type(of: self)) + "_\(self)"
        return full.replacingOccurrences(of: "\"", with: "")
    }
}

