
import Foundation

public enum TabBarAccessibility: AccessibilityIdentifiable {
    case mainView
    case tabBar
}
