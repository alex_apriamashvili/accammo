
class SliderControlImpl: UIControl {

    weak var output: SliderControlOutput?

    struct ScaleDefaults {
        static let lhs: Double = 0
        static let rhs: Double = 100
        static let step: Double = 1
    }

    // MARK: - Values

    var minimal: Double
    var maximal: Double
    var step: Double
    var singleThumb: Bool

    var selectedMinValue: Double = ScaleDefaults.lhs
    var selectedMaxValue: Double = ScaleDefaults.rhs

    private var shouldUseRTL: Bool {
        return style?.shouldUseRTL ?? false
    }
    private var style: SliderControlStyle?

    // MARK: - Subviews

    /*
     -noun; Runner /ˈrənər/:
     a rod, groove, or blade on which something slides.
    */

    private let inactiveRunner = UIView()
    private let activeRunner = UIView()
    private let leftThumb = UIView()
    private let rightThumb = UIView()
    private var activeThumb: UIView?
    private var draggingStartPoint: CGPoint?

    // MARK: - Constraints

    private var rightThumbXConstraint: NSLayoutConstraint?
    private var leftThumbXConstraint: NSLayoutConstraint?
    private let context: String

    // MARK: - Initialization

    init(minimal: Double = ScaleDefaults.lhs,
         maximal: Double = ScaleDefaults.rhs,
         step: Double = ScaleDefaults.step,
         singleThumb: Bool = false,
         context: String) {
        self.minimal = minimal
        self.maximal = maximal
        self.selectedMinValue = minimal
        self.selectedMaxValue = maximal
        self.step = step
        self.singleThumb = singleThumb
        self.context = context

        super.init(frame: .zero)

        configure()
    }

    //  can be implemented as soon as either default dependencies or resources provider is available
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        configureSubviews()
        constrainSubviews()
        configureDragging()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateThumbs()
    }
}

// MARK: - SliderControlInput Implementation

extension SliderControlImpl: SliderControlInput {

    func setSelectedMinValue(_ value: Double) {
        selectedMinValue = value
        updateLeftThumb()
    }

    func setSelectedMaxValue(_ value: Double) {
        selectedMaxValue = value
        updateRightThumb()
    }

    func setStyle(_ style: SliderControlStyle) {
        self.style = style
    }

    func setDisabled(_ disable: Bool) {
        isUserInteractionEnabled = !disable
        leftThumb.alpha = disable ? 0.5 : 1.0
        rightThumb.alpha = disable ? 0.5 : 1.0
        inactiveRunner.alpha = disable ? 0.5 : 1.0
        activeRunner.alpha = disable ? 0.5 : 1.0
    }

    func redraw() {
        guard let style = style else { return }
        inactiveRunner.backgroundColor = style.inactiveRunnerColor
        activeRunner.backgroundColor = style.activeRunnerColor
        leftThumb.layer.borderColor = style.thumbBorderColor.cgColor
        leftThumb.layer.shadowColor = style.thumbShadowColor.cgColor
        leftThumb.backgroundColor = style.thumbColor
        rightThumb.layer.borderColor = style.thumbBorderColor.cgColor
        rightThumb.layer.shadowColor = style.thumbShadowColor.cgColor
        rightThumb.backgroundColor = style.thumbColor
    }

    func register(output: SliderControlOutput) {
        self.output = output
    }
}

// MARK: - Configuration

private extension SliderControlImpl {

    struct ConfigurationConstants {
        static let borderWidth: CGFloat = 1
        static let shadowOpacity: CGFloat = 0.15
        static let shadowRadius: CGFloat = 2
        static let cornerRadius: CGFloat = 13.5
    }

    func configureSubviews() {
        configureRunners()
        configureLeftThumb()
        configureRightThumb()
    }

    func configureRunners() {
        inactiveRunner.backgroundColor = style?.inactiveRunnerColor
        addSubview(inactiveRunner)

        activeRunner.backgroundColor = style?.activeRunnerColor
        addSubview(activeRunner)
    }

    func configureLeftThumb() {
        configureThumb(leftThumb)
        addSubview(leftThumb)
    }

    func configureRightThumb() {
        configureThumb(rightThumb)
        rightThumb.isHidden = singleThumb
        addSubview(rightThumb)
    }

    func configureThumb(_ thumb: UIView) {
        thumb.isUserInteractionEnabled = true
        thumb.clipsToBounds = false
        thumb.layer.borderColor = style?.thumbBorderColor.cgColor
        thumb.layer.borderWidth = ConfigurationConstants.borderWidth
        thumb.layer.shadowColor = style?.thumbShadowColor.cgColor
        thumb.layer.shadowOpacity = Float(ConfigurationConstants.shadowOpacity)
        thumb.layer.shadowRadius = ConfigurationConstants.shadowRadius
        thumb.layer.cornerRadius = ConfigurationConstants.cornerRadius
        thumb.layer.shadowOffset = .zero
        thumb.backgroundColor = style?.thumbColor
    }

    func configureDragging() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(drag(_:)))
        panGesture.delegate = self
        addGestureRecognizer(panGesture)
    }
}

// MARK: - Gesture Handling

private extension SliderControlImpl {

    struct ActionConstants {
        static let insetPercentage: CGFloat = 0.7
        static let thumbDiameter: CGFloat = 27
    }

    @objc
    func drag(_ gesture: UIPanGestureRecognizer) {
        let velocity = gesture.velocity(in: self)
        if gesture.state == .began {
            activeThumb = activeThumb(at: draggingStartPoint, velocity: velocity)
        }
        guard let activeThumb = activeThumb else {
            draggingStartPoint = CGPoint(x: CGFloat.infinity, y: CGFloat.infinity)
            return
        }

        handleTouchesCountinue(gesture, activeThumb: activeThumb)

        let wasDraggingStopped = gesture.state == .ended || gesture.state == .cancelled
        if wasDraggingStopped {
            draggingStartPoint = CGPoint(x: CGFloat.infinity, y: CGFloat.infinity)
            if self.activeThumb == leftThumb {
                output?.slider(self, didFinishUserInteractionWithMinEdgeThumbWithValue: selectedMinValue)
            } else {
                output?.slider(self, didFinishUserInteractionWithMaxEdgeThumbWithValue: selectedMaxValue)
            }
            output?.slider(
                self,
                didFinishUserInteractionWithSelectedMinimalValue: selectedMinValue,
                selectedMaximalValue: selectedMaxValue
            )
            self.activeThumb = nil
        }
    }

    // MARK: - Handlers

    func handleTouchesCountinue(_ gesture: UIPanGestureRecognizer, activeThumb: UIView) {
        let locationInRunner = gesture.location(in: self.inactiveRunner)
        if activeThumb == leftThumb {
            let minValueCandidate = leftThumbValue(locationInRunner)
            selectedMinValue = min(minValueCandidate, selectedMaxValue)
            output?.slider(self, didChangeMinimalValue: selectedMinValue)
            updateLeftThumb()
        } else if activeThumb == rightThumb {
            let maxValueCandidate = rightThumbValue(locationInRunner)
            selectedMaxValue = max(maxValueCandidate, selectedMinValue)
            output?.slider(self, didChangeMaximalValue: selectedMaxValue)
            updateRightThumb()
        }
        sendActions(for: .valueChanged)
    }

    // MARK: - Calculations [ Better to be moved out and covered with UTs ]

    func activeThumb(at location: CGPoint?, velocity: CGPoint) -> UIView? {
        guard let location = location else { return nil }

        let leftThumbFrame = leftThumb.frame
        let lhsdx = -leftThumbFrame.size.width * ActionConstants.insetPercentage / 2
        let lhsdy = -leftThumbFrame.size.height * ActionConstants.insetPercentage / 2
        let leftThumbArea = leftThumbFrame.insetBy(dx: lhsdx, dy: lhsdy)

        let rightThumbFrame = rightThumb.frame
        let rhsdx = -rightThumbFrame.size.width * ActionConstants.insetPercentage / 2
        let rhsdy = -rightThumbFrame.size.height * ActionConstants.insetPercentage / 2
        let rightThumbArea = rightThumbFrame.insetBy(dx: rhsdx, dy: rhsdy)

        let isLeftThumb = leftThumbArea.contains(location)
        let isRightThumb = !singleThumb && rightThumbArea.contains(location)

        guard isLeftThumb || isRightThumb else { return nil }

        if isLeftThumb && isRightThumb {
            let shouldPickRightControl = shouldUseRTL ? velocity.x < 0 : velocity.x > 0
            return shouldPickRightControl ? rightThumb : leftThumb
        }

        return isLeftThumb ? leftThumb : rightThumb
    }

    func leftThumbPosition(_ value: Double) -> CGFloat {
        let rightOffset = singleThumb ? 0 : ActionConstants.thumbDiameter
        let percent = percentage(value)
        let result = (inactiveRunner.frame.size.width - rightOffset) * CGFloat(percent)

        return result
    }

    func rightThumbPosition(_ value: Double) -> CGFloat {
        let leftOffset = ActionConstants.thumbDiameter
        let percent = percentage(value)
        let result = CGFloat(leftOffset) + (inactiveRunner.frame.size.width - leftOffset) * CGFloat(percent)

        return result
    }

    func percentage(_ value: Double) -> Double {
        let percentage = (value - minimal) / (maximal - minimal)
        return max(min(percentage, 1), 0.0)
    }

    func leftThumbValue(_ coordinate: CGPoint) -> Double {
        let percentage = leftThumbProcentage(coordinate.x)
        return nearestValue(percentage)
    }

    func rightThumbValue(_ coordinate: CGPoint) -> Double {
        let percentage = rightThumbProcentage(coordinate.x)
        return nearestValue(percentage)
    }

    func leftThumbProcentage(_ xPosition: CGFloat) -> Double {
        let rightOffset = singleThumb ? 0 : ActionConstants.thumbDiameter
        let width = inactiveRunner.frame.size.width
        var position = xPosition

        if shouldUseRTL {
            let slope = (width - rightOffset) / (rightOffset - width)
            position = 0 + slope * (position - width)
        }

        var percentage = position / (width - rightOffset)
        percentage = max(min(percentage, 1.0), 0.0)
        return Double(percentage)
    }

    func rightThumbProcentage(_ xPosition: CGFloat) -> Double {
        let leftOffset = ActionConstants.thumbDiameter
        let width = inactiveRunner.frame.size.width
        var position = xPosition

        if shouldUseRTL {
            let slope = (leftOffset - width) / (width - leftOffset)
            position = width + slope * (position - 0)
        }

        var percentage = (position - leftOffset) / (width - leftOffset)
        percentage = max(min(percentage, 1.0), 0.0)
        return Double(percentage)
    }

    func nearestValue(_ percentage: Double) -> Double {
        let numberOfSteps = ceil((maximal - minimal) / step)
        return minimal + round(numberOfSteps * percentage) * step
    }

    // MARK: - Updates

    func updateThumbs() {
        updateLeftThumb()
        updateRightThumb()
    }

    func updateLeftThumb() {
        let constraintValue = leftThumbPosition(selectedMinValue)
        leftThumbXConstraint?.constant = constraintValue
    }

    func updateRightThumb() {
        let constraintValue = rightThumbPosition(selectedMaxValue)
        rightThumbXConstraint?.constant = constraintValue
    }
}

extension SliderControlImpl: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        draggingStartPoint = touch.location(in: self)
        let wasThumbTouched = activeThumb(at: draggingStartPoint, velocity: .zero) != nil

        return wasThumbTouched
    }
}

// MARK: - Constraints

private extension SliderControlImpl {

    struct Layout {
        static let height: CGFloat = 40
        static let buttonDiameter: CGFloat = 27
        static let buttonRadius: CGFloat = 13.5
        static let runnerHeight: CGFloat = 2
    }

    func constrainSubviews() {
        constrainContainer()
        constrainInactiveRunner()
        constrainActiveRunner()
        constrainLeftThumb()
        constrainRightThumb()
    }

    func constrainContainer() {
        translatesAutoresizingMaskIntoConstraints = false

        heightAnchor.constraint(equalToConstant: Layout.height).isActive = true
    }

    func constrainInactiveRunner() {
        inactiveRunner.translatesAutoresizingMaskIntoConstraints = false

        inactiveRunner.heightAnchor.constraint(equalToConstant: Layout.runnerHeight).isActive = true

        inactiveRunner.leadingAnchor.constraint(
            equalTo: leadingAnchor,
            constant: Layout.buttonRadius
            ).isActive = true
        inactiveRunner.trailingAnchor.constraint(
            equalTo: trailingAnchor,
            constant: -Layout.buttonRadius
            ).isActive = true

        inactiveRunner.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

    func constrainActiveRunner() {
        activeRunner.translatesAutoresizingMaskIntoConstraints = false

        activeRunner.leadingAnchor.constraint(equalTo: leftThumb.centerXAnchor).isActive = true
        activeRunner.trailingAnchor.constraint(equalTo: rightThumb.centerXAnchor).isActive = true
        activeRunner.heightAnchor.constraint(equalTo: inactiveRunner.heightAnchor).isActive = true
        activeRunner.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

    func constrainLeftThumb() {
        leftThumb.translatesAutoresizingMaskIntoConstraints = false

        leftThumb.widthAnchor.constraint(equalToConstant: Layout.buttonDiameter).isActive = true
        leftThumb.heightAnchor.constraint(equalToConstant: Layout.buttonDiameter).isActive = true
        leftThumb.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        leftThumbXConstraint = leftThumb.centerXAnchor.constraint(equalTo: inactiveRunner.leadingAnchor)
        leftThumbXConstraint?.isActive = true
    }

    func constrainRightThumb() {
        rightThumb.translatesAutoresizingMaskIntoConstraints = false

        rightThumb.widthAnchor.constraint(equalToConstant: Layout.buttonDiameter).isActive = true
        rightThumb.heightAnchor.constraint(equalToConstant: Layout.buttonDiameter).isActive = true
        rightThumb.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        rightThumbXConstraint = rightThumb.centerXAnchor.constraint(equalTo: inactiveRunner.leadingAnchor)
        rightThumbXConstraint?.isActive = true
    }
}
