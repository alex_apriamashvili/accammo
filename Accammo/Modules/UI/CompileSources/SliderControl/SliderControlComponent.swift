
// MARK: - Dependencies

/// a struct which describes all the style components
/// needed for the configuration of the component
public struct SliderControlStyle {

    /// a color of a runner (line) between minimal and maximal thumbs
    let activeRunnerColor: UIColor

    /// a color of a runner (line) aside from minimal and maximal thumbs
    let inactiveRunnerColor: UIColor

    /// a color of a border around each thumb
    let thumbBorderColor: UIColor

    /// a thumb shadow color
    let thumbShadowColor: UIColor

    /// a thumb backgroung color
    let thumbColor: UIColor

    let shouldUseRTL: Bool

    public init(activeRunnerColor: UIColor,
                inactiveRunnerColor: UIColor,
                thumbBorderColor: UIColor,
                thumbShadowColor: UIColor,
                thumbColor: UIColor,
                shouldUseRTL: Bool) {
        self.activeRunnerColor = activeRunnerColor
        self.inactiveRunnerColor = inactiveRunnerColor
        self.thumbBorderColor = thumbBorderColor
        self.thumbShadowColor = thumbShadowColor
        self.thumbColor = thumbColor
        self.shouldUseRTL = shouldUseRTL
    }
}

// MARK: - Component

/// an input protocol which provides the ability to interact with the control
public protocol SliderControlInput: Redrawable {

    /// registers an output (delegate) for the slider
    ///
    /// - parameter output: an entyty which conforms to `SliderControlOutput`
    ///                     and dedicated to serve as a delegate for the slider
    func register(output: SliderControlOutput)

     /// sets an selected minimum value
    func setSelectedMinValue(_ value: Double)

    /// sets an selected maximum value
    func setSelectedMaxValue(_ value: Double)

    /// sets the appearance of the slider.
    /// after it is set redraw has to be called.
    func setStyle(_ style: SliderControlStyle)

    /// sets disabled of label and style of slider
    func setDisabled(_ disable: Bool)
}

/// an output protocol which is dedicated to provide events to a receiver
public protocol SliderControlOutput: class {

    typealias Slider = UIView & SliderControlInput

    /// notifies the receiver about a maximal value changed in the slider
    ///
    /// - parameter slider: the slider which value has been changed
    /// - parameter value: a new maximal selected value for slider
    func slider(_ slider: Slider,
                didChangeMaximalValue value: Double)

    /// notifies the receiver about a minimal value changed in the slider
    ///
    /// - parameter slider: the slider which value has been changed
    /// - parameter value: a new minimal selected value for slider
    func slider(_ slider: Slider,
                didChangeMinimalValue value: Double)

    /// notifies the receiver about the user interaction has ended or cancelled
    ///
    /// - parameter slider: the last active slider
    /// - parameter minValue: a user selected minimal value for the slider
    /// - parameter maxValue: a user selected maximal value for the slider
    func slider(_ slider: Slider,
                didFinishUserInteractionWithSelectedMinimalValue minValue: Double,
                selectedMaximalValue maxValue: Double)

    /// notifies the receiver when the user interaction with a minimal edge thumb has been stopped
    ///
    /// - parameter slider: the last active slider
    /// - parameter value: a user-defined minimal value at the moment when the interaction is finished
    func slider(_ slider: Slider,
                didFinishUserInteractionWithMinEdgeThumbWithValue value: Double)

    /// notifies the receiver when the user interaction with a maximal edge thumb has been stopped
    ///
    /// - parameter slider: the last active slider
    /// - parameter value: a user-defined maximal value at the moment when the interaction is finished
    func slider(_ slider: Slider,
                didFinishUserInteractionWithMaxEdgeThumbWithValue value: Double)
}

/// `SliderControlOutput` default implementation,
/// hence all functions are optional
extension SliderControlOutput {

    func slider(_ slider: SliderControlInput,
                didChangeMaximalValue value: Double) {}

    func slider(_ slider: SliderControlInput,
                didChangeMinimalValue value: Double) {}

    func slider(_ slider: SliderControlInput,
                didFinishUserInteractionWithSelectedMinimalValue minValue: Double,
                selectedMaximalValue maxValue: Double) {}
}

/// a component which provides different types of slider upun a request
public protocol SliderControlComponent {

    typealias Element = UIView & SliderControlInput

    associatedtype Value

    /// creates a one sided (minimal thumb only) slider with given parameters
    ///
    /// - parameter lhsValue: a minimal thumb value
    /// - parameter step: a value increment step
    func oneSidedSlider(lhsValue: Value,
                        step: Value,
                        context: String) -> Element

    /// creates a double sided (minimal & maximal thumbs) slider with given parameters
    ///
    /// - parameter lhsValue: a minimal thumb value
    /// - parameter rhsValue: a maximal thumb value
    /// - parameter step: a value increment step
    func doubleSidedSlider(lhsValue: Value,
                           rhsValue: Value,
                           step: Value,
                           context: String) -> Element
}

/// an extension which simplifies the calling of set up methods
/// this exactly extension is for `Double` `ValueType` only
public extension SliderControlComponent where Value == Double {

    /// a shortcut for the `oneSidedSlider(lhsValue: step:)` function
    ///
    /// which puts `lhsValue = 0` and `step = 1` by default
    func oneSidedSlider(context: String) -> Element {
        return oneSidedSlider(lhsValue: 0, step: 1, context: context)
    }

    /// a shortcut for the `doubleSidedSlider(lhsValue: rhsValue: step:)` function
    ///
    /// which puts `lhsValue = 0`, `rhsValue = 100` and `step = 1` by default
    func doubleSidedSlider(context: String) -> Element {
        return doubleSidedSlider(lhsValue: 0, rhsValue: 100, step: 1, context: context)
    }
}

/// the one of all possible implementations of the Component protocol
/// assumes `Double` as a `ValueType`
public class SliderControlComponentImpl: SliderControlComponent {

    public typealias ValueType = Double

    public init() {
    }

    public func oneSidedSlider(lhsValue: ValueType, step: ValueType, context: String) -> Element {
        return SliderControlImpl(minimal: lhsValue, step: step, singleThumb: true,
                                 context: context)
    }

    public func doubleSidedSlider(lhsValue: ValueType,
                                  rhsValue: ValueType,
                                  step: ValueType,
                                  context: String) -> Element {
        return SliderControlImpl(minimal: lhsValue, maximal: rhsValue, step: step,
                                 context: context)
    }
}
