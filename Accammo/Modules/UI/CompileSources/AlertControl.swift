

public final class AlertControl {

    public static func errorAlert(message: String) -> UIAlertController {
        let alertView = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

        return alertView
    }
}
