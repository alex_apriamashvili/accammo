
import SafariServices

@objc public class RespondChecker: NSObject {

    @objc public static func supportsSafeAreaLayoutGuide() -> Bool {
        if #available(iOS 11.0, *) {
            return UIView.instancesRespond(to: #selector(getter: UIView.safeAreaLayoutGuide))
        } else {
            return false
        }
    }

    @objc public static func supportsSafeAreaInsets() -> Bool {
        if #available(iOS 11.0, *) {
            return UIView.instancesRespond(to: #selector(getter: UIView.safeAreaInsets))
        } else {
            return false
        }
    }

    @objc public static func supportsContentInsetAdjustmentBehavior() -> Bool {
        if #available(iOS 11.0, *) {
            return UIScrollView.instancesRespond(to: #selector(getter: UIScrollView.contentInsetAdjustmentBehavior))
        } else {
            return false
        }
    }

    @objc public static func supportsAdjustedContentInset() -> Bool {
        if #available(iOS 11.0, *) {
            return UIScrollView.instancesRespond(to: #selector(getter: UIScrollView.adjustedContentInset))
        } else {
            return false
        }
    }

    @objc public static func supportsDismissButtonStyle() -> Bool {
        if #available(iOS 11.0, *) {
            return SFSafariViewController.instancesRespond(
                to: #selector(getter: SFSafariViewController.dismissButtonStyle)
            )
        } else {
            return false
        }
    }

    @objc public static func supportsInsetsLayoutMarginsFromSafeArea() -> Bool {
        if #available(iOS 11.0, *) {
            return UIView.instancesRespond(to: #selector(getter: UIView.insetsLayoutMarginsFromSafeArea))
        } else {
            return false
        }
    }

    @objc public static func supportsPrefersLargeTitles() -> Bool {
        if #available(iOS 11.0, *) {
            return UINavigationBar.instancesRespond(to: #selector(getter: UINavigationBar.prefersLargeTitles))
        } else {
            return false
        }
    }
}

