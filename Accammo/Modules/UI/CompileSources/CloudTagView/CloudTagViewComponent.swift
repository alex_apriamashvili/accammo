
public struct CloudTagViewStyle {
    let font: UIFont

    var textColor: UIColor
    var backgroundColor: UIColor
    var borderColor: UIColor

    var selectedTextColor: UIColor
    var selectedBackgroundColor: UIColor
    var selectedBorderColor: UIColor

    var disabledTextColor: UIColor
    var disabledBackgroundColor: UIColor
    var disabledBorderColor: UIColor

    var moreFont: UIFont
    var moreTextColor: UIColor
    var moreBackgroundColor: UIColor
    var moreBorderColor: UIColor

    public init(font: UIFont,
                textColor: UIColor,
                backgroundColor: UIColor,
                borderColor: UIColor,
                selectedTextColor: UIColor,
                selectedBackgroundColor: UIColor,
                selectedBorderColor: UIColor,
                disabledTextColor: UIColor,
                disabledBackgroundColor: UIColor,
                disabledBorderColor: UIColor,
                moreFont: UIFont,
                moreTextColor: UIColor,
                moreBackgroundColor: UIColor,
                moreBorderColor: UIColor) {
        self.font = font
        self.textColor = textColor
        self.backgroundColor = backgroundColor
        self.borderColor = borderColor
        self.selectedTextColor = selectedTextColor
        self.selectedBackgroundColor = selectedBackgroundColor
        self.selectedBorderColor = selectedBorderColor
        self.disabledTextColor = disabledTextColor
        self.disabledBackgroundColor = disabledBackgroundColor
        self.disabledBorderColor = disabledBorderColor
        self.moreFont = moreFont
        self.moreTextColor = moreTextColor
        self.moreBackgroundColor = moreBackgroundColor
        self.moreBorderColor = moreBorderColor
    }
}

public protocol CloudTagViewOutput: class {
    /// tells the receiver that the 'More...' button has been tapped
    ///
    /// - parameter tagCloud: a patricular tag cloud view that detected the tap on a 'More' button
    func didTapMoreButton(tagCloud: CloudTagView)

    /// terlls the receiver that an item has been selected / deselected at a particular index
    ///
    /// - parameter tagCloud: a patricular tag cloud view that detected the tap
    /// - parameter index: an index of the item tapped
    func tagCloud(_ tagCloud: CloudTagView, didSelectItemAt index: Int)
}
