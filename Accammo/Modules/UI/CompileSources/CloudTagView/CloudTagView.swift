
public final class CloudTagView: UIView {

    public enum State {
        case collapsed
        case expanded
    }

    public var moreText: String = "More..."

    private(set) var style: CloudTagViewStyle?
    private(set) var items: [CloudTagItem] = []
    private(set) var selectedIndexes: Set<Int> = []
    private(set) var disabledIndexes: Set<Int> = []

    private var views: [CloudTagItemView] = []
    private var moreItemView: CloudTagItemView?
    private var rowViews: [UIView] = []

    private var currentWidth: CGFloat = 0.0
    private let itemSpacing: CGFloat = 10.0
    private let rowSpacing: CGFloat = 10.0
    private let collapseRowCount: Int = 2
    private var state: State = .collapsed

    private var context: String = ""
    public weak var output: CloudTagViewOutput?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
}

// MARK: - View configuration

private extension CloudTagView {

    func setUp() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
    }
}

// MARK: - Item configuration

public extension CloudTagView {

    func item(at index: Int) -> CloudTagItem? {
        guard index < items.count else { return nil }
        return items[index]
    }

    func setState(_ state: State) {
        self.state = state
    }

    func setItems(_ items: [CloudTagItem]) {
        self.items = items
        views = createItemViews()
    }

    func setSelectedIndexes(_ indexes: Set<Int>) {
        selectedIndexes = indexes
    }

    func setDisabledIndexes(_ indexes: Set<Int>) {
        disabledIndexes = indexes
    }

    func apply(_ style: CloudTagViewStyle) {
        self.style = style
    }

    func update(_ context: String) {
        self.context = context
    }

    func redraw() {
        updateLayout()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if currentWidth != bounds.width {
            currentWidth = bounds.width
            updateLayout()
        }
    }
}

private extension CloudTagView {

    enum ItemId {
        static let moreId = -1
    }

    func createItemViews() -> [CloudTagItemView] {
        guard let style = style else { return [] }

        var views = [CloudTagItemView]()

        for item in items {
            let view = CloudTagItemView(item: item, style: style, type: .item)
            view.output = self
            views.append(view)
        }
        return views
    }

    func createMoreItem() -> CloudTagItemView? {
        guard let style = style else { return nil }

        let moreItem = CloudTagItem(text: moreText, itemId: ItemId.moreId)
        let moreItemView = CloudTagItemView(item: moreItem, style: style, type: .more)
        moreItemView.output = self

        return moreItemView
    }

    func createRow() -> UIView {
        let row = UIView()
        row.translatesAutoresizingMaskIntoConstraints = false
        return row
    }

    func updateLayout() {
        if views.count == 0 || currentWidth == 0 { return }

        subviews.forEach { $0.removeFromSuperview() }

        layoutRows()
        layoutMoreItemIfNeeded()
    }

    func layoutRows() {
         rowViews = prepareRows()

        var spacing: CGFloat = 0
        var previousRowView: UIView?
        for (index, rowView) in rowViews.enumerated() {
            let rowTopAnchor = previousRowView?.bottomAnchor ?? topAnchor
            spacing = previousRowView == nil ? 0 : rowSpacing

            addSubview(rowView)
            rowView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            rowView.topAnchor.constraint(equalTo: rowTopAnchor, constant: spacing).isActive = true
            rowView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true

            if index == rowViews.count - 1 {
                rowView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -rowSpacing).isActive = true
            }
            previousRowView = rowView
        }
    }

    func prepareRows() -> [UIView] {
        var rowViews: [UIView] = []

        var rowView = createRow()

        var spacing: CGFloat = 0
        var contentWidth: CGFloat = 0.0
        var previousView: CloudTagItemView?

        for (index, view) in views.enumerated() {
            let itemLeadingAnchor = previousView?.trailingAnchor ?? rowView.leadingAnchor
            spacing = previousView == nil ? 0 : itemSpacing

            view.isEnabled = !disabledIndexes.contains(index)
            view.isSelected = view.isEnabled ? selectedIndexes.contains(index) : false

            contentWidth += view.intrinsicContentSize.width
            contentWidth += spacing
            let restWidth = currentWidth - contentWidth

            rowView.addSubview(view)
            view.leadingAnchor.constraint(equalTo: itemLeadingAnchor, constant: spacing).isActive = true
            view.topAnchor.constraint(equalTo: rowView.topAnchor).isActive = true
            view.bottomAnchor.constraint(equalTo: rowView.bottomAnchor).isActive = true

            previousView = view

            // next item
            if index + 1 < views.count {
                let nextView = views[index + 1]
                let isNextItemFitted = (restWidth >= spacing + nextView.intrinsicContentSize.width)

                if !isNextItemFitted {
                    view.trailingAnchor.constraint(greaterThanOrEqualTo: rowView.trailingAnchor,
                                                   constant: -restWidth).isActive = true

                    rowViews.append(rowView)
                    rowView = createRow()

                    previousView = nil
                    contentWidth = 0.0
                }
            } else {
                view.trailingAnchor.constraint(greaterThanOrEqualTo: rowView.trailingAnchor,
                                               constant: -restWidth).isActive = true
                rowViews.append(rowView)
            }

            if rowViews.count >= collapseRowCount && state == .collapsed {
                break
            }
        }

        return rowViews
    }

    func layoutMoreItemIfNeeded() {
        guard state == .collapsed,
            views.count != itemCountInAllRows(),
            let rowView = rowViews.last,
            let moreItemView = createMoreItem() else { return }

        self.moreItemView = moreItemView
        let moreItemWidth = moreItemView.intrinsicContentSize.width
        let subviews = rowView.subviews

        var contentWidth: CGFloat = 0
        for view in subviews {
            contentWidth += view.intrinsicContentSize.width
            contentWidth += itemSpacing
        }

        let reversedSubviews: [UIView] = subviews.reversed()
        for (index, view) in reversedSubviews.enumerated() {
            let width = view.intrinsicContentSize.width
            view.removeFromSuperview()

            contentWidth -= width
            contentWidth -= itemSpacing

            if width >= moreItemWidth {
                contentWidth += itemSpacing
                contentWidth += moreItemWidth

                var previousView: UIView? = nil
                if index + 1 < reversedSubviews.count {
                    previousView = reversedSubviews[index + 1]
                }
                let restWidth = currentWidth - contentWidth

                rowView.addSubview(moreItemView)
                moreItemView.leadingAnchor.constraint(equalTo: previousView?.trailingAnchor ?? rowView.leadingAnchor,
                                                      constant: previousView != nil ? itemSpacing : 0).isActive = true
                moreItemView.topAnchor.constraint(equalTo: rowView.topAnchor).isActive = true
                moreItemView.bottomAnchor.constraint(equalTo: rowView.bottomAnchor).isActive = true
                moreItemView.trailingAnchor.constraint(greaterThanOrEqualTo: rowView.trailingAnchor,
                                                       constant: -restWidth).isActive = true

                break
            }
        }
    }

    func itemCountInAllRows() -> Int {
        var count = 0
        for rowView in rowViews {
            count += rowView.subviews.count
        }
        return count
    }
}

// MARK: - CloudTagItemViewOutput

extension CloudTagView: CloudTagItemViewOutput {

    func select(view: CloudTagItemView) {
        if let moreItemView = moreItemView, moreItemView == view {
            output?.didTapMoreButton(tagCloud: self)
        } else if let index = views.firstIndex(of: view) {
            output?.tagCloud(self, didSelectItemAt: index)
        }
    }
}

// MARK: - CloudTagView Information

extension CloudTagView {

    public var selectedIndices: Set<Int> {
        return selectedIndexes
    }

    public var count: Int {
        return items.count
    }

    public func index(of item: CloudTagItem) -> Int? {
      return items.firstIndex(of: item)
    }
}
