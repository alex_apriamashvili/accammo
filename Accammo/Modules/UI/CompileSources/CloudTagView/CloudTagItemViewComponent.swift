

protocol CloudTagItemViewOutput: class {
    func select(view: CloudTagItemView)
}
