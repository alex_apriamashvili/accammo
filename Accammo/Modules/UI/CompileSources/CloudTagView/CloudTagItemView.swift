
final class CloudTagItemView: UIButton {

    public enum ViewType {
        case more
        case item
    }

    private let cornerRadius: CGFloat = 2.0
    private let borderWidth: CGFloat = 1.0
    private let horizontalInset: CGFloat = 10.0
    private let verticalInset: CGFloat = 10.0
    private var contentSize: CGSize = .zero

    weak var output: CloudTagItemViewOutput?
    private var style: CloudTagViewStyle?
    private var type: ViewType = .item

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    public convenience init(item: CloudTagItem, style: CloudTagViewStyle, type: ViewType) {
        self.init(frame: .zero)
        self.style = style
        self.type = type
        setTitle(item.text, for: .normal)

        if type == .item {
            setTitleColor(style.textColor, for: .normal)
            setTitleColor(style.selectedTextColor, for: .selected)
            setTitleColor(style.disabledTextColor, for: .disabled)
            titleLabel?.font = style.font
        } else {
            setTitleColor(style.moreTextColor, for: .normal)
            setTitleColor(style.moreTextColor, for: .selected)
            setTitleColor(style.moreTextColor, for: .disabled)
            titleLabel?.font = style.moreFont
        }

        updateStyle()
        updateContentSize()
    }

    // MARK: - Override

    override public var intrinsicContentSize: CGSize {
        return contentSize
    }

    override public var isEnabled: Bool {
        didSet {
            updateStyle()
        }
    }

    override public var isSelected: Bool {
        didSet {
            updateStyle()
        }
    }
}

// MARK: - View configuration

private extension CloudTagItemView {

    private func setup() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .clear

        titleLabel?.textAlignment = .center
        clipsToBounds = true

        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth

        addTarget(self, action: #selector(tapItem(_:)), for: .touchUpInside)
    }

    private func updateStyle() {
        guard let style = style else { return }

        if isEnabled {
            if isSelected {
                backgroundColor = type == .item ? style.selectedBackgroundColor : style.moreBackgroundColor
                layer.borderColor = type == .item ? style.selectedBorderColor.cgColor : style.moreBorderColor.cgColor
            } else {
                backgroundColor = type == .item ? style.backgroundColor : style.moreBackgroundColor
                layer.borderColor = type == .item ? style.borderColor.cgColor : style.moreBorderColor.cgColor
            }
        } else {
            backgroundColor = type == .item ? style.disabledBackgroundColor : style.moreBackgroundColor
            layer.borderColor = type == .item ? style.disabledBorderColor.cgColor : style.moreBorderColor.cgColor
        }
    }

    private func updateContentSize() {
        guard let text = titleLabel?.text as NSString?, let font = titleLabel?.font else {
            return
        }

        let maxSize: CGSize = CGSize(width: CGFloat.infinity, height: CGFloat.infinity)
      let attributes: [NSAttributedString.Key: Any] = [.font: font]

        let fitSize = text.boundingRect(with: maxSize,
                                        options: [.usesLineFragmentOrigin, .usesFontLeading],
                                        attributes: attributes,
                                        context: nil).size

        contentSize = CGSize(width: fitSize.width + 2 * horizontalInset,
                             height: fitSize.height + 2 * verticalInset)
    }
}

// MARK: - Actions

private extension CloudTagItemView {

    @objc func tapItem(_ item: CloudTagItemView) {
        output?.select(view: self)
    }
}
