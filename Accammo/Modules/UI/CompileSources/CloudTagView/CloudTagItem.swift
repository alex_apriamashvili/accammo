

public struct CloudTagItem {
    public let text: String
    public let itemId: Int

    public init(text: String, itemId: Int) {
        self.text = text
        self.itemId = itemId
    }
}

extension CloudTagItem: Equatable {

    public static func == (lhs: CloudTagItem, rhs: CloudTagItem) -> Bool {
        return  lhs.text == rhs.text &&
            lhs.itemId == rhs.itemId
    }
}
