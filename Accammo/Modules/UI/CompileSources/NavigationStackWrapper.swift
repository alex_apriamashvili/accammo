
public final class NavigationStackWrapper {

  public static func wrap(_ controller: UIViewController,
                          large: Bool = false,
                          translucent: Bool = true) -> UINavigationController {
    let stack = UINavigationController(rootViewController: controller)

    if #available(iOS 11.0, *) {
      stack.navigationBar.prefersLargeTitles = large
    }
    stack.navigationBar.isTranslucent = translucent

    return stack
  }
}
