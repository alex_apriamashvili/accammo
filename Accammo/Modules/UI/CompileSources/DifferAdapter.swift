
import Differ

public struct UIComponentsDifferDependencies: HasUIComponentsDifferAdapter {
    public let differAdapter: UIComponentsDifferAdapter = UIComponentsDiffer()

    public init() {}
}

public struct UIComponentsDiffer: UIComponentsDifferAdapter {

    public func diff(previous: [SectionViewModel],
                     update: [SectionViewModel],
                     isEqualSection: @escaping SectionEquality,
                     isEqualElement: @escaping ElementEquality) -> [UIComponentsDifferElement] {

        let diff = previous.nestedExtendedDiff(
            to: update,
            isEqualSection: isEqualSection,
            isEqualElement: isEqualElement
        )

        return diff.map { (element) -> UIComponentsDifferElement in
            switch element {

            case .deleteSection(let index):
                return .deleteSection(index)
            case .insertSection(let index):
                return .insertSection(index)
            case .moveSection(let from, let to):
                return .moveSection(from: from, to: to)
            case .deleteElement(let row, let section):
                return .deleteElement(row, section: section)
            case .insertElement(let row, let section):
                return .insertElement(row, section: section)
            case .moveElement(let from, let to):
                return .moveElement(from: from, to: to)
            }
        }
    }
}


