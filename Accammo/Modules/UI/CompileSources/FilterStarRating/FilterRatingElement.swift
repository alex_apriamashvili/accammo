
class FilterRatingElementImpl: UIButton, FilterRatingElement {

    weak var output: SingleFilterRatingComponentOutput?

    private(set) var value: Int = 0

    private(set) var icon: UIImage?
    private(set) var activeColor: UIColor?
    private(set) var activeContentColor: UIColor?
    private(set) var inactiveColor: UIColor?
    private(set) var inactiveContentColor: UIColor?
    private(set) var borderColor: UIColor?
    private(set) var inactiveTextColor: UIColor?
    private(set) var titleFont: UIFont?
    private(set) var radii: CGFloat?

    init(value: Int) {
        self.value = value
        super.init(frame: .zero)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    private func setUp() {
        preconfigure()
        configureAction()
        configureIcon()
        configureTitle()

        configureInsets()
        reconfigure()
    }

    @objc
    func tap() {
        isSelected = !isSelected
        reconfigure()
        output?.didSelect(self)
    }

    func setSelected(_ selected: Bool) {
        isSelected = selected
        reconfigure()
    }

    func setStyle(_ style: FilterRatingStyleProvider) {
        icon = style.icon.withRenderingMode(.alwaysTemplate)
        activeColor = style.activeColor
        activeContentColor = style.activeContentColor
        inactiveColor = style.inactiveColor
        inactiveContentColor = style.inactiveContentColor
        borderColor = style.borderColor
        inactiveTextColor = style.inactiveTextColor
        titleFont = style.titleFont
        radii = style.radii
    }

    func redraw() {
        preconfigure()
        configureIcon()
        configureTitle()
        configureInsets()
        reconfigure()
    }
}

// MARK: - Configuration

private extension FilterRatingElementImpl {

    func preconfigure() {
        layer.masksToBounds = true
        layer.borderColor = borderColor?.cgColor
        layer.cornerRadius = radii ?? 0
    }

    func reconfigure() {
        if isSelected {
            configureActiveState()
        } else {
            configureInactiveState()
        }
    }

    func configureActiveState() {
        tintColor = activeContentColor
        backgroundColor = activeColor
        setTitleColor(activeContentColor, for: .normal)
        layer.borderWidth = 0
    }

    func configureInactiveState() {
        tintColor = inactiveContentColor
        backgroundColor = inactiveColor
        setTitleColor(inactiveTextColor, for: .normal)
        layer.borderWidth = 1
    }

    func configureAction() {
        addTarget(
            self,
            action: #selector(tap),
            for: .touchUpInside
        )
    }

    func configureIcon() {
        setImage(icon, for: .normal)
    }

    func configureTitle() {
        setTitle(String(value), for: .normal)
        titleLabel?.font = titleFont
    }
}

// MARK: - Layout

private extension FilterRatingElementImpl {

    struct Insets {
        static let ordinaryEdge: CGFloat = 12.0
        static let labelToImage: CGFloat = 5.0
        static let labelTopInset: CGFloat = 3.0
        static let singleCharacterWidth: CGFloat = 8.0
    }

    func configureInsets() {
        imageEdgeInsets = imageInsets()
        titleEdgeInsets = labelInsets()
        contentEdgeInsets = contentInsets()
    }

    func imageInsets() -> UIEdgeInsets {
        let attribute = semanticContentAttribute
        let layoutDirection = UIView.userInterfaceLayoutDirection(for: attribute)

        let imageWidth = icon?.size.width ?? 0
        let rightInset = (imageWidth + Insets.singleCharacterWidth + Insets.labelToImage) / 2
        let leftInset = rightInset + Insets.labelToImage

        if layoutDirection == .leftToRight {
            return UIEdgeInsets(
                top: imageEdgeInsets.top,
                left: leftInset,
                bottom: imageEdgeInsets.bottom,
                right: -rightInset
            )
        } else {
            return UIEdgeInsets(
                top: imageEdgeInsets.top,
                left: -leftInset,
                bottom: imageEdgeInsets.bottom,
                right: rightInset
            )
        }
    }

    func labelInsets() -> UIEdgeInsets {
        let attribute = semanticContentAttribute
        let layoutDirection = UIView.userInterfaceLayoutDirection(for: attribute)

        let imageWidth = icon?.size.width ?? 0
        let rightInset = imageWidth + Insets.singleCharacterWidth

        if layoutDirection == .leftToRight {
            return UIEdgeInsets(
                top: Insets.labelTopInset,
                left: -rightInset,
                bottom: titleEdgeInsets.bottom,
                right: titleEdgeInsets.right
            )
        } else {
            return UIEdgeInsets(
                top: Insets.labelTopInset,
                left: titleEdgeInsets.left,
                bottom: titleEdgeInsets.bottom,
                right: -rightInset
            )
        }
    }

    func contentInsets() -> UIEdgeInsets {
        return UIEdgeInsets(
            top: Insets.ordinaryEdge,
            left: contentEdgeInsets.left,
            bottom: contentEdgeInsets.right,
            right: Insets.ordinaryEdge
        )
    }
}
