
class FilterRatingContainerImpl: UIView, FilterRatingContainer {

    typealias Element = UIButton & FilterRatingElement

    private(set) var elementsCount: Int = 0
    private(set) var elements: [Element] = []
    private(set) var value: [Int]  = []

    weak var output: FilterRatingContainerComponentOutput?
    private var style: FilterRatingStyleProvider?

    init(elements: Int) {
        elementsCount = elements
        super.init(frame: .zero)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    private func setUp() {
        elements = createElements()
        layoutElements()
    }

    func setRating(_ value: FilterRatingContainer.Rating) {
        self.value = value
        updateSelections(value)
    }

    func setStyle(_ style: FilterRatingStyleProvider) {
        self.style = style
    }

    func redraw() {
        elements.forEach {
            if let style = style {
                $0.setStyle(style)
            }
            $0.redraw()
        }
    }
}

// MARK: Private Functions

private extension FilterRatingContainerImpl {

    private func createElements() -> [Element] {
        var elements = [Element]()
        for index in 0..<elementsCount {
            let value = index + 1
            let element = FilterRatingElementImpl(value: value)
            element.output = self
            elements.append(element)
            addSubview(element)
        }
        return elements
    }

    private func updateSelections(_ selections: Rating) {
        let selectionsSet = Set(selections)
        elements.forEach {
            let shouldBeSelected = selectionsSet.contains($0.value)
            $0.setSelected(shouldBeSelected)
        }
    }
}

// MARK: - Layout

private extension FilterRatingContainerImpl {

    struct Layout {
        static let elementOffset: CGFloat = 9
        static let padding: CGFloat = 0
        static let topBottomPadding: CGFloat = 16
    }

    func layoutElements() {
        guard let first = elements.first,
            let last = elements.last else { return }
        layoutFirst(first)
        layoutLast(last, first: first)
        guard elements.count > 2 else { return }
        let lastIndex = elements.count - 1
        for (index, element) in elements.enumerated() {
            guard index > 0, index < lastIndex else { continue }
            let prevElement = elements[index - 1]
            let nextElemet = elements[index + 1]
            layout(element, previous: prevElement, next: nextElemet)
        }
    }

    func layoutFirst(_ element: Element) {
        element.translatesAutoresizingMaskIntoConstraints = false

        element.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.padding).isActive = true
        element.topAnchor.constraint(equalTo: topAnchor, constant: Layout.padding).isActive = true
        element.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.padding).isActive = true
    }

    func layoutLast(_ element: Element, first: Element) {
        element.translatesAutoresizingMaskIntoConstraints = false

        element.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.padding).isActive = true
        element.topAnchor.constraint(equalTo: topAnchor, constant: Layout.padding).isActive = true
        element.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.padding).isActive = true
        element.widthAnchor.constraint(equalTo: first.widthAnchor, multiplier: 1).isActive = true
    }

    func layout(_ element: Element, previous prevElement: Element, next nextElement: Element) {
        element.translatesAutoresizingMaskIntoConstraints = false

        element.leadingAnchor.constraint(
            equalTo: prevElement.trailingAnchor,
            constant: Layout.elementOffset
            ).isActive = true
        element.topAnchor.constraint(equalTo: prevElement.topAnchor).isActive = true
        element.bottomAnchor.constraint(equalTo: prevElement.bottomAnchor).isActive = true
        element.trailingAnchor.constraint(
            equalTo: nextElement.leadingAnchor,
            constant: -Layout.elementOffset
            ).isActive = true

        element.widthAnchor.constraint(equalTo: prevElement.widthAnchor, multiplier: 1).isActive = true
    }
}

// MARK: - SingleFilterRatingComponentOutput Implementation

extension FilterRatingContainerImpl: SingleFilterRatingComponentOutput {

    func didSelect(_ element: FilterRatingComponent.Element) {
      guard let elementIndex = elements.firstIndex(where: { $0 == element }) else { return }
      value = elements.compactMap { if $0.isSelected { return $0.value }; return nil }
        output?.didSelect(element, at: elementIndex)
    }
}

// MARK: Convenience Methods

private extension Array {
    func element(at index: Int) -> Element? {
        return index < count ? self[index] : nil
    }
}
