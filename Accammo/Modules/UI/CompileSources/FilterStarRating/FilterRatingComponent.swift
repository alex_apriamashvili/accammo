
// MARK: - Component

/// protocol which provides a possibility to redraw a component
public protocol Redrawable {

    /// tells receiver that it needs to be redrawn
    /// might be usefull when either stiles or resources changed
    func redraw()
}

public struct FilterRatingStyleProvider {
    let icon: UIImage
    let activeColor: UIColor
    let activeContentColor: UIColor
    let inactiveColor: UIColor
    let inactiveContentColor: UIColor
    let borderColor: UIColor
    let inactiveTextColor: UIColor
    let titleFont: UIFont
    let radii: CGFloat

    public init(icon: UIImage,
                activeColor: UIColor,
                activeContentColor: UIColor,
                inactiveColor: UIColor,
                inactiveContentColor: UIColor,
                borderColor: UIColor,
                inactiveTextColor: UIColor,
                titleFont: UIFont,
                radii: CGFloat) {
        self.icon = icon
        self.activeColor = activeColor
        self.activeContentColor = activeContentColor
        self.inactiveColor = inactiveColor
        self.inactiveContentColor = inactiveContentColor
        self.borderColor = borderColor
        self.inactiveTextColor = inactiveTextColor
        self.titleFont = titleFont
        self.radii = radii
    }
}

/// a protocol which describes a whole element container
/// _created for an_ `Interface Segregation` _sake (lettrer 'I' in SOLID)_
public protocol FilterRatingContainer: Redrawable {

    typealias Rating = [Int]

    /// a container value
    /// returns a sequence of each element values in a proper order
    var value: Rating { get }

    /// a component output thats being notified about changes in view
    /// **this reference must be weak**
    var output: FilterRatingContainerComponentOutput? { get set }

    /// sets an initial value for the container
    func setRating(_ value: Rating)

    /// Configure the appearance style of the FilterRating
    func setStyle(_ style: FilterRatingStyleProvider)
}

/// a protocol which describes a single element
/// _created for an_ `Interface Segregation` _sake (lettrer 'I' in SOLID)_
public protocol FilterRatingElement: Redrawable {

    /// an element value
    var value: Int { get }

    /// a component output thats being notified about changes in view
    /// **this reference must be weak**
    var output: SingleFilterRatingComponentOutput? { get set }

    /// tells receiver whether it has to become slected or not
    ///
    /// - parameter selected: a boolean flag which describes a state that teh receiver has to reset to
    func setSelected(_ selected: Bool)

    /// Configure the appearance style of the FilterRating
    func setStyle(_ style: FilterRatingStyleProvider)
}

/// an output for a single component
public protocol SingleFilterRatingComponentOutput: class {

    /// tells receiver that the element has been selected
    ///
    /// - parameter element: selected element
    func didSelect(_ element: FilterRatingComponent.Element)
}

/// an output for a group
public protocol FilterRatingContainerComponentOutput: class {

    /// tells receiver that the element at specified index has been selected
    ///
    /// - parameter element: selected element
    /// - parameter index: an index of the selected element
    func didSelect(_ element: FilterRatingComponent.Element, at index: Int)
}

/// a component which provides whether a single or a groupped rilter rating view
public protocol FilterRatingComponent {

    typealias Container = UIView & FilterRatingContainer
    typealias Element = UIView & FilterRatingElement

    func filterContainer(_ numberOfElements: Int) -> Container
    func singleElement(_ value: Int) -> Element
}

public class FilterRatingComponentImpl: FilterRatingComponent {

    weak var singleOutput: SingleFilterRatingComponentOutput?
    weak var containerOutput: FilterRatingContainerComponentOutput?

    public init() {}

    public func filterContainer(_ numberOfElements: Int) -> Container {
        return FilterRatingContainerImpl(elements: numberOfElements)
    }

    public func singleElement(_ value: Int) -> Element {
        return FilterRatingElementImpl(value: value)
    }
}
