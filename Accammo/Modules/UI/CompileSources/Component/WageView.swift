
public final class WageView: UIView {

    public let rateLabel: UILabel = UILabel()
    public let basisLabel: UILabel = UILabel()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }
}

private extension WageView {

    func configureUI() {
        addSubview(rateLabel)
        addSubview(basisLabel)

        configureRateLabel()
        configureBasisLabel()
    }

    func configureRateLabel() {
        rateLabel.textAlignment = .center
        constrainRate()
    }

    func configureBasisLabel() {
        basisLabel.textAlignment = .center
        constrainBasis()
    }
}

private extension WageView {

    func constrainRate() {
        rateLabel.translatesAutoresizingMaskIntoConstraints = false
        rateLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        rateLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        rateLabel.bottomAnchor.constraint(equalTo: basisLabel.topAnchor).isActive = true
    }

    func constrainBasis() {
        basisLabel.translatesAutoresizingMaskIntoConstraints = false
        basisLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        basisLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        basisLabel.topAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
