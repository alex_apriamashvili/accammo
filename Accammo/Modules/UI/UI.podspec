#
# Be sure to run `pod lib lint UI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UI'
  s.version          = '0.1.0'
  s.summary          = 'Shared UI logic and components'
  s.description      = 'A module that keeps all the logic that is related to UI and components that needs to be shared across the App modules and the App itself.'
  s.homepage         = 'https://alex_apriamashvili@bitbucket.org/alex_apriamashvili/accommo.git'
  s.author           = { 'Alex Apriamashvili' => 'alexander.apriamashvili@gmail.com' }

  s.ios.deployment_target = '10.0'
  s.swift_version = '5'
  s.source = { :path => '' }
  s.source_files = 'CompileSources/**/*'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => "#{s.swift_version}", 'FRAMEWORK_SEARCH_PATHS' => '${BUILT_PRODUCTS_DIR}/../' }

  s.dependency 'Differ', '~> 1.4.3'
  s.dependency 'SDWebImage/WebP'
end
