
import Foundation

public struct UserDetailResponse: Codable {

    public let info: UserDetailResponse.UserInfo

    public init(info: UserDetailResponse.UserInfo) {
        self.info = info
    }
}

public extension UserDetailResponse {

    struct UserInfo: Codable {

        public var imageURL: String
        public var imageW: Double
        public var imageH: Double
        public var expMonths: Int
        public var fName: String
        public var lName: String
        public var title: String
        public var bio: String

        public init(imageURL: String,
                    imageW: Double,
                    imageH: Double,
                    expMonths: Int,
                    fName: String,
                    lName: String,
                    title: String,
                    bio: String) {
            self.imageURL = imageURL
            self.imageW = imageW
            self.imageH = imageH
            self.expMonths = expMonths
            self.fName = fName
            self.lName = lName
            self.title = title
            self.bio = bio
        }
    }
}
