
import Foundation

public protocol UserProfileService {

    /// user detail response type
    typealias Response = UserDetailResponse

    /// success closure description
    /// in case of success, a array of UserDetailResponse is returned
    typealias SuccessClosure = (Response) -> ()

    /// failure closure description
    /// in case of failure an instance of an Error is returned
    typealias FailureClosure = (Error) -> ()

    /// obtain user infor from a remote
    /// - Parameters:
    ///     - token: a user token that is used to identify an actual user
    ///     - success: a callback that is triggered on success
    ///     - failure: a callback that is triggered on failure
    func fetchUserInfo(token: String,
                       success: @escaping SuccessClosure,
                       failure: @escaping FailureClosure)

    /// notify server that a user with such identifier
    /// has been logged out
    func logout(token: String)
}

final public class UserProfileServiceImpl: UserProfileService {

    private let client: APIClient

    public init(client: APIClient) {
        self.client = client
    }

    public func fetchUserInfo(token: String,
                       success: @escaping UserProfileService.SuccessClosure,
                       failure: @escaping UserProfileService.FailureClosure) {
        let params = [Key.token : token]
        client.post(path: Path.detail, parameters: params, responseType: Response.self, success: { result in
            success(result)
        }) { error in
            failure(error)
        }
    }

    public func logout(token: String) {
        let params = [Key.token : token]
            client.post(
                path: Path.logout,
                parameters: params,
                responseType: IgnoredResponse.self,
                success: { _ in }
            ){ _ in }
    }
}

private extension UserProfileServiceImpl {

    struct Path {
        static let detail = "user/detail"
        static let logout = "user/logout"
    }

    struct Key {
        static let token = "token"
    }
}
