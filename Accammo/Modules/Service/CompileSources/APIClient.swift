
import Alamofire

/// a protocol that describes an entity
/// that could actually communicate with an API.
public protocol APIClient {

    /// a type that describes input parameters
    typealias Parameters = [String: Any]

    /// a type that describes a success callback
    typealias SuccessClosure<T> = (T) -> ()

    /// a type that describes a failure callback
    typealias FailureClosure = (Error) -> ()

    /// a base URL, defined by the client
    var baseURL: String { get }

    /// make a POST request to a server with given params
    /// and handle it's response
    /// - parameter path: URI of the service on a server side
    /// - parameter parameters: request body parameters
    /// - parameter responceType: a type of a response object expected
    /// - parameter success: a callback that is triggered in case of server returned a result woth no errors in it
    /// - parameter failure: a callback that is triggered in case of a bad response
    func post<R: Codable>(path: String,
                          parameters: Parameters?,
                          responseType: R.Type,
                          success: @escaping SuccessClosure<R>,
                          failure: @escaping FailureClosure)

    /// make a GET request to a server with given params
    /// and handle it's response
    /// - parameter path: URI of the service on a server side
    /// - parameter parameters: request body parameters
    /// - parameter responceType: a type of a response object expected
    /// - parameter success: a callback that is triggered in case of server returned a result woth no errors in it
    /// - parameter failure: a callback that is triggered in case of a bad response
    func get<R: Codable>(path: String,
                         parameters: Parameters?,
                         responseType: R.Type,
                         success: @escaping SuccessClosure<R>,
                         failure: @escaping FailureClosure)

    /// make a PUT request to a server with given params
    /// and handle it's response
    /// - parameter path: URI of the service on a server side
    /// - parameter parameters: request body parameters
    /// - parameter responceType: a type of a response object expected
    /// - parameter success: a callback that is triggered in case of server returned a result woth no errors in it
    /// - parameter failure: a callback that is triggered in case of a bad response
    func put<R: Codable>(path: String,
                         parameters: Parameters?,
                         responseType: R.Type,
                         success: @escaping SuccessClosure<R>,
                         failure: @escaping FailureClosure)
}

public final class APIClientImpl: APIClient {

    enum ClientError: Error {
        case parsingError
    }

    public let baseURL: String = "http://demo9469512.mockable.io"

    public init() {}
}

// MARK: - POST

public extension APIClientImpl {

    func post<R: Codable>(path: String,
                          parameters: Parameters?,
                          responseType: R.Type,
                          success: @escaping SuccessClosure<R>,
                          failure: @escaping FailureClosure) {
        request(
            path: path,
            parameters: parameters,
            responseType: responseType,
            method: .post,
            success: success,
            failure: failure
        )
    }
}

// MARK: - GET

public extension APIClientImpl {

    func get<R: Codable>(path: String,
                          parameters: Parameters?,
                          responseType: R.Type,
                          success: @escaping SuccessClosure<R>,
                          failure: @escaping FailureClosure) {
        request(
            path: path,
            parameters: parameters,
            responseType: responseType,
            method: .get,
            success: success,
            failure: failure
        )
    }
}

// MARK: - PUT

public extension APIClientImpl {

    func put<R: Codable>(path: String,
                         parameters: Parameters?,
                         responseType: R.Type,
                         success: @escaping SuccessClosure<R>,
                         failure: @escaping FailureClosure) {
        request(
            path: path,
            parameters: parameters,
            responseType: responseType,
            method: .put,
            success: success,
            failure: failure
        )
    }
}

private extension APIClientImpl {

    func request<R: Codable>(path: String,
                             parameters: Parameters?,
                             responseType: R.Type,
                             method: HTTPMethod,
                             success: @escaping SuccessClosure<R>,
                             failure: @escaping FailureClosure) {
        let url = fullPath(uri: path)
        let request = Alamofire.request(
            url,
            method: method,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: [:]
        )

        guard responseType != IgnoredResponse.self else { return }

        request.responseJSON { response in
            guard response.error == nil else { failure(response.error!); return }
            do {
                let data = try JSONDecoder().decode(responseType, from: response.data!)
                success(data)
            } catch {
                failure(APIClientImpl.ClientError.parsingError)
            }
        }
    }

    func fullPath(uri: String) -> String {
        if uri[uri.startIndex] == "/" {
            return "\(baseURL)\(uri)"
        }

        return "\(baseURL)/\(uri)"
    }
}

struct IgnoredResponse: Codable {}
