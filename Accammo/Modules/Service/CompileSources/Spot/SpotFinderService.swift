
import Foundation

/// a protocol that gives a description for a service
/// that provides with available spots
public protocol SpotFinderService {

    /// spots search response type
    typealias SearchResponse = SpotSearchResponse

    /// success closure description
    /// in case of success, an array of `SpotModel`s is returned
    typealias SuccessClosure = (SearchResponse) -> ()

    /// failure closure description
    /// in case of failure an instance of an Error is returned
    typealias FailureClosure = (Error) -> ()

    /// get a list of spots available for a specified search criteria
    /// - Parameters:
    ///     - criteria: a search criteria for spots that needs to be found
    ///     - success: a closure that is being called on success
    ///     - failure: a closure that is being called if a failure occures
    func findSpots(criteria: SpotSearchCriteriaModel,
                   success: @escaping SuccessClosure,
                   failure: @escaping FailureClosure)

    /// get a list of spots, recommended for the user
    /// - Parameters:
    ///     - success: a closure that is being called on success
    ///     - failure: a closure that is being called if a failure occures
    func recommendedSpots(success: @escaping SuccessClosure,
                          failure: @escaping FailureClosure)
}

final public class SpotFinderServiceImpl: SpotFinderService {

    let client: APIClient

    public init(client: APIClient) {
        self.client = client
    }

    public func findSpots(criteria: SpotSearchCriteriaModel,
                   success: @escaping (SearchResponse) -> (),
                   failure: @escaping (Error) -> ()) {
        guard let params = criteria.dictionary else {
            failure(ServiceError.badParams)
            return
        }
        client.post(path: Path.search, parameters: params, responseType: SearchResponse.self, success: { results in
            success(results)
        }) { error in
            failure(error)
        }
    }

    public func recommendedSpots(success: @escaping SuccessClosure,
                          failure: @escaping FailureClosure) {
        client.post(path: Path.recommended, parameters: nil, responseType: SearchResponse.self, success: { results in
            success(results)
        }) { error in
            failure(error)
        }
    }
}

private extension SpotFinderServiceImpl {

    struct Path {
        static let search = "search/spot"
        static let recommended = "search/recommended"
    }
}
