
import Foundation

public struct SpotSearchResponse: Codable {

    public let spotList: [SpotSearchResponse.Spot]

    public init(spotList: [SpotSearchResponse.Spot]) {
        self.spotList = spotList
    }
}

public extension SpotSearchResponse {

    struct Spot: Codable {

        public let id: Int
        public let name: String
        public let description: String
        public let type: SpotSearchResponse.Spot.SpotType
        public let rating: Double
        public let image: SpotSearchResponse.Spot.Image
        public let wage: SpotSearchResponse.Spot.Wage

        public init(id: Int,
                    name: String,
                    description: String,
                    type: SpotSearchResponse.Spot.SpotType,
                    rating: Double,
                    image: SpotSearchResponse.Spot.Image,
                    wage: SpotSearchResponse.Spot.Wage) {
            self.id = id
            self.name = name
            self.description = description
            self.type = type
            self.rating = rating
            self.image = image
            self.wage = wage
        }

        public enum SpotType: String, Codable {
            case none
            case bar
            case club
            case apartment
            case studio
            case band
        }

        public struct Image: Codable {

            public let url: String
            public let width: Double
            public let height: Double

            public init(url: String,
                        width: Double,
                        height: Double) {
                self.url = url
                self.width = width
                self.height = height
            }
        }

        public struct Wage: Codable {

            public let amount: Double
            public let currency: SpotSearchResponse.Spot.Wage.Currency
            public let basis: SpotSearchResponse.Spot.Wage.Basis

            public init(amount: Double,
                        currency: SpotSearchResponse.Spot.Wage.Currency,
                        basis: SpotSearchResponse.Spot.Wage.Basis) {
                self.amount = amount
                self.currency = currency
                self.basis = basis
            }

            public struct Currency: Codable {
                public let code: String
                public let symbol: String

                public init(code: String,
                            symbol: String) {
                    self.code = code
                    self.symbol = symbol
                }
            }

            public enum Basis: String, Codable {
                case none
                case set
                case day
                case hour
                case tour
            }
        }
    }
}
