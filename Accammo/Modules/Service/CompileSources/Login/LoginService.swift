
import Foundation

public protocol LoginService {

    /// user detail response type
    typealias Response = AuthorizationResult

    /// success closure description
    /// in case of success, a array of UserDetailResponse is returned
    typealias SuccessClosure = (Response) -> ()

    /// failure closure description
    /// in case of failure an instance of an Error is returned
    typealias FailureClosure = (Error) -> ()

    /// obtain user infor from a remote
    /// - Parameters:
    ///     - token: a user token that is used to identify an actual user
    ///     - success: a callback that is triggered on success
    ///     - failure: a callback that is triggered on failure
    func login(credentials: AuthorizationParams,
               success: @escaping SuccessClosure,
               failure: @escaping FailureClosure)
}

public final class LoginServiceImpl: LoginService {

    private let client: APIClient

    public init(client: APIClient) {
        self.client = client
    }

    public func login(credentials: AuthorizationParams, success: @escaping SuccessClosure, failure: @escaping FailureClosure) {
        client.post(path: Path.login, parameters: credentials.dictionary, responseType: Response.self, success: { result in
            success(result)
        }) { error in
            failure(error)
        }
    }
}

private extension LoginServiceImpl {

    struct Path {
        static let login = "login"
    }
}
