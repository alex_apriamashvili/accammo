
import Foundation

public struct AuthorizationParams: Codable {

    public var login: String
    public var password: String

    public init(login: String,
                password: String) {
        self.login = login
        self.password = password
    }
}
