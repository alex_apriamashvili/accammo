
import Foundation

public struct AuthorizationResult: Codable {

    public var token: String

    public init(token: String) {
        self.token = token
    }
}
