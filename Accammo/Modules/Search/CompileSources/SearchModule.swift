
import UI
import Domain

/// a typealias that represents the Search Module
public typealias SearchModule = (controller: SearchViewController, input: SearchModuleInput)

// MARK: - Dependencies

/// a typealias that combiles all the dependencies needed for this module
public typealias SearchModuleDependency = HasSearchModuleStyleProvider & HasSearchModuleResourcesPovider

/// a protocol that describes an entity
/// that brings styling attributes to a module
public protocol HasSearchModuleStyleProvider {

    /// a set of styling attruibutes
    var style: SearchModuleStyleProvider { get }
}

/// a protocol that describes an entity
/// that brings resources into a module
public protocol HasSearchModuleResourcesPovider {

    /// a set of resources required by the module
    var resources: SearchModuleResourcesPovider { get }
}

/// a protocol that describes styles that are required by the module
public protocol SearchModuleStyleProvider {

    /// a main background color
    var backgroundColor: UIColor { get }

    /// a main tint color
    var tintColor: UIColor { get }

    // Cell

    /// a color of the search result title
    var searchResultTitleColor: UIColor { get }

    /// a font of the search result title
    var searchResultFont: UIFont { get }

    /// a color of the search result short description
    var searchResultDescriptionTextColor: UIColor { get }

    /// a font of the search result short description
    var searchResultDescriptionFont: UIFont { get }

    /// a color of the wage title of the search result
    var wageTitleColor: UIColor { get }

    /// a font of the wage title of the search result
    var wageTitleFont: UIFont { get }

    /// a color of the wage base title for the given result
    var wageBaseColor: UIColor { get }

    /// a font of the wage base title for the given result
    var wageBaseFont: UIFont { get }
}

/// a protocol that describes resources that are required by the module
public protocol SearchModuleResourcesPovider {

    /// a view controller title string
    var title: String { get }

    /// an image of the search button
    var searchButtonImage: UIImage { get }
}

// MARK: - Module I/O Protocols

/// an interface that holds methods to communicate with the module
public protocol SearchModuleInput {

    /// tells the receiver that a search criteria
    /// has been changed and needs to be updated
    /// - parameter searchCriteria: new search criteria selected by user
    func update(searchCriteria: SearchCriteria)

    /// tells the receiver that a Network Call could be performed
    /// in case if this method wasn't triggered – no network call would be performed
    func setNeedsMakeNetworkCall()

    /// tells receiver to present a login flow to let user get logged in first
    /// this action should be performed immidiately – no animation needed
    func present(login flow: UIViewController)

    /// if any flow is presented at the moment above the current one,
    /// it will be dismissed by calling this function
    func dismissAnyPresentedFlows()

    /// ask the receiver to present a filter flow
    func show(filter flow: UIViewController)

    /// ask the receiver to present a detail flow
    func show(detail flow: UIViewController)
}

/// a protocol that contains methods to handle events
/// that may occur in the module
public protocol SearchModuleOutput: class {

    /// notify the receiver, that user has selected an entry
    /// - parameter spot: the entry user has selected
    func handleTapOnSpot(_ spot: Spot)

    /// notify the receiver,
    /// that user wants to see search edit screen
    func handleTapOnSearch()
}

// MARK: Interactor

/// a protocol that describes a communication interface
/// for the module interactor
protocol SearchInteractorInput {

    /// perform search with a given SearchCriteria
    /// - parameter searchCriteria: a set of filters user wants to apply
    func search(searchCriteria: SearchCriteria)

    /// obtain a particulart search result for a given index
    /// - parameter index: an index of an element that needs to be obtained
    func fetchSearchResult(at index: Int)
}

/// a way for the Interactor to notify about changes that've taken place
protocol SearchInteractorOutput: class {

    /// tells the receiver
    /// that the received results have to be represented
    /// - parameter spots: an array of domain entities taht describe search result
    func presentResults(spots: [Spot])

    /// tells the receiver that an error has occured
    /// and that the user needs to be notified
    func present(error: Error)

    /// tells the receiver that a particular search result has to be presented
    /// used to navigate to details
    func present(spot: Spot)
}

// MARK: - View

/// a ptorocol that defines actions that could been done to a view
protocol SearchViewInput: class {

    /// tells the receiver to show a spinner
    func showPreloader()

    /// tells the receiver to hide a spinner
    func hidePreloader()

    /// tells the receiver that an emplty state has to be represented
    func showEmptyState()

    /// tells the receiver to represent a given error message
    /// - parameter message: a localized description of the error occured
    func showError(message: String)

    /// tells the receiver that the view models must be updated
    func update(viewModels: [SearchViewModel])
}

/// a protocol that holds the events that might happen for the view
/// basically it's the only way of how view might tell about it's changes
protocol SearchViewOutput {

    /// tells the receiver that all views were being loaded
    /// and it's ready for the interaction
    func viewIsReady()

    /// tells the receiver that user has tapped on a search button
    func handleTapOnSearch()

    /// tells the receiver that user wan't to see updated data
    func reloadData()
}

// MARK: - Router

/// a protocol that describes routing actions
protocol SearchRouterInput {

    /// tell the receiver to hide
    /// all the additional flows presented on this module
    func dismiss()

    /// this method should be called if next view-countroller
    /// **shouldn't** be put into a navigation stack
    func present(_ viewController: UIViewController, animated: Bool)

    /// this method should be called if next view-countroller
    /// **needs** to be put into a navigation stack
    func push(_ viewController: UIViewController)
}

