
import UI
import Domain
import Service

public final class SearchModuleBuilder {

    public static func build(output: SearchModuleOutput?,
                             searchCtiteria: SearchCriteria,
                             dependencies: SearchModuleDependency) -> SearchModule {
        let presenter = SearchPresenter(searchCriteria: searchCtiteria, dependency: dependencies)
        let view = SearchViewController(dependency: dependencies, output: presenter)
        let router = SearchRouter(baseViewController: view)
        let service = SpotFinderServiceImpl(client: APIClientImpl())
        let interactor = SearchInteractor(service: service)

        presenter.output = output
        presenter.view = view

        presenter.router = router

        interactor.output = presenter
        presenter.interactor = interactor

        return (controller: view, input: presenter)
    }
}
