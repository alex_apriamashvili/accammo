
import Foundation
import UI

final class SearchRouter {

    weak var controller: UIViewController?

    init(baseViewController: UIViewController) {
        controller = baseViewController
    }
}

extension SearchRouter: SearchRouterInput {

    func dismiss() {
        if let _ = controller?.presentedViewController {
            controller?.dismiss(animated: true, completion: nil)
        } else {
            controller?.navigationController?.popViewController(animated: true)
        }
    }

    func present(_ viewController: UIViewController, animated: Bool) {
        controller?.present(viewController, animated: animated, completion: nil)
    }

    func push(_ viewController: UIViewController) {
        controller?.navigationController?.pushViewController(viewController, animated: true)
    }
}
