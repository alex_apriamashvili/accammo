
import UI
import Domain
import Service

final class SearchModuleMapper {

    static func map(entity: Spot, style: SearchModuleStyleProvider) -> SearchViewModel {
        return SearchViewModel(
            title: entity.name,
            description: entity.description,
            wage: formatted(wage: entity.wage),
            basis: formatted(wageBasis: entity.wage.basis),
            imageURL: entity.image.imageURL,
            imageSize: adjustedSize(width: entity.image.width, height: entity.image.height),
            type: formtted(type: entity.type),
            style: style
        )
    }

    static func map(searchCriteria: SearchCriteria) -> SpotSearchCriteriaModel {
        return SpotSearchCriteriaModel()
    }

    static func map(model: SpotSearchResponse.Spot) -> Spot {
        return Spot(
            identifier: model.id,
            name: model.name,
            description: shortenedDescription(model.description),
            rating: model.rating,
            image: Image(
                imageURL: URL(string: model.image.url)!,
                width: model.image.width,
                height: model.image.height
            ),
            wage: Wage(
                amount: model.wage.amount,
                currency: Currency(
                    symbol: model.wage.currency.symbol,
                    code:  model.wage.currency.code
                ),
                basis: map(basis: model.wage.basis)
            ),
            type: map(type: model.type)
        )
    }
}

private extension SearchModuleMapper {

    struct Basis {
        static let set = "per set"
        static let day = "per day"
        static let hour = "per hour"
        static let tour = "per tour"
    }

    struct SpotType {
        static let bar = "Bar"
        static let club = "Club"
        static let appartment = "Apartment"
        static let studio = "Studio"
    }

    static func formatted(wage: Wage) -> String {
        let amount = String(format:"%.2f", wage.amount)
        return "\(wage.currency.symbol)\(amount)"
    }

    static func formatted(wageBasis: Wage.Basis) -> String {
        switch wageBasis {
        case .set: return Basis.set
        case .day: return Basis.day
        case .hour: return Basis.hour
        case .tour: return Basis.tour
        }
    }

    static func formtted(type: Spot.SpotType) -> String {
        return type.rawValue.capitalized
    }

    static func adjustedSize(width: Double, height: Double) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let ratio = CGFloat(width / height)
        let imageHeight = round(screenWidth / ratio)
        return CGSize(width: screenWidth, height: imageHeight)
    }

    static func shortenedDescription(_ description: String) -> String {
        if description.count > 144 {
            let index = description.index(description.startIndex, offsetBy: 141)
            return "\(description[..<index])..."
        }
        return description
    }

    static func map(basis: SpotSearchResponse.Spot.Wage.Basis) -> Wage.Basis {
        switch basis {
        case .day: return .day
        case .hour: return .hour
        case .set: return .set
        case .tour: return .tour
        default: return .hour
        }
    }

    static func map(type: SpotSearchResponse.Spot.SpotType) -> Spot.SpotType {
        switch type {
        case .apartment: return .apartment
        case .band: return .band
        case .bar: return .bar
        case .club: return .club
        case .studio: return .studio
        default: return .studio
        }
    }
}
