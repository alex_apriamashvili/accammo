
import Domain
import Service

final class SearchInteractor {

    weak var output: SearchInteractorOutput?

    private let service: SpotFinderService?
    private var entities: [Spot] = []

    init(service: SpotFinderService) {
        self.service = service
    }
}

// MARK: - SearchInteractorInput Implementation

extension SearchInteractor: SearchInteractorInput {

    func search(searchCriteria: SearchCriteria) {
        let requestParams = SearchModuleMapper.map(searchCriteria: searchCriteria)
        service?.findSpots(criteria: requestParams, success: { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.entities = result.spotList.map{ SearchModuleMapper.map(model: $0) }
            strongSelf.output?.presentResults(spots: strongSelf.entities)
        }, failure: { [weak self] error in
            guard let strongSelf = self else { return }
            strongSelf.output?.present(error: error)
        })
    }

    func fetchSearchResult(at index: Int) {
        guard index < entities.count else {
            assertionFailure("index is out of bounds")
            return
        }
        let spot = entities[index]
        output?.present(spot: spot)
    }
}
