
import Foundation
import UI

public final class SearchViewController: UIViewController {

    typealias Dependency = HasSearchModuleStyleProvider & HasSearchModuleResourcesPovider
    typealias Output = SearchViewOutput & SearchCollectionViewSourceActionHandler


    let output: Output

    private let dependency: Dependency
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private let source: SearchCollectionViewSource
    private let refreshControl = UIRefreshControl()

    // MARK: - Initialization

    init(dependency: Dependency, output: Output) {
        self.dependency = dependency
        self.output = output
        source = SearchCollectionViewSource(actionHandler: self.output)
        super.init(nibName: nil, bundle: nil)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output.viewIsReady()
    }

    // MARK: - Actions

    @objc
    func tapSearchButtonItem() {
        output.handleTapOnSearch()
    }

    @objc
    func pullToRefresh() {
        output.reloadData()
    }
}

// MARK: - SearchViewInput Implementation

extension SearchViewController: SearchViewInput {

    func update(viewModels: [SearchViewModel]) {
        source.viewModels = viewModels
        collectionView.reloadData()
    }

    func showPreloader() {}

    func hidePreloader() {
        refreshControl.endRefreshing()
    }

    func showEmptyState() {}

    func showError(message: String) {}
}

// MARK: - Private

private extension SearchViewController {

    func configureUI() {
        title = dependency.resources.title
        view.backgroundColor = dependency.style.backgroundColor
        configureCollectionView()
        configureTabBar()
        configureRefreshControl()
    }

    func configureTabBar() {
        let barButton = UIBarButtonItem(
            image: dependency.resources.searchButtonImage,
            style: .plain,
            target: self,
            action: #selector(tapSearchButtonItem)
        )
        barButton.tintColor = dependency.style.tintColor
        navigationItem.setRightBarButtonItems([barButton], animated: true)
    }

    func configureRefreshControl() {
        collectionView.alwaysBounceVertical = true
        refreshControl.tintColor = dependency.style.tintColor
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
}

// MARK: - CollectionView

private extension SearchViewController {

    func configureCollectionView() {
        collectionView.backgroundColor = dependency.style.backgroundColor

        view.addSubview(collectionView)

        registerPreviewCell()

        collectionView.dataSource = source
        collectionView.delegate = source

        constrainCollectionView()
    }

    func registerPreviewCell() {
        collectionView.register(
            SearchPreviewCell.self,
            forCellWithReuseIdentifier: SearchCollectionViewSource.searchCellIdentifier
        )
    }

    func constrainCollectionView() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
