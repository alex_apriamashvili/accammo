
import UI

protocol SearchCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int)
}

final class SearchCollectionViewSource: NSObject {

    static let searchCellIdentifier = "com.accammo.spots.cell.identifier"

    var viewModels: [SearchViewModel] = []

    private let prototypeCell = SearchPreviewCell(frame: .zero)
    private let actionHandler: SearchCollectionViewSourceActionHandler

    init(actionHandler: SearchCollectionViewSourceActionHandler) {
        self.actionHandler = actionHandler
        super.init()
    }
}

private extension SearchCollectionViewSource {

    func cellSize(_ viewModel: SearchViewModel, width: CGFloat) -> CGSize {
        let widthContraint = prototypeCell.contentView.widthAnchor.constraint(equalToConstant: width)
        widthContraint.isActive = true

        prototypeCell.contentView.translatesAutoresizingMaskIntoConstraints = false
        prototypeCell.configure(viewModel: viewModel)

        prototypeCell.contentView.setNeedsUpdateConstraints()
        prototypeCell.contentView.updateConstraintsIfNeeded()
        prototypeCell.contentView.setNeedsLayout()
        prototypeCell.contentView.layoutIfNeeded()

      let size = prototypeCell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        widthContraint.isActive = false
        prototypeCell.removeConstraint(widthContraint)

        return size
    }
}

extension SearchCollectionViewSource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: SearchCollectionViewSource.searchCellIdentifier,
            for: indexPath
        )
        guard let spotCell = cell as? SearchPreviewCell,
            indexPath.item < viewModels.count else { fatalError("wrong cell was dequeued") }
        let viewModel = viewModels[indexPath.item]
        spotCell.configure(viewModel: viewModel)

        return spotCell
    }
}

extension SearchCollectionViewSource: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionHandler.handleCellTap(at: indexPath.item)
    }
}

extension SearchCollectionViewSource: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewModel = viewModels[indexPath.item]
        let width = collectionView.bounds.width

        return cellSize(viewModel, width: width)
    }
}
