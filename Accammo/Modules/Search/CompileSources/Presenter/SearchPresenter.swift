
import UI
import Domain

// MARK: - Public

final class SearchPresenter {

    typealias Dependency = HasSearchModuleStyleProvider

    weak var output: SearchModuleOutput?
    weak var view: SearchViewInput?
    var interactor: SearchInteractorInput?
    var router: SearchRouterInput?

    private(set) var searchCriteria: SearchCriteria
    private(set) var dependency: Dependency
    private(set) var viewLoaded: Bool = false
    private(set) var pendingViewController: UIViewController?

    init(searchCriteria: SearchCriteria, dependency: Dependency) {
        self.searchCriteria = searchCriteria
        self.dependency = dependency
    }
}

// MARK: - Private

private extension SearchPresenter {

    func completelyReloadData() {
        view?.showPreloader()
        interactor?.search(searchCriteria: searchCriteria)
    }
}

// MARK: - SearchInteractorOutput Implementation

extension SearchPresenter: SearchInteractorOutput {

    func presentResults(spots: [Spot]) {
        view?.hidePreloader()

        guard spots.count > 0 else {
            view?.showEmptyState()
            return
        }

        let viewModels = spots.map{ SearchModuleMapper.map(entity: $0, style: dependency.style) }
        view?.update(viewModels: viewModels)
    }

    func present(error: Error) {
        view?.hidePreloader()
        view?.showError(message: error.localizedDescription)
    }

    func present(spot: Spot) {
        output?.handleTapOnSpot(spot)
    }
}

// MARK: - SearchViewOutput Implementation

extension SearchPresenter: SearchViewOutput {

    func viewIsReady() {
        viewLoaded = true
        if let vc = pendingViewController {
            router?.present(vc, animated: false)
        } else  {
            completelyReloadData()
        }
    }

    func handleTapOnSearch() {
        output?.handleTapOnSearch()
    }

    func reloadData() {
        completelyReloadData()
    }
}

// MARK: - SearchModuleInput Implementation

extension SearchPresenter: SearchModuleInput {

    func dismissAnyPresentedFlows() {
        router?.dismiss()
    }

    func setNeedsMakeNetworkCall() {
        pendingViewController = nil
        guard viewLoaded else { return }
        completelyReloadData()
    }

    func present(login flow: UIViewController) {
        pendingViewController = flow
    }

    func update(searchCriteria: SearchCriteria) {
        self.searchCriteria = searchCriteria
        completelyReloadData()
    }

    func show(filter flow: UIViewController) {
        router?.push(flow)
    }

    func show(detail flow: UIViewController) {
        router?.push(flow)
    }
}

// MARK: - SearchCollectionViewSourceActionHandler Implementation

extension SearchPresenter: SearchCollectionViewSourceActionHandler {

    func handleCellTap(at index: Int) {
        interactor?.fetchSearchResult(at: index)
    }
}

