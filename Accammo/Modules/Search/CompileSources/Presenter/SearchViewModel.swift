
import UI

/// a view model that describes a particular entity
/// that should be displayed on a screen
struct SearchViewModel {

    /// a title of a spot
    /// basically just a name of it
    let title: String

    /// spot description
    /// few words about the spot itself to lure the musition
    let description: String

    /// an amount of money
    /// that is offered an a compensation for an assistance
    let wage: String

    /// a wage basis (e.g.: per hour, per set, etc.)
    let basis: String

    /// a URL to the image of the spot
    /// a fancy image that takes almost the whole cell
    /// to catch an eye of a mucician
    let imageURL: URL

    /// a size of an image
    let imageSize: CGSize

    /// a type of a spot that needs to be displayed
    /// (e.g.: Bar, Studio, etc.)
    let type: String

    /// a style dependency that should be applied on a view
    let style: SearchModuleStyleProvider
}
