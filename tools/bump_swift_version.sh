##
#
# A scripts that helps to specify the SWIFT_VERSION of the local CocoaPod dependencies
#
#   Usage:
#   `cd $POJECT_ROOT`
#   `sh tools/bump_swift_version.sh "Accammo/Modules" "5"`,
#   where "Accammo/Modules" is a path to the local pods and "5" is a desired SWIFT_VERSION
#
#   To set a version for a particular pod only, the first parameter shall identify a directory of that particular pod.
#   For example if it is required to update only the Profile pod, the command shall look as the following:
#   `sh tools/bump_swift_version.sh "Accammo/Modules/Profile" "5.1"`
#
##

PODSPEC_PATTERN="*.podspec"
SWIFT_VERSION_PATTERN="\w*\.swift_version\s*="
VERSION_NUMBER_PATTERN="\d*\.*\d"

find_podspec_files() {
    local search_dir="$1"
    find "${search_dir}" -name "${PODSPEC_PATTERN}"
}

set_swift_version() {
    local podspec="$1"
    local swift_version="$2"
    local origin_string=$(grep -e "$SWIFT_VERSION_PATTERN" "${podspec}")
    local current_swift_version=$(echo "$origin_string" | grep -ohr "$VERSION_NUMBER_PATTERN")
    local result_string="${origin_string//$current_swift_version/$swift_version}"
    if [[ -n current_swift_version ]]; then 
        sed -i -- "s/${origin_string}/${result_string}/g" "${podspec}"
        echo "Bumped up SWIFT_VERSION from ${current_swift_version} to ${swift_version} in ${podspec}"
        rm -rf "${podspec}--"
    fi
}

main() {
    local search_dir="$1"
    local swift_version="$2"
    local podspec_list=( $(find_podspec_files "$search_dir") )
    for podspec in "${podspec_list[@]}"; do
        set_swift_version "$podspec" "$swift_version"
    done
}

main "$@"