##
#
# A scripts that helps to specify the Pod version of the local CocoaPod dependencies
#
#   Usage:
#   `cd $POJECT_ROOT`
#   `sh tools/bump_pod_version.sh "Accammo/Modules" "0.1.0"`,
#   where "Accammo/Modules" is a path to the local pods and "0.1.0" is a desired version of each pod
#
#   To set a version for a particular pod only, the first parameter shall identify a directory of that particular pod.
#   For example if it is required to update only the Profile pod, the command shall look as the following:
#   `sh tools/bump_pod_version.sh "Accammo/Modules/Profile" "1.2.5"`
#
##

PODSPEC_PATTERN="*.podspec"
POD_VERSION_PATTERN="\w*\.version\s*="
VERSION_NUMBER_PATTERN="\d*\.*\d*\.*\d"

find_podspec_files() {
    local search_dir="$1"
    find "${search_dir}" -name "${PODSPEC_PATTERN}"
}

set_version() {
    local podspec="$1"
    local version="$2"
    local origin_string=$(grep -e "$POD_VERSION_PATTERN" "${podspec}")
    local current_version=$(echo "$origin_string" | grep -ohr "$VERSION_NUMBER_PATTERN")
    local result_string="${origin_string//$current_version/$version}"
    if [[ -n current_version ]]; then 
        sed -i -- "s/${origin_string}/${result_string}/g" "${podspec}"
        echo "Bumped up version from ${current_version} to ${version} for the Pod in ${podspec}"
        rm -rf "${podspec}--"
    fi
}

main() {
    local search_dir="$1"
    local version="$2"
    local podspec_list=( $(find_podspec_files "$search_dir") )
    for podspec in "${podspec_list[@]}"; do
        set_version "$podspec" "$version"
    done
}

main "$@"